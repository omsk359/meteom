// action creators are functions that take a param and return
// an 'action' that is consumed by a reducer. This may seem like
// unneeded boilerplate  but it's **really** nice to have a file
// with *all* possible ways to mutate the state of the app.

import { Streamy } from 'meteor/yuukan:streamy';
import TaskData from "../../common/collections/TaskData";
import { log } from '../../common/logger/Logger';
import { trackCollection } from 'meteor/skinnygeek1010:flux-helpers';
import AccManager from "../AccManager";
import { AccTypes } from "../../common/AccInfo";
import AccInfo from "../../common/AccInfo";
import UserAccs from '../../common/collections/UserAccs';
import store from './store';
import { createAction, handleAction, handleActions } from 'redux-actions';
import UserIdLists_DB from '../../common/collections/UserIdLists_DB';
import { getUserData, setUserData } from "../helpers";

export const ActionTypes = {
    TASK_CREATOR: {
        SWITCH_TO: 'TASK_CREATOR.SWITCH_TO',
        SWITCH_BACK: 'TASK_CREATOR.SWITCH_BACK',
        SET_HIDDEN: 'TASK_CREATOR.SET_HIDDEN'
    },
    IDS_ITERATE_TC: {
        SET_IDS_SOURCE: 'IDS_ITERATE_TC.SET_IDS_SOURCE',
        ADD_ACTION: 'IDS_ITERATE_TC.ADD_ACTION',
        REMOVE_ACTION: 'IDS_ITERATE_TC.REMOVE_ACTION',
        SET_FILTER: 'IDS_ITERATE_TC.SET_FILTER',
        SET_ACTION: 'IDS_ITERATE_TC.SET_ACTION',
        SET_PARAMS: 'IDS_ITERATE_TC.SET_PARAMS',
        SET_TASK_SCHEDULER: 'IDS_ITERATE_TC.SET_TASK_SCHEDULER',
        SET_ON_ACTION: 'IDS_ITERATE_TC.SET_ON_ACTION',
        SET_DELAYS: 'IDS_ITERATE_TC.SET_DELAYS',
    },
    DIALOGS_HISTORY_TC: {
        SET_PARAMS: 'DIALOGS_HISTORY_TC.SET_PARAMS',
        CALL_ACC_METHOD: 'DIALOGS_HISTORY_TC.CALL_ACC_METHOD',
        TASK_RESTART: 'DIALOGS_HISTORY_TC.TASK_RESTART'
    },
    IDS_COLLECT_TC: {
        SET_PARAMS: 'IDS_COLLECT_TC.SET_PARAMS'
    },
    RENAME_ACCS_TC: {
        SET_PARAMS: 'RENAME_ACCS_TC.SET_PARAMS'
    },
    REG_ACCS_TC: {
        SET_PARAMS: 'REG_ACCS_TC.SET_PARAMS'
    },
    UNFREEZE_ACCS_TC: {
        SET_PARAMS: 'UNFREEZE_ACCS_TC.SET_PARAMS'
    },
    BIND_PHONE_TC: {
        SET_PARAMS: 'BIND_PHONE_TC.SET_PARAMS'
    },
    TASK_SCHEDULER_TC: {
        SET_PARAMS: 'TASK_SCHEDULER_TC.SET_PARAMS'
    },
    TASKS: {
        UPDATE: 'TASKS.UPDATE',
        UPDATED: 'TASKS.UPDATED',
        ADD: 'TASKS.ADD',
        //CREATE_REQUEST: 'TASKS.CREATE_REQUEST',
        CREATED: 'TASKS.CREATED',
        START: 'TASKS.START',
        STOP: 'TASKS.STOP',
        PAUSE: 'TASKS.PAUSE',
        RESUME: 'TASKS.RESUME',
        REMOVE: 'TASKS.REMOVE'
    },
    TASK_TABLE: {
        SELECT_TASK: 'TASK_TABLE.SELECT_TASK',
    },
    TASK_DETAILS: {
        SET_TASK_LOG_ID: 'TASK_DETAILS.SET_TASK_LOG_ID',
    },
    ACC_TABLE: {
        SET_ACC_LIST: 'ACC_TABLE.SET_ACC_LIST'
    },
    LOGGER: {
        SET_STREAM_ID: 'LOGGER.SET_STREAM_ID',
        LOAD_FULL_HISTORY: 'LOGGER.LOAD_FULL_HISTORY'
    },
    ACC_MANAGER: {
        LOAD: 'ACC_MANAGER.LOAD',
        SAVE: 'ACC_MANAGER.SAVE',
        ADD_LIST: 'ACC_MANAGER.ADD_LIST',
        REMOVE_LIST: 'ACC_MANAGER.REMOVE_LIST',
        SAVE_LIST: 'ACC_MANAGER.SAVE_LIST',
        SWITCH_LIST: 'ACC_MANAGER.SWITCH_LIST',
        GET_LIST: 'ACC_MANAGER.GET_LIST',
        ACCS_BY_IDS: 'ACC_MANAGER.ACCS_BY_IDS'
    },
    USER_ID_LISTS_MANAGER: {
        LOAD: 'USER_ID_LISTS_MANAGER.LOAD',
        LOAD_LOST_NAMES: 'USER_ID_LISTS_MANAGER.LOAD_LOST_NAMES',
        SAVE: 'USER_ID_LISTS_MANAGER.SAVE',
        ADD_LIST: 'USER_ID_LISTS_MANAGER.ADD_LIST',
        REMOVE_LIST: 'USER_ID_LISTS_MANAGER.REMOVE_LIST',
        SAVE_LIST: 'USER_ID_LISTS_MANAGER.SAVE_LIST',
        SWITCH_LIST: 'USER_ID_LISTS_MANAGER.SWITCH_LIST',
        GET_LIST_SIZE: 'USER_ID_LISTS_MANAGER.GET_LIST_SIZE'
    },
    MESSENGER: {
        SET_TASK_ID: 'MESSENGER.SET_TASK_ID',
        SET_ACCS: 'MESSENGER.SET_ACCS',
        SET_DIALOGS: 'MESSENGER.SET_DIALOGS',
        SET_HISTORY: 'MESSENGER.SET_HISTORY',
        SET_ACC: 'MESSENGER.SET_ACC',
        SET_USER_ID: 'MESSENGER.SET_USER_ID'
    },
    SHOW_MSG: 'SHOW_MSG',
    SHOW_ERR: 'SHOW_ERR',
    HIDE_MSG: 'HIDE_MSG',
    HIDE_ERR: 'HIDE_ERR'
};

export const Actions = {
    taskCreator: {
        switchTo: createAction(ActionTypes.TASK_CREATOR.SWITCH_TO, (ref, params) => ({ ref, params })),
        switchBack: createAction(ActionTypes.TASK_CREATOR.SWITCH_BACK),
        setHidden: createAction(ActionTypes.TASK_CREATOR.SET_HIDDEN),

        //setTaskScheduler: createAction(ActionTypes.TASK_CREATOR.SET_TASK_SCHEDULER),
    },
    idsIterateTC: {
        setIdsSource: createAction(ActionTypes.IDS_ITERATE_TC.SET_IDS_SOURCE),
        addAction: createAction(ActionTypes.IDS_ITERATE_TC.ADD_ACTION),
        removeAction: createAction(ActionTypes.IDS_ITERATE_TC.REMOVE_ACTION),
        setFilter: createAction(ActionTypes.IDS_ITERATE_TC.SET_FILTER),
        setParams: createAction(ActionTypes.IDS_ITERATE_TC.SET_PARAMS),
        setTaskScheduler: createAction(ActionTypes.IDS_ITERATE_TC.SET_TASK_SCHEDULER),
        setDelays: createAction(ActionTypes.IDS_ITERATE_TC.SET_DELAYS),
        setOnAction: createAction(ActionTypes.IDS_ITERATE_TC.SET_ON_ACTION)
    },
    dialogsHistoryTC: {
        setParams: createAction(ActionTypes.DIALOGS_HISTORY_TC.SET_PARAMS),
        callAccMethod: createAction(ActionTypes.DIALOGS_HISTORY_TC.CALL_ACC_METHOD, (dialogTaskId, login, method, params) => {
            return new Promise((resolve, reject) => {
                Streamy.emit('DialogsHistoryTask_{1}'.format(dialogTaskId), {login, method, params});
                Streamy.on('DialogsHistoryTask_{1}'.format(dialogTaskId), (data, from) => {
                    if (data.error)
                        return reject(data.error);
                    resolve(data);
                    log.debug('RECEIVED sendMsg: {1}', data);
                });
            });
        }),
        taskRestart: createAction(ActionTypes.DIALOGS_HISTORY_TC.TASK_RESTART, (dialogTaskId, login) => {
            return new Promise((resolve, reject) => {
                Streamy.emit('DialogsHistoryTask_{1}'.format(dialogTaskId), {taskRestart: login});
                resolve();
            });
        })
    },
    idsCollectTC: {
        setParams: createAction(ActionTypes.IDS_COLLECT_TC.SET_PARAMS)
    },
    renameAccsTC: {
        setParams: createAction(ActionTypes.RENAME_ACCS_TC.SET_PARAMS)
    },
    unfreezeAccsTC: {
        setParams: createAction(ActionTypes.UNFREEZE_ACCS_TC.SET_PARAMS)
    },
    bindPhoneTC: {
        setParams: createAction(ActionTypes.BIND_PHONE_TC.SET_PARAMS)
    },
    regAccsTC: {
        setParams: createAction(ActionTypes.REG_ACCS_TC.SET_PARAMS)
    },
    taskScheduler: {
        setParams: createAction(ActionTypes.TASK_SCHEDULER_TC.SET_PARAMS)
    },
    tasks: {
        created: createAction(ActionTypes.TASKS.CREATED),
        update: createAction(ActionTypes.TASKS.UPDATE, () => {
            return new Promise((resolve, reject) => {
                let onUpdate = tasks => {
                    store.dispatch(Actions.tasks.updated(tasks));
                };
                Meteor.subscribe('tasks', {
                    onReady: () => {
                        let tasks = TaskData.find().fetch();
                        //log.debug('Meteor.subscribe: {1}', tasks);
                        onUpdate(tasks);
                        resolve(tasks);
                    },
                    onError: reject
                });
                trackCollection(TaskData, onUpdate);
            })
        }),
        updated: createAction(ActionTypes.TASKS.UPDATED),
        create: createAction(ActionTypes.TASKS.CREATED, (taskName, params) => {
            log.debug('CREATE TASK: {1}', params);
            return Meteor.callPromise('createTask', taskName, params).then(taskId => {
                log.debug(`Created! ${taskId}`);
                //return taskId;
            }, err => {
                //store.dispatch(Actions.showErr(err.message));
                store.dispatch(Actions.showErr(err.details));
                alert(err.details);
                log.error('create task: {1}', err.details);
            });
        }),
        start: createAction(ActionTypes.TASKS.START, taskId => {
            return Meteor.callPromise('startTask', taskId).then(() => {
                log.debug(`Started! ${taskId}`);
            }, err => {
                store.dispatch(Actions.showErr(err.details));
                alert(err.details);
                log.error('start task: {1}', err.details);
            });
        }),
        stop: createAction(ActionTypes.TASKS.STOP, taskId => {
            return Meteor.callPromise('stopTask', taskId).then(() => {
                log.debug(`Stopped! ${taskId}`);
            }, err => {
                store.dispatch(Actions.showErr(err.details));
                alert(err.details);
                log.error('stop task: {1}', err.details);
            });
        }),
        pause: createAction(ActionTypes.TASKS.PAUSE, taskId => {
            return Meteor.callPromise('pauseTask', taskId).then(() => {
                log.debug(`Paused! ${taskId}`);
            }, err => {
                store.dispatch(Actions.showErr(err.details));
                alert(err.details);
                log.error('pause task: {1}', err.details);
            });
        }),
        resume: createAction(ActionTypes.TASKS.RESUME, taskId => {
            return Meteor.callPromise('resumeTask', taskId).then(() => {
                log.debug(`Resume! ${taskId}`);
            }, err => {
                store.dispatch(Actions.showErr(err.details));
                alert(err.details);
                log.error('resume task: {1}', err.details);
            });
        }),
        remove: createAction(ActionTypes.TASKS.REMOVE, taskId => {
            return Meteor.callPromise('removeTask', taskId).then(() => {
                log.debug(`remove! ${taskId}`);
            }, err => {
                store.dispatch(Actions.showErr(err.details));
                alert(err.details);
                log.error('remove task: {1}', err.details);
            });
        })
    },
    taskTable: {
        selectTask: createAction(ActionTypes.TASK_TABLE.SELECT_TASK)
    },
    taskDetails: {
        setTaskLogId: createAction(ActionTypes.TASK_DETAILS.SET_TASK_LOG_ID)
    },
    logger: {
        setStreamId: createAction(ActionTypes.LOGGER.SET_STREAM_ID),
        loadFullHistory: createAction(ActionTypes.LOGGER.LOAD_FULL_HISTORY, taskLogId => {
            return new Promise((resolve, reject) => {
                store.dispatch(Actions.logger.setStreamId(taskLogId));
                Streamy.emit(taskLogId + '_loadAll');
                log.debug('RESOLVE: {1}', taskLogId);
                Streamy.on(taskLogId + '_loadAllFinished', () => resolve(taskLogId));
            });
        })
    },
    accManager: {
        load: createAction(ActionTypes.ACC_MANAGER.LOAD, () => {
            return new Promise((resolve, reject) => {
                let onReady = () => {
                    let lists = getUserData('accLists') || {};
                    resolve(lists);
                };
                Meteor.subscribe('userData', {
                    onReady: () => {
                        Meteor.subscribe('Accs', {
                            onReady: onReady,
                            onError: reject
                        });
                    },
                    onError: reject
                });
            });
        }),
        accsByIds: createAction(ActionTypes.ACC_MANAGER.ACCS_BY_IDS, ids => {
            return store.dispatch(Actions.accManager.load()).then(lists => {
                return _.transform(ids, (accs, _id) => {
                    let accData = UserAccs.findOne(_id);
                    if (accData)
                        accs.push(new AccInfo().deserialize(accData));
                });
            });
        }),
        save: createAction(ActionTypes.ACC_MANAGER.SAVE, lists => {
            setUserData('accLists', lists);
            return lists;
        }),
        addList: createAction(ActionTypes.ACC_MANAGER.ADD_LIST),
        removeList: createAction(ActionTypes.ACC_MANAGER.REMOVE_LIST),
        saveList: createAction(ActionTypes.ACC_MANAGER.SAVE_LIST, (listName, str) => { // str -> [ _id1, ... ]
            let accs = AccManager.parseFromStr(str, AccTypes.VK);
            let newList = _.map(accs, (acc, i) => {
                let accData = UserAccs.findOne({login: acc.login});
                return accData ? accData : acc;
            });
            let fieldsToUpdate = ['login', 'pass', 'type', 'proxy.ip', 'proxy.port', 'proxy.login', 'proxy.pass'];
            let toUpdate = _.filter(newList, (accData, i) =>
                !accData._id || _.some(fieldsToUpdate, field => _.get(accs[i], field) != _.get(accData, field))
            ).map((accData, i) =>
                _.find(accs, ['login', accData.login]).deserialize(accData, true).serialize()
            );
            return Meteor.callPromise('updateAccs', toUpdate).then(results => {
                log.debug(`Update results: {1}`, results);
                let ids = _.compact(_.map(newList, acc => acc._id || results[acc.login]._id));
                return { ids, listName };
            });
        }),
        switchList: createAction(ActionTypes.ACC_MANAGER.SWITCH_LIST)
    },
    userIdListManager: {
        load: createAction(ActionTypes.USER_ID_LISTS_MANAGER.LOAD, () => {
            return new Promise((resolve, reject) => {
                let onReady = () => {
                    let lists = UserIdLists_DB.find({}).fetch();
                    lists = _.transform(lists, (lists, list) => lists[list.listName] = list.ids, {});
                    resolve(lists);
                };
                let handle = Meteor.subscribe('UserIdLists', {
                    onReady,
                    onError: reject
                });
                if (handle.ready())
                    onReady();
            });
        }),
        loadListNames: createAction(ActionTypes.USER_ID_LISTS_MANAGER.LOAD_LOST_NAMES, () => { // TODO
            return store.dispatch(Actions.userIdListManager.load()).then(lists => {
                return _.keys(lists.payload);
            });
        }),
        save: createAction(ActionTypes.USER_ID_LISTS_MANAGER.SAVE, lists => {
            let oldListNames = UserIdLists_DB.find({}).fetch();
            oldListNames = _.map(oldListNames, 'listName');
            let listsToRemove = _.difference(oldListNames, _.keys(lists));

            let promises = _.map(lists, (newIds, listName) => {
                let oldIds = UserIdLists_DB.findOne({listName});
                oldIds = _.get(oldIds, 'ids');
                if (_.isEqual(newIds, oldIds))
                    return Promise.resolve();
                return Meteor.callPromise('updateUserIdList', listName, newIds);
            });
            return Promise.all(promises).then(() =>
                listsToRemove.length && Meteor.callPromise('removeUserIdLists', listsToRemove)
            );
        }),
        getListSize: createAction(ActionTypes.USER_ID_LISTS_MANAGER.GET_LIST_SIZE, listName => {
            return store.dispatch(Actions.userIdListManager.load()).then(lists => {
                return _.size(lists.payload[listName]);
            });
        }),
        addList: createAction(ActionTypes.USER_ID_LISTS_MANAGER.ADD_LIST),
        removeList: createAction(ActionTypes.USER_ID_LISTS_MANAGER.REMOVE_LIST),
        saveList: createAction(ActionTypes.USER_ID_LISTS_MANAGER.SAVE_LIST),
        switchList: createAction(ActionTypes.USER_ID_LISTS_MANAGER.SWITCH_LIST)
    },
    messenger: {
        setTaskId: createAction(ActionTypes.MESSENGER.SET_TASK_ID),
        setAccs: createAction(ActionTypes.MESSENGER.SET_ACCS),
        setDialogs: createAction(ActionTypes.MESSENGER.SET_DIALOGS),
        setHistory: createAction(ActionTypes.MESSENGER.SET_HISTORY),
        setAcc: createAction(ActionTypes.MESSENGER.SET_ACC),
        setUserId: createAction(ActionTypes.MESSENGER.SET_USER_ID)
    },
    showMsg: createAction(ActionTypes.SHOW_MSG),
    showErr: createAction(ActionTypes.SHOW_ERR),
    hideMsg: createAction(ActionTypes.HIDE_MSG),
    hideErr: createAction(ActionTypes.HIDE_ERR)
};
