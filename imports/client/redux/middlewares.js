import { ActionTypes } from "./actions";
import { log } from "../../common/logger/Logger";
import { LOAD, SAVE } from 'redux-storage';

// middleware allows you to do something in between the dispatch
// and handing it off to the reducer

// console.log our state changes
export const loggerMiddleware = store => next => action => {
    let skip = action.type == SAVE || action.type == ActionTypes.TASKS.UPDATE || action.type == ActionTypes.TASKS.UPDATED || !__debug_redux;
    if (!skip)
        log.debug('[Dispatching] {1}', action);
    // essentially call 'dispatch'
    let result = next(action);
    if (!skip)
        log.debug('[Store] {1}', store.getState());
    return result;
};

export const __debug_redux = true;