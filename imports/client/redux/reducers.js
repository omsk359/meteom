import { ActionTypes } from "./actions";
import { TaskActionTypes } from "../../common/types";
import { TaskFilterTypes } from "../../common/types";
import { IdsSourceTypes } from "../../common/types";
import moment from 'moment';
import { StdUserIdLists } from "../../common/types";
import { TasksCreatorRefs as Refs } from "../components/TasksCreator/TasksCreator";
import { merge } from '../../common/helpers';
import * as _ from 'lodash';

var Reducers = {};

Reducers.taskCreator = (state = {
    history: [Refs.FirstPage],
    hidden: true,
    switchToParams: null, // temp place to pass params to the curr page
    //idsIterateTask: undefined,
    //taskScheduler: null // to share current view settings. A view modify it, and all params will be merged with this scheduler before task create
}, action) => {
    const types = ActionTypes.TASK_CREATOR;
    switch (action.type) {
        case types.SWITCH_TO:
            let { ref, params } = action.payload;
            if (_.includes(state.history, ref))
                var history = state.history.slice(0, state.history.indexOf(ref) + 1);
            else
                history = [...state.history, ref];
            return { ...state, history, switchToParams: params };
        case types.SWITCH_BACK:
            let currView = _.last(state.history), newHistory = _.initial(state.history), prevView = _.last(newHistory);
            let newState = { ...state, history: newHistory };
            //if (action.payload === undefined)
                return newState;
            //switch (currView) {
            //    case Refs.IdsEdit:
            //        return { ...newState, ids: action.payload };
            //    case Refs.AccEdit:
            //        let { accIds, selectedList } = action.payload;
            //        return { ...newState, accs: accIds, accList: selectedList };
            //    case Refs.Delays:
            //        return { ...newState, delays: action.payload };
            //    case Refs.TaskScheduler:
            //        return { ...newState, taskScheduler: { ...state.taskScheduler, ...action.payload }};
            //    default:
            //        return newState;
            //}
        case types.SET_TASK_SCHEDULER:
            return { ...state, taskScheduler: action.payload };
        case types.SET_HIDDEN:
            return { ...state, hidden: action.payload };
        default:
            return state;
    }
};

const { INVITE, SEND_MSG, ADD_TO_USER_ID_LISTS, REMOVE_FROM_USER_ID_LISTS, ADD_TO_OLD_INVITE_DB, NOPE, DELAY } = TaskActionTypes;
const { USER_ID_LISTS, NOT_IN_OLD_INVITE_DB, LAST_SEEN } = TaskFilterTypes;



function checkIdsIterateReady(state) {
    const { INVITE, SEND_MSG } = TaskActionTypes;
    let isReady = {
        idsSource:
            //state.idsSource.type == IdsSourceTypes.IDS_COMMON_LIST && _.size(state.idsSource.userIdList) ||
            state.idsSource.type == IdsSourceTypes.IDS_COMMON_LIST && state.idsSource.userIdListName ||
            state.idsSource.type == IdsSourceTypes.IDS_COPY_LIST && state.idsSource.userIdListName ||
            state.idsSource.type == IdsSourceTypes.SEARCH_URLS && _.size(state.idsSource.urls) ||
            _.includes([IdsSourceTypes.FRIENDS, IdsSourceTypes.FRIEND_REQUESTS_IN, IdsSourceTypes.FRIEND_REQUESTS_OUT], state.idsSource.type),
        [INVITE]: state.params[INVITE].idsOkCntMax > 0,
        [SEND_MSG]: state.params[SEND_MSG].idsOkCntMax > 0
    };
    _.assign(isReady, {
        idsIterateTask: _.size(state.accs) && _.size(state.actions) && isReady.idsSource && _.every(_.pick(isReady, state.actions))
    });
    if (_.isEqual(state.isReady, isReady))
        return state;
    return { ...state, isReady }
}

export const IDS_ITERATE = {
    COMMON_FILTERS: 'IDS_ITERATE_COMMON_FILTERS',
    SELECTED_ACTIONS: 'IDS_ITERATE_SELECTED_ACTIONS'
};

// onActions: { 'invite' -> { onOk: ['addToInviteDB'], onFail: [] }, 'sendMsg,like' -> { onOk: [] } }
Reducers.idsIterateTaskCreator = (state = {
    history: [Refs.FirstPage], // copy of taskCreator.history need to get prev state after modify the taskCreator
    //delays: { 0: [5, 10], 1: [10, 30], 3: [50, 100], 5: [100, 300] },
    delays: { 0: [5, 10], 1: [10, 30] },
    idsSource: { type: IdsSourceTypes.IDS_COMMON_LIST, urls: [], userIdListName: undefined, resetLastParams: false, listInfLoop: false },
    actions: [], isReady: {}, filters: {
        [IDS_ITERATE.COMMON_FILTERS]: []
        //[INVITE]: [ USER_ID_LISTS, NOT_IN_OLD_INVITE_DB ]
        //[INVITE]: [  ]
    },
    params: {
        [INVITE]: { idsOkCntMax: 48, msgEnable: false, msg: '' },
        [SEND_MSG]: { idsOkCntMax: 100, msgs: [] },
        [NOPE]: { idsOkCntMax: 500 },
        [ADD_TO_USER_ID_LISTS]: {
            onOK: [], onFail: []
            //[INVITE]: [StdUserIdLists.INVITE_LIST],
            //[SEND_MSG]: [StdUserIdLists.FIRST_MSG_LIST]
        },
        [REMOVE_FROM_USER_ID_LISTS]: {
            onOK: [], onFail: []
        },
        // filters params
        //[LAST_SEEN]: { date: moment().subtract(2, 'days').minutes(0).seconds(0).toDate() },
        [LAST_SEEN]: { date: moment().subtract(2, 'days').startOf('hour').toDate() },
        [USER_ID_LISTS]: []
        //[USER_ID_LISTS]: {
            //[INVITE]: [StdUserIdLists.INVITE_LIST],
            //[SEND_MSG]: [StdUserIdLists.FIRST_MSG_LIST]
        //}
    },
    onActions: {
        [IDS_ITERATE.SELECTED_ACTIONS]: { onOK: [ADD_TO_USER_ID_LISTS, REMOVE_FROM_USER_ID_LISTS, DELAY], onFail: [ADD_TO_USER_ID_LISTS, REMOVE_FROM_USER_ID_LISTS, DELAY] },

        //[INVITE]: { onOK: [DELAY, ADD_TO_USER_ID_LISTS, ADD_TO_OLD_INVITE_DB, DELAY, DELAY, DELAY] },
        //[SEND_MSG]: { onOK: [ADD_TO_USER_ID_LISTS, DELAY] },
        [INVITE]: { onOK: [ADD_TO_OLD_INVITE_DB] },
        [SEND_MSG]: { onOK: [] },
        [NOPE]: { onOK: [] }
    },
    accs: [], accList: '',
    taskScheduler: { enabled: false }
}, action) => {
    state = checkIdsIterateReady(state);
    const types = merge(ActionTypes.IDS_ITERATE_TC, ActionTypes.TASK_CREATOR);
    switch (action.type) {
        case types.SET_IDS_SOURCE:
            //return {...state, idsSource: {...state.idsSource, type: action.payload}};
            return {...state, idsSource: _.merge(state.idsSource, action.payload)};
        case types.ADD_ACTION:
            _.defaultsDeep(state.onActions, { [action.payload]: {onOK: [], onFail: []} });
            if (action.payload == INVITE) {
                //state.onActions[action.payload].onOK.push(TaskActionTypes.ADD_TO_INVITE_DB);
                //state.actions.push(TaskActionTypes.ADD_TO_INVITE_DB);
                //state.filters.push(NOT_IN_INVITE_DB);
                //state.filters.push(NOT_IN_OLD_INVITE_DB);
            }
            return {...state, actions: [...state.actions, action.payload]};
        case types.REMOVE_ACTION:
            if (action.payload == INVITE) {
                //state.filters = _.without(state.filters, NOT_IN_INVITE_DB, NOT_IN_OLD_INVITE_DB);
                //state.actions = _.without(state.actions, TaskActionTypes.ADD_TO_INVITE_DB);
            }
            return {...state, actions: _.without(state.actions, action.payload)};
        case types.SET_ON_ACTION:
            var newState = {...state};
            _.defaults(action.payload, { on: 'onOK', enable: true });
            let { on, enable, causeAction } = action.payload, taskAction = action.payload.action;
            _.defaultsDeep(newState.onActions, { [causeAction]: { [on]: [] } });
            let onActions = newState.onActions[causeAction][on];
            if (enable) {
                if (!_.includes(onActions, taskAction))
                    onActions.push(taskAction);
            } else
                _.pull(onActions, taskAction);
            if (_.includes(onActions, DELAY)) {
                _.pull(onActions, DELAY);
                onActions.push(DELAY);
            }
            return newState;
        case types.SET_FILTER:
            let aType = action.payload.action, { filter } = action.payload;
            if (!aType) aType = IDS_ITERATE.COMMON_FILTERS;
            let filters = state.filters[aType];
            if (action.payload.enable)
                filters = _.compact(_.uniq([...filters, filter]));
            else
                filters = _.without(filters, filter);
            return {...state, filters: {...state.filters, [aType]: filters}};
        case types.SET_PARAMS:
            return {...state, params: _.merge(state.params, action.payload)};
        case types.SET_TASK_SCHEDULER:
            return {...state, taskScheduler: action.payload};
        case types.SET_DELAYS:
            return { ...state, delays: action.payload };
        case types.SWITCH_TO:
            let { ref, params } = action.payload;
            if (_.includes(state.history, ref))
                var history = state.history.slice(0, state.history.indexOf(ref) + 1);
            else
                history = [...state.history, ref];
            return { ...state, history };
        case types.SWITCH_BACK:
            let currView = _.last(state.history), newHistory = _.initial(state.history), prevView = _.last(newHistory);
            newState = { ...state, history: newHistory };
            if (action.payload === undefined || !_.includes([Refs.IdsIterateTask, Refs.InviteTaskConfig, Refs.SendMsgConfig, Refs.IdsIterateOnActions,
                    Refs.UserIdListsAddOnOk, Refs.UserIdListsRemoveOnOk, Refs.UserIdListsAddOnFail, Refs.UserIdListsRemoveOnFail], prevView))
                return newState;
            let addListNames = newState.params[ADD_TO_USER_ID_LISTS];
            let removeListNames = newState.params[REMOVE_FROM_USER_ID_LISTS];
            //let listNamesFilters = newState.params[USER_ID_LISTS];
            switch (currView) {
                case Refs.AccEdit:
                    let { accIds, selectedList } = action.payload;
                    return { ...newState, accs: accIds, accList: selectedList };
                case Refs.IdsIterateDelays:
                    return { ...newState, delays: action.payload };
                case Refs.SearchUrlsEdit:
                    return { ...newState, idsSource: {...state.idsSource, ...action.payload} };
                case Refs.IdLists:
                    return { ...newState, idsSource: {...state.idsSource, userIdListName: action.payload.selectedList} };
                case Refs.LastSeen:
                    return { ...newState, params: { ...state.params, ...merge(state.params[LAST_SEEN], { date: action.payload }) }};
                case Refs.InviteMsgEdit:
                    return { ...newState, params: _.merge(state.params, {[INVITE]: { msg: action.payload.join('\n') }}) };
                case Refs.SendMsgEdit:
                    newState.params[SEND_MSG].msgs = action.payload;
                    return newState;
                //case Refs.UserIdListsInvite:
                //    listNames[INVITE] = action.payload;
                //    return newState;
                //case Refs.UserIdListsInviteFilters:
                //    listNamesFilters[INVITE] = action.payload;
                //    return newState;
                //case Refs.UserIdListsSendMsg:
                //    listNames[SEND_MSG] = action.payload;
                //    return newState;
                case Refs.UserIdListsFilters:
                    newState.params[USER_ID_LISTS] = action.payload;
                    return newState;
                case Refs.UserIdListsAddOnOk:
                    addListNames.onOK = action.payload;
                    return newState;
                case Refs.UserIdListsAddOnFail:
                    addListNames.onFail = action.payload;
                    return newState;
                case Refs.UserIdListsRemoveOnOk:
                    removeListNames.onOK = action.payload;
                    return newState;
                case Refs.UserIdListsRemoveOnFail:
                    removeListNames.onFail = action.payload;
                    return newState;
                default:
                    return newState;
            }
        default:
            return state;
    }
};

Reducers.taskScheduler = (state = {
    enabled: false,
    cron: '* */10 * * * *'
}, action) => {
    const types = ActionTypes.TASK_SCHEDULER_TC;
    switch (action.type) {
        case types.SET_PARAMS:
            return { ...state, ...action.payload };
        default:
            return state;
    }
};


Reducers.dialogsHistoryTaskCreator = (state = {
    history: [Refs.FirstPage],
    accs: [], accList: '',
    taskScheduler: { enabled: true, cron: '* */5 * * * *' }
}, action) => {
    const types = merge(ActionTypes.DIALOGS_HISTORY_TC, ActionTypes.TASK_CREATOR);
    switch (action.type) {
        case types.SET_PARAMS:
            return { ...state, ...action.payload };
        case types.SWITCH_TO:
            let { ref, params } = action.payload;
            if (_.includes(state.history, ref))
                var history = state.history.slice(0, state.history.indexOf(ref) + 1);
            else
                history = [...state.history, ref];
            return { ...state, history };
        case types.SWITCH_BACK:
            let currView = _.last(state.history), newHistory = _.initial(state.history), prevView = _.last(newHistory);
            let newState = { ...state, history: newHistory };
            if (action.payload === undefined || prevView != Refs.DialogsHistoryTask)
                return newState;
            switch (currView) {
                case Refs.AccEdit:
                    let { accIds, selectedList } = action.payload;
                    return { ...newState, accs: accIds, accList: selectedList };
                default:
                    return newState;
            }
        default:
            return state;
    }
};

Reducers.regAccsTaskCreator = (state = {
    history: [Refs.FirstPage],
    proxies: [], poolSize: 2, totalCnt: 3,
    firstNames: [], lastNames: [], gender: 2, pass: '',
    useSimSms: true, useSmsVk: false, useSmsLike: false, useSmsArea: false,
    taskScheduler: { enabled: false }
}, action) => {
    const types = merge(ActionTypes.REG_ACCS_TC, ActionTypes.TASK_CREATOR);
    switch (action.type) {
        case types.SET_PARAMS:
            return { ...state, ...action.payload };
        case types.SWITCH_TO:
            let { ref, params } = action.payload;
            if (_.includes(state.history, ref))
                var history = state.history.slice(0, state.history.indexOf(ref) + 1);
            else
                history = [...state.history, ref];
            return { ...state, history };
        case types.SWITCH_BACK:
            let currView = _.last(state.history), newHistory = _.initial(state.history), prevView = _.last(newHistory);
            let newState = { ...state, history: newHistory };
            if (action.payload === undefined || prevView != Refs.RegAccsTask)
                return newState;
            //if (action.payload === undefined || !_.includes([Refs.RegAccsTask, Refs.RegAccsProxiesEdit, Refs.RegAccsFirstNames, Refs.RegAccsLastNames], prevView))
            //    return newState;
            switch (currView) {
                case Refs.RegAccsProxiesEdit:
                    return { ...newState, proxies: action.payload };
                case Refs.RegAccsFirstNames:
                    return { ...newState, firstNames: action.payload };
                case Refs.RegAccsLastNames:
                    return { ...newState, lastNames: action.payload };
                default:
                    return newState;
            }
        default:
            return state;
    }
};

Reducers.renameAccsTaskCreator = (state = {
    history: [Refs.FirstPage],
    changeName: false, firstNames: [], lastNames: [], shuffleNames: true,
    changeAge: false, ageFrom: 19, ageTo: 25,
    changeBdateVisibility: false, bdateVisibility: 0,
    changeGender: false, gender: 2,
    changeRelation: false, relation: 0,
    changeHomeTown: false, homeTown: '',
    changeStatus: false, status: '',
    clear: false, clearParams: {
        avatars: true, wall: true, messages: true, info: true, friends: true,
        friendRequests: true, leaveGroups: true, video: true, photoAlbums: true,
        music: true, groupInvites: true, photo: true,
        delay: { enable: true, range: [1, 1] }
    },
    accs: [], accList: '',
    taskScheduler: { enabled: false }
}, action) => {
    const types = merge(ActionTypes.RENAME_ACCS_TC, ActionTypes.TASK_CREATOR);
    switch (action.type) {
        case types.SET_PARAMS:
            return { ...state, ...action.payload };
        case types.SWITCH_TO:
            let { ref, params } = action.payload;
            if (_.includes(state.history, ref))
                var history = state.history.slice(0, state.history.indexOf(ref) + 1);
            else
                history = [...state.history, ref];
            return { ...state, history };
        case types.SWITCH_BACK:
            let currView = _.last(state.history), newHistory = _.initial(state.history), prevView = _.last(newHistory);
            let newState = { ...state, history: newHistory };
            if (action.payload === undefined || prevView != Refs.RenameAccsTask)
                return newState;
            switch (currView) {
                case Refs.RenameAccsFirstNames:
                    return { ...newState, firstNames: action.payload };
                case Refs.RenameAccsLastNames:
                    return { ...newState, lastNames: action.payload };
                case Refs.AccEdit:
                    let { accIds, selectedList } = action.payload;
                    return { ...newState, accs: accIds, accList: selectedList };
                case Refs.ClearAcc:
                    return { ...newState, clearParams: action.payload };
                default:
                    return newState;
            }
        default:
            return state;
    }
};

Reducers.unfreezeAccsTaskCreator = (state = {
    history: [Refs.FirstPage],
    useSimSms: false, useSmsVk: false, useSmsLike: false, useSmsArea: false,
    accs: [], accList: '',
    taskScheduler: { enabled: false }
}, action) => {
    const types = merge(ActionTypes.UNFREEZE_ACCS_TC, ActionTypes.TASK_CREATOR);
    switch (action.type) {
        case types.SET_PARAMS:
            return { ...state, ...action.payload };
        case types.SWITCH_TO:
            let { ref, params } = action.payload;
            if (_.includes(state.history, ref))
                var history = state.history.slice(0, state.history.indexOf(ref) + 1);
            else
                history = [...state.history, ref];
            return { ...state, history };
        case types.SWITCH_BACK:
            let currView = _.last(state.history), newHistory = _.initial(state.history), prevView = _.last(newHistory);
            let newState = { ...state, history: newHistory };
            if (action.payload === undefined || prevView != Refs.UnfreezeAccsTask)
                return newState;
            switch (currView) {
                case Refs.AccEdit:
                    let { accIds, selectedList } = action.payload;
                    return { ...newState, accs: accIds, accList: selectedList };
                default:
                    return newState;
            }
        default:
            return state;
    }
};

Reducers.bindPhoneTaskCreator = (state = {
    history: [Refs.FirstPage],
    useSimSms: false, useSmsVk: false, useSmsLike: false, useSmsArea: false,
    accs: [], accList: '',
    taskScheduler: { enabled: false }
}, action) => {
    const types = merge(ActionTypes.BIND_PHONE_TC, ActionTypes.TASK_CREATOR);
    switch (action.type) {
        case types.SET_PARAMS:
            return { ...state, ...action.payload };
        case types.SWITCH_TO:
            let { ref, params } = action.payload;
            if (_.includes(state.history, ref))
                var history = state.history.slice(0, state.history.indexOf(ref) + 1);
            else
                history = [...state.history, ref];
            return { ...state, history };
        case types.SWITCH_BACK:
            let currView = _.last(state.history), newHistory = _.initial(state.history), prevView = _.last(newHistory);
            let newState = { ...state, history: newHistory };
            if (action.payload === undefined || prevView != Refs.BindPhoneTask)
                return newState;
            switch (currView) {
                case Refs.AccEdit:
                    let { accIds, selectedList } = action.payload;
                    return { ...newState, accs: accIds, accList: selectedList };
                default:
                    return newState;
            }
        default:
            return state;
    }
};


Reducers.idsCollectTaskCreator = (state = {
    history: [Refs.FirstPage],
    searchUrlParams: { urls: [], resetLastParams: true },
    accs: [], accList: '',
    idsListName: '',
    algorithm: 'parallel',
    taskScheduler: { enabled: false }
}, action) => {
    const types = merge(ActionTypes.IDS_COLLECT_TC, ActionTypes.TASK_CREATOR);
    switch (action.type) {
        case types.SET_PARAMS:
            return { ...state, ...action.payload };
        case types.SWITCH_TO:
            let { ref, params } = action.payload;
            if (_.includes(state.history, ref))
                var history = state.history.slice(0, state.history.indexOf(ref) + 1);
            else
                history = [...state.history, ref];
            return { ...state, history };
        case types.SWITCH_BACK:
            let currView = _.last(state.history), newHistory = _.initial(state.history), prevView = _.last(newHistory);
            let newState = { ...state, history: newHistory };
            if (action.payload === undefined || prevView != Refs.IdsCollectTask)
                return newState;
            switch (currView) {
                case Refs.IdLists:
                    return { ...newState, idsListName: action.payload.selectedList };
                case Refs.AccEdit:
                    let { accIds, selectedList } = action.payload;
                    return { ...newState, accs: accIds, accList: selectedList };
                case Refs.SearchUrlsEdit:
                    return { ...newState, searchUrlParams: action.payload };
                default:
                    return newState;
            }
        default:
            return state;
    }
};

Reducers.taskTable = (state = { selectedTaskId: '', createTaskHidden: true }, action) => {
    const types = ActionTypes.TASK_TABLE;
    switch (action.type) {
        case types.SELECT_TASK:
            return { ...state, selectedTaskId: action.payload };
        default:
            return state;
    }
};

Reducers.accTable = (state = { accList: '' }, action) => {
    const types = ActionTypes.ACC_TABLE;
    switch (action.type) {
        case types.SET_ACC_LIST:
            return { ...state, accList: action.payload };
        default:
            return state;
    }
};

Reducers.taskDetails = (state = { /*taskLogId: ''*/ }, action) => {
    const types = ActionTypes.TASK_DETAILS;
    switch (action.type) {
        //case types.SET_TASK_LOG_ID:
        //    return { ...state, taskLogId: action.payload };
        default:
            return state;
    }
};

Reducers.logger = (state = { streamId: '' }, action) => {
    const types = ActionTypes.LOGGER;
    switch (action.type) {
        case types.SET_STREAM_ID:
        case types.LOAD_FULL_HISTORY:
            return { ...state, streamId: action.payload };
        default:
            return state;
    }
};

Reducers.tasks = (state = { tasks: [] }, action) => {
    const types = ActionTypes.TASKS;
    switch (action.type) {
        case types.UPDATED:
            return { ...state, tasks: action.payload };
        default:
            return state;
    }
};

Reducers.accManager = (state = { lists: {}, selectedList: '' }, action) => {
    const types = ActionTypes.ACC_MANAGER;
    switch (action.type) {
        case types.ADD_LIST:
            return { ...state, lists: { ...state.lists, [action.payload]: [] } };
        case types.REMOVE_LIST:
            return { ...state, lists: _.omit(state.lists, action.payload) };
        case types.SAVE_LIST:
            let { ids, listName } = action.payload;
            return { ...state, lists: { ...state.lists, [listName]: ids } };
        case types.SWITCH_LIST:
            return { ...state, selectedList: action.payload };
        case types.LOAD:
            return { ...state, lists: action.payload };
        default:
            return state;
    }
};

const { INVITE_LIST, FIRST_MSG_LIST } = StdUserIdLists;

Reducers.userIdListManager = (state = {
    lists: {
        [INVITE_LIST]: [], [FIRST_MSG_LIST]: []
    }, selectedList: ''
}, action) => {
    const types = ActionTypes.USER_ID_LISTS_MANAGER;
    switch (action.type) {
        case types.ADD_LIST:
            return { ...state, lists: { ...state.lists, [action.payload]: [] } };
        case types.REMOVE_LIST:
            if (_.includes(StdUserIdLists, action.payload))
                return state;
            return { ...state, lists: _.omit(state.lists, action.payload) };
        case types.SAVE_LIST:
            let { ids, listName } = action.payload;
            return { ...state, lists: { ...state.lists, [listName]: ids } };
        case types.SWITCH_LIST:
            return { ...state, selectedList: action.payload };
        case types.LOAD:
            return { ...state, lists: _.defaults(action.payload, { [INVITE_LIST]: [], [FIRST_MSG_LIST]: [] }) };
        default:
            return state;
    }
};

Reducers.common = (state = { infoMsg: '', errMsg: '' }, action) => {
    switch (action.type) {
        case ActionTypes.SHOW_MSG:
            return { ...state, infoMsg: action.payload };
        case ActionTypes.SHOW_ERR:
            return { ...state, errMsg: action.payload };
        case ActionTypes.HIDE_MSG:
            return { ...state, infoMsg: '' };
        case ActionTypes.HIDE_ERR:
            return { ...state, errMsg: '' };
        default:
            return state;
    }
};

Reducers.messenger = (state = { taskId: '', accs: [], acc: {}, userId: '', dialogs: [], history: [] }, action) => {
    const types = ActionTypes.MESSENGER;
    switch (action.type) {
        case types.SET_TASK_ID:
            return { ...state, taskId: action.payload };
        case types.SET_ACCS:
            return { ...state, accs: action.payload };
        case types.SET_DIALOGS:
            return { ...state, dialogs: action.payload };
        case types.SET_HISTORY:
            return { ...state, history: action.payload };
        case types.SET_ACC:
            return { ...state, acc: action.payload };
        case types.SET_USER_ID:
            return { ...state, userId: action.payload };
        default:
            return state;
    }
};

export default Reducers;
