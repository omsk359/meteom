// https://www.npmjs.com/package/redux-storage

import { createStore, applyMiddleware, combineReducers } from 'redux';
import createEngine from 'redux-storage-engine-localstorage';
import promiseMiddleware from 'redux-promise';
import * as storage from 'redux-storage'
import Reducers from './reducers';
import { TaskActionTypes } from "../../common/types";
import { IdsSourceTypes } from "../../common/types";
import { ActionTypes } from "./actions";
import { loggerMiddleware } from "./middlewares";
import * as _ from 'lodash';
// let { taskCreator, taskTable, idsIterateTaskCreator, dialogsHistoryTaskCreator, idsCollectTaskCreator, bindPhoneTaskCreator,
//     renameAccsTaskCreator, unfreezeAccsTaskCreator, accManager, userIdListManager, common, tasks, taskDetails, logger, messenger, accTable, regAccsTaskCreator } = Reducers;



function checkComponentsReady(baseState, state) {
    //const { accs } = baseState;
    const { INVITE, SEND_MSG } = TaskActionTypes;
    let ready = {
        idsSource:
            //state.idsSource.type == IdsSourceTypes.IDS_COMMON_LIST && _.size(state.idsSource.userIdList) ||
            state.idsSource.type == IdsSourceTypes.IDS_COMMON_LIST && state.idsSource.userIdListName ||
            state.idsSource.type == IdsSourceTypes.IDS_COPY_LIST && state.idsSource.userIdListName ||
            state.idsSource.type == IdsSourceTypes.SEARCH_URLS && _.size(state.idsSource.urls) ||
            state.idsSource.type == IdsSourceTypes.FRIENDS,
        [INVITE]: state.params[INVITE].idsOkCntMax > 0,
        [SEND_MSG]: state.params[SEND_MSG].idsOkCntMax > 0
    };
    _.assign(ready, {
        idsIterateTask: _.size(state.accs) && _.size(state.actions) && ready.idsSource && _.every(_.pick(ready, state.actions))
    });
    return ready;
}

//function taskCreatorCombine(state, action) {
//    let taskCreatorState = taskCreator(state, action);
//    let idsIterateState = idsIterateTaskCreator(taskCreatorState.idsIterateState, action);
//
//    let isReady = checkComponentsReady(taskCreatorState, idsIterateState);
//    if (!_.isEqual(idsIterateState.isReady, isReady))
//        idsIterateState = {...idsIterateState, isReady};
//
//    if (taskCreatorState == state && state.idsIterateState == idsIterateState)
//        return state;
//    if (taskCreatorState == state)
//        taskCreatorState = _.clone(taskCreatorState);
//    taskCreatorState.idsIterateState = idsIterateState;
//    return taskCreatorState;
//}

// export const myReducer = (state = { arr: [1, 2, 3] }, action) => action.type == 'TEST' ? { arr: [5] } : state;


// storage

const engineKey = 'myStorageKey';

// const createEngine = (key = engineKey) => ({
//     load() {
//         const jsonState = localStorage.getItem(key);
//         return Promise.resolve(JSON.parse(jsonState) || {});
//     },
//     save(state) {
//         const jsonState = JSON.stringify(state);
//         localStorage.setItem(key, jsonState);
//         return Promise.resolve();
//     }
// });

export const clearStorage = (key = engineKey) => {
    localStorage.removeItem(key);
};

// Now it's time to decide which storage engine should be used
//
// Note: The arguments to `createEngine` are different for every engine!
let reducer = combineReducers(Reducers);
reducer = storage.reducer(reducer);
const engine = createEngine(engineKey);

// And with the engine we can create our middleware function. The middleware
// is responsible for calling `engine.save` with the current state afer
// every dispatched action.
//
// Note: You can provide a list of action types as second argument, those
//       actions will be filtered and WON'T trigger calls to `engine.save`!
const storageMiddleware = storage.createMiddleware(engine, [ActionTypes.TASKS.UPDATED, ActionTypes.TASKS.UPDATE]);

// As everything is prepared, we can go ahead and combine all parts as usual
//const createStoreWithMiddleware = applyMiddleware(middleware)(createStore);
//const store = createStoreWithMiddleware(reducer);

//const logger = createLogger();
const createStoreWithMiddleware = applyMiddleware(promiseMiddleware, loggerMiddleware, storageMiddleware/*, logger*/)(createStore);
//const createStoreWithMiddleware = applyMiddleware(loggerMiddleware, promiseMiddleware)(createStore);

const store = createStoreWithMiddleware(reducer);
export default store;
STORE = store; // for debug

// At this stage the whole system is in place and every action will trigger
// a save operation.
//
// BUT (!) an existing old state HAS NOT been restored yet! It's up to you to
// decide when this should happen. Most of the times you can/should do this
// right after the store object has been created.

// To load the previous state we create a loader function with our prepared
// engine. The result is a function that can be used on any store object you
// have at hand :)
const load = storage.createLoader(engine);
//load(store);

// Notice that our load function will return a promise that can also be used
// to respond to the restore event.
load(store)
    .then(newState => {
        console.log('Loaded state:', newState);
    }).catch(() => console.log('Failed to load previous state'));
