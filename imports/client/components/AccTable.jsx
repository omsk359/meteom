import React from 'react';
import * as _ from 'lodash';
import store from '../redux/store';
import { connect, Provider } from 'react-redux';
import { log } from "../../common/logger/Logger";
import { TasksCreatorRefs as Refs } from "./TasksCreator/TasksCreator";
import AccEditManager from "./AccEdit";
import { DropDownMenu, MenuItem, Table, TableHeader, TableBody,
         TableRow, TableHeaderColumn, TableRowColumn
} from 'material-ui';

export class AccTable extends React.Component {
    //mixins: [ReactMeteorData],
    //getMeteorData() {
    //    var data = {};
    //    var handle = Meteor.subscribe('userData');
    //    if (handle.ready()) {
    //        var accManager = new AccManager();
    //        data.accs = accManager.get(this.props.accListName);
    //        log.debug('accs table: {1}', data.accs);
    //    }
    //    return data;
    //},
    render() {
        //var unreads = getUserData('DialogsHistoryTask') || {};
        let lists = _.map(listNames, listName => ({ payload: listName, text: listName }));
        return (
            <div className="acc-table-box">
                <DropDownMenu value={selectedList} onChange={(e, i, item) => onSwitchList(item)}>
                    { lists.map((item, i) => (
                        <MenuItem key={i} value={item.payload} primaryText={item.text} />
                    )) }
                </DropDownMenu>
                <AccEditManager ref={Refs.AccEdit} {...accManagerStore} onFinished={goBack} />
                <Table height="300px" selectable={false}>
                    <TableHeader displaySelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn tooltip='Account'>Acc</TableHeaderColumn>
                            <TableHeaderColumn tooltip='Proxy'>Proxy</TableHeaderColumn>
                            <TableHeaderColumn tooltip='Status'>State</TableHeaderColumn>
                            <TableHeaderColumn tooltip='Unread'>Unread</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        { _.map(this.data.accs, acc => (
                            <TableRow key={acc.login}>
                                <TableRowColumn>{acc.login + ':' + acc.pass}</TableRowColumn>
                                <TableRowColumn>{acc.proxy.ip + ':' + acc.proxy.port}</TableRowColumn>
                                <TableRowColumn>{acc.getState()}</TableRowColumn>
                                <TableRowColumn>{unreads[acc.login]}</TableRowColumn>
                            </TableRow>
                        )) }
                    </TableBody>
                </Table>
            </div>
        )
    }
}
AccTable.defaultProps = {
    accListName: 'main'
};
AccTable.propTypes = {
    accListName: React.PropTypes.string
};

const AccTableContainer = props => (
    <Provider store={store}>
        <AccTableConnected />
    </Provider>
);

export default AccTableContainer
// export default class AccTableContainer extends React.Component {
//     render() {
//         return (
//             <Provider store={store}>
//                 <AccTableConnected />
//             </Provider>
//         );
//     }
// }

const AccTableConnected = connect(state => ({
    ...state.accTable, ...state.common, ...state.tasks, accManagerStore: state.accManager
}))(AccTable);
