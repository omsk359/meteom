import React from 'react';
import * as _ from 'lodash';
import store from '../redux/store';
import { log } from "../../common/logger/Logger";
import { Actions } from "../redux/actions";
import { TaskRunStates } from "../../common/types";
import { accStateText } from "./helpers";
import AccInfo from "../../common/AccInfo";
import { connect, Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { taskStateText } from "./helpers";
import { browserHistory } from 'react-router';
import LogArea from "./LogArea";
import { getCtrlMenu } from './TaskBoard';
import { Drawer, TextField, Table, TableHeader, TableBody, TableRow,
    TableHeaderColumn, TableRowColumn, FlatButton, IconButton, FontIcon
} from 'material-ui';

export class MultiTaskDetails extends React.Component {
    componentDidMount() {
        //this.showLog(_.get(this.props.taskData, '_id'));
        this.showLog('');
        //store.dispatch(Actions.taskDetails.setTaskLogId(_.get(this.props.taskData, '_id')));
    }
    showLog(taskLogId) {
        store.dispatch(Actions.logger.setStreamId(taskLogId));
    }
    loadLogHistory(taskLogId) {
        store.dispatch(Actions.logger.loadFullHistory(taskLogId));
    }
    startTask(taskId) {
        store.dispatch(Actions.tasks.start(taskId));
    }
    render() {
        MULTI_TASK = this; // !!!!debug
        GET_CM = getCtrlMenu;
        let taskData = this.props.taskData || {}, subTasks = _.get(taskData, 'data.tasks');

        const { STARTED, STOPPED, PAUSED, FINISHED, WAIT, RESUME } = TaskRunStates;
        var runState = _.get(taskData, 'data.state.runState');

        return (
            <div className="tasks-table-box">
                <Table height="300px" selectable={false}>
                    <TableHeader displaySelectAll={false} className="task-table-header">
                        <TableRow>
                            { _.map(this.props.columns, (column, i) => {
                                return (
                                    <TableHeaderColumn key={i} style={{textAlign: 'center'}} tooltip={column.header.tooltip}>{column.header.text}</TableHeaderColumn>
                                )
                            })}
                            <TableHeaderColumn style={{textAlign: 'center'}} tooltip='Состояние задачи'>Состояние задачи</TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}} tooltip='Запуск/пауза/стоп'>Управление</TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}} tooltip='Текущий лог'>Лог</TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}} tooltip='Загузить полную историю выполнения задачи'>Полный лог</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        <TableRow key={taskData._id}>
                            { _.map(this.props.columns, (column, i) => {
                                return (
                                    <TableHeaderColumn key={i} style={{textAlign: 'center'}}>{column.taskRow(taskData)}</TableHeaderColumn>
                                )
                            })}
                            <TableRowColumn style={{textAlign: 'center'}}>{taskStateText(runState)}</TableRowColumn>


                            <TableRowColumn style={{textAlign: 'center'}}>{getCtrlMenu(taskData._id, runState)}</TableRowColumn>

                            <TableRowColumn style={{textAlign: 'center'}}><FlatButton disabled={runState != STARTED && runState != PAUSED} label="Показать" primary={true} onClick={() => this.showLog(taskData._id)} /></TableRowColumn>
                            <TableRowColumn style={{textAlign: 'center'}}><FlatButton label="Загрузить" primary={true} onClick={() => this.loadLogHistory(taskData._id)} /></TableRowColumn>
                        </TableRow>

                        { _.map(subTasks, task => {
                            var runState = _.get(task, 'data.state.runState');

                            return (
                            <TableRow key={task._id}>
                                { _.map(this.props.columns, (column, i) => {
                                    return (
                                        <TableHeaderColumn key={i} style={{textAlign: 'center'}}>{column.subTaskRow(task)}</TableHeaderColumn>
                                    )
                                })}
                                <TableRowColumn style={{textAlign: 'center'}}>{taskStateText(runState)}</TableRowColumn>
                                <TableRowColumn style={{textAlign: 'center'}}>
                                    {' ' || <IconButton onClick={actionFunc[runState].bind(this, task._id)}>
                                        <FontIcon className="material-icons">{actionIcons[runState]}</FontIcon>
                                    </IconButton>}
                                </TableRowColumn>
                                <TableRowColumn style={{textAlign: 'center'}}><FlatButton disabled={runState != STARTED && runState != PAUSED} label="Показать" primary={true} onClick={() => this.showLog(task._id)} /></TableRowColumn>
                                <TableRowColumn style={{textAlign: 'center'}}><FlatButton label="Загрузить" primary={true} onClick={() => this.loadLogHistory(task._id)} /></TableRowColumn>
                            </TableRow>
                                )
                            })}
                    </TableBody>
                </Table>
                <div style={{margin: '0 auto', display: 'flex'}}>
                    {this.props.elements}
                </div>
                <div style={{clear: 'both'}}>
                    <LogArea {...this.props.loggerState} />
                </div>
            </div>
        )
    }
}
MultiTaskDetails.propTypes = {
    taskData: React.PropTypes.object.isRequired,
    columns: React.PropTypes.array.isRequired,
    elements: React.PropTypes.array
};


export class TaskDetails extends React.Component {
    render() {
        let { taskId, tasks } = this.props;
        let taskData = _.find(tasks, { _id: taskId });
        if (!taskData) return;
        let columns = [], elements = [], subTasks = _.get(taskData, 'data.tasks');

        let accLoginColumn = {
            header: { text: 'Логин', tooltip: 'Логин аккаунта' },
            taskRow: () => <b>Общая задача</b>,
            subTaskRow: subTask => _.get(subTask, 'data.acc.login')
        };
        let accLoginPassColumn = {
            header: { text: 'Логин/пароль', tooltip: 'Логин/пароль аккаунта' },
            taskRow: () => <b>Общая задача</b>,
            subTaskRow: subTask => {
                let { login, pass } = _.get(subTask, 'data.acc') || {};
                if (login)
                    return `${login}:${pass}`;
            }
        };
        let accStateColumn = {
            header: { text: 'Состояние аккаунта', tooltip: 'Состояние аккаунта' },
            taskRow: () => '',
            subTaskRow: subTask => accStateText(_.get(subTask, 'data.acc.state'))
        };
        columns = [accLoginColumn, accStateColumn];

        switch (taskData.taskName) {
            case 'IdsIterateTask':
                let okTotalCol = {
                    header: { text: 'Oк/Всего', tooltip: 'Успешно отправлено/Всего ID' },
                    taskRow: () => '',
                    subTaskRow: subTask => {
                        let { idsOkCnts, idsOkCntsMax } = subTask.data.state;
                        idsOkCnts = _.get(_.values(idsOkCnts), '[0]') || {};
                        idsOkCntsMax = _.get(_.values(idsOkCntsMax), '[0]');
                        let { ok } = _.defaults(idsOkCnts, {ok: idsOkCnts ? idsOkCnts.ok : '?'});
                        return `${ok}/${idsOkCntsMax}`;
                    }
                };
                let failCol = {
                    header: { text: 'Ошибки', tooltip: 'Ошибки' },
                    taskRow: () => '',
                    subTaskRow: subTask => {
                        let { idsOkCnts } = subTask.data.state;
                        idsOkCnts = _.get(_.values(idsOkCnts), '[0]') || {};
                        return idsOkCnts ? idsOkCnts.fail : '?';
                    }
                };
                columns = _.union(columns, [okTotalCol, failCol]);
                break;
            case 'UnfreezeAccsTask':
                let curLoginPassCol = {
                    header: { text: 'Текущий логин/пасс', tooltip: 'Старый логин/новый пароль' },
                    taskRow: () => '',
                    subTaskRow: subTask => {
                        let { oldLogin, newLogin, newPass } = subTask.data.state;
                        if (oldLogin)
                            return `${oldLogin}:${newPass}`;
                    }
                };
                let nextLoginPassCol = {
                    header: { text: 'Через 24 ч.', tooltip: 'Новый логин/новый пароль' },
                    taskRow: () => '',
                    subTaskRow: subTask => {
                        let { oldLogin, newLogin, newPass } = subTask.data.state;
                        if (oldLogin)
                            return `${newLogin}:${newPass}`;
                    }
                };
                columns = _.union(columns, [curLoginPassCol, nextLoginPassCol]);

                // elements
                let unfreezedSubTasks = _.filter(subTasks, subTask => _.has(subTask, 'data.state.newLogin'));
                let unfreezeLines = _.map(unfreezedSubTasks, subTask => {
                    let { oldLogin, newLogin, oldPass, newPass } = subTask.data.state;
                    return `${oldLogin}:${oldPass}\t${newLogin}:${newPass}`;
                });
                var { smsProviders } = taskData.data.state;
                var providersText = _.map(smsProviders, (p, title) => <div>{title}:  <b>{p.balance}</b> руб, <b>{p.availableCnts}</b> активаций</div>);
                elements = [(
                    <div key="unfreeze1" style={{margin: '0 auto', width: '100%', marginBottom: '20px', marginTop: '5px'}}>
                        <TextField ref="unfText" onFocus={() => {$(ReactDOM.findDOMNode(this.refs.unfText)).find('textarea:eq(1)').select()}}
                                   fullWidth={true} floatingLabelText={`Разморожено: ${unfreezeLines.length}`} multiLine={true} rowsMax={2} value={unfreezeLines.join('\n')} />
                    </div>
                ), (
                    <div key="unfreeze2" style={{margin: '0 auto', width: '100%', marginBottom: '20px', marginTop: '30px', marginLeft: '40px'}}>
                        <div style={{float: 'left', paddingRight: '15px'}}><b>Доступно:</b></div>
                        <div style={{float: 'left'}}>{providersText}</div>
                    </div>
                )];
                break;
            case 'RegAccsTask':
                var doneCol = {
                    header: { text: 'Выполнено', tooltip: 'Зарегистрировано акков/Всего' },
                    taskRow: () => {
                        let total_i = 0, total_n = 0;
                        _.each(subTasks, subTask => {
                            let { i, n } = _.get(subTask, 'data.state') || {};
                            total_i += i || 0;
                            total_n += n || 0;
                        });
                        return <b>{`${total_i}/${total_n}`}</b>
                    },
                    subTaskRow: subTask => {
                        let { i, n } = _.get(subTask, 'data.state') || {};
                        if (n) return `${i}/${n}`;
                    }
                };

                columns = [doneCol];

                // elements
                let regOkLines = [];
                _.each(subTasks, subTask =>
                    regOkLines = _.union(regOkLines, _.map(_.get(subTask, 'data.accs'), accData =>
                        new AccInfo().deserialize(accData).toString()
                    ))
                );

                //let regOkSubTasks = _.filter(subTasks, subTask => _.has(subTask, 'data.acc.state'));
                //let regOkLines = _.map(regOkSubTasks, subTask => {
                //    let acc = new AccInfo();
                //    acc.deserialize(subTask.data.acc);
                //    return acc.toString();
                //});
                smsProviders = taskData.data.state.smsProviders;
                providersText = _.map(smsProviders, (p, title) => <div key={title}>{title}:  <b>{p.balance}</b> руб, <b>{p.availableCnts}</b> активаций</div>);
                elements = [(
                    <div key="reg1" style={{margin: '0 auto', width: '100%', marginBottom: '20px', marginTop: '5px'}}>
                        <TextField ref="regText" onFocus={() => $(ReactDOM.findDOMNode(this.refs.regText)).find('textarea:eq(1)').select()}
                                   fullWidth={true} floatingLabelText={`Зарегистрированы: ${regOkLines.length}`} multiLine={true} rowsMax={2} value={regOkLines.join('\n')} />
                    </div>
                ), (
                    <div key="reg2" style={{margin: '0 auto', width: '100%', marginBottom: '20px', marginTop: '30px', marginLeft: '40px'}}>
                        <div style={{float: 'left', paddingRight: '15px'}}><b>Доступно:</b></div>
                        <div style={{float: 'left'}}>{providersText}</div>
                    </div>
                )];
                break;
            case 'BindPhoneTask':
                doneCol = {
                    header: { text: 'Новый логин', tooltip: 'Аккаунт привязан к этому номеру' },
                    taskRow: () => {
                        let cnt = _.countBy(subTasks, subTask => _.has(subTask, 'data.state.newLogin'))[true] || 0;
                        return <b>{`${cnt}/${subTasks.length}`}</b>
                    },
                    subTaskRow: subTask => _.get(subTask, 'data.state.newLogin')
                };

                columns = _.union(columns, [doneCol]);

                // elements
                let doneSubTasks = _.filter(subTasks, subTask => _.has(subTask, 'data.state.newLogin'));
                let doneLines = _.map(doneSubTasks, subTask => {
                    let { oldLogin, newLogin } = subTask.data.state;
                    let { pass } = subTask.data.acc;
                    return `${oldLogin}:${pass}\t${newLogin}:${pass}`;
                });

                smsProviders = taskData.data.state.smsProviders;
                providersText = _.map(smsProviders, (p, title) => <div>{title}:  <b>{p.balance}</b> руб, <b>{p.availableCnts}</b> активаций</div>);
                elements = [(
                    <div key="bind1" style={{margin: '0 auto', width: '100%', marginBottom: '20px', marginTop: '5px'}}>
                        <TextField ref="regText" onFocus={() => $(ReactDOM.findDOMNode(this.refs.regText)).find('textarea:eq(1)').select()}
                                   fullWidth={true} floatingLabelText={`Привязаны: ${doneLines.length}`} multiLine={true} rowsMax={2} value={doneLines.join('\n')} />
                    </div>
                ), (
                    <div key="bind2" style={{margin: '0 auto', width: '100%', marginBottom: '20px', marginTop: '30px', marginLeft: '40px'}}>
                        <div style={{float: 'left', paddingRight: '15px'}}><b>Доступно:</b></div>
                        <div style={{float: 'left'}}>{providersText}</div>
                    </div>
                )];
                break;
            case 'IdsCollectTask':
                let doneTotalCol = {
                    header: { text: 'Собрано/Всего', tooltip: 'Собрано ID/Всего ID' },
                    taskRow: () => {
                        let subTask = _.head(subTasks);
                        let { doneCnt, totalCnt } = _.get(subTask, 'data.state') || {};
                        return <b>{`${doneCnt}/${totalCnt}`}</b>
                    },
                    subTaskRow: () => ''
                };
                let doneUrlsCol = {
                    header: { text: 'Ссылоки/Всего', tooltip: 'Ссылок обработано/Всего ссылок' },
                    taskRow: () => {
                        let subTask = _.head(subTasks);
                        let { url_i } = _.get(subTask, 'data.state') || {};
                        let { urls } = _.get(taskData, 'data.state.params.idsSearchParams') || {};
                        return <b>{`${url_i || 0}/${_.size(urls) || '??'}`}</b>
                    },
                    subTaskRow: () => ''
                };
                if (_.get(taskData, 'data.state.params.algorithm') == 'parallel')
                    columns = _.union(columns, [doneTotalCol, doneUrlsCol]);
                break;
        }
        return <MultiTaskDetails {...this.props} taskData={taskData} columns={columns} elements={elements} />;
    }
}

const TaskDetailsConnected = connect(state => ({
    ...state.common, ...state.tasks, ...state.taskDetails, loggerState: state.logger
}))(TaskDetails);

export default class TaskDetailsContainer extends React.Component {
    componentWillMount() {
        store.dispatch(Actions.tasks.update());//.then(() => this.forceUpdate()));
    }
    render() {
        let taskId = this.props.params.id;
        return (
            <Provider store={store}>
                <TaskDetailsConnected taskId={taskId} />
            </Provider>
        );
    }
}
