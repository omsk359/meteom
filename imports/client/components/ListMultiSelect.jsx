import React from 'react';
import * as _ from 'lodash';
import store from '../redux/store';
import { log } from "../../common/logger/Logger";
import { TasksCreatorRefs as Refs } from "./TasksCreator/TasksCreator";
import { RaisedButton, List, ListItem, Toggle } from 'material-ui';

export default class ListMultiSelect extends React.Component {
    constructor(props) {
        super(props);
        let { all, selected } = this.props;
        this.state = {
            toggle: _.transform(all, (toggle, title) => { toggle[title] = _.includes(selected, title) }, {})
        };
    }
    finished() {
        //let selected = _.keys(_.pickBy(this.state.toggle));
        //let selected = _.invertBy(this.state.toggle)[true];
        let selected = _.transform(this.state.toggle, (selected, toggled, key) => { toggled && selected.push(key) }, []);
        this.props.onFinished(selected);
        log.debug('ListMultiSelect: {1}', selected);
    }
    render() {
        let lines = this.props.lines || [];
        let desc = this.props.desc;
        if (desc) {
            var br = <br />, hr = <hr />;
        }
        let { all } = this.props;
        //all = ['Inbox1', 'Inbox2', 'Inbox3', 'Inbox4'];
        //selected = ['Inbox2'];
        //{ hr }<pre><i>{ desc }{ br }</i></pre><br />
        let items = _.map(all, title =>
            <ListItem key={title} primaryText={title} rightToggle={
                <Toggle toggled={this.state.toggle[title]} onToggle={(a, toggled) => {
                    this.setState({toggle: {...this.state.toggle, [title]: toggled}})
                }} />}
            />
        );
        return (
            <div>
                <b>{this.props.title}</b>
                <List>
                    {items}
                </List>
                { hr }<span><p dangerouslySetInnerHTML={{__html: desc}}></p></span><br />

                <div className="right-btn">
                    <RaisedButton onClick={() => this.props.switchTo(Refs.IdLists)} label="Редактировать списки..." secondary={true} style={{marginRight: '20px'}} />
                    <RaisedButton onClick={() => this.finished()} label="OK" primary={true} />
                </div>
            </div>
        )
    }
}
ListMultiSelect.propTypes = {
    all: React.PropTypes.arrayOf(React.PropTypes.string),
    selected: React.PropTypes.arrayOf(React.PropTypes.string),
    title: React.PropTypes.string,
    onFinished: React.PropTypes.func,
    desc: React.PropTypes.string
};
