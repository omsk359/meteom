import React from 'react';
import { TaskRunStates } from "../../common/types";
import { AccStateType } from "../../common/AccInfo";
import { TextField, DropDownMenu, IconButton, FontIcon, Checkbox } from 'material-ui';

// stack of elements where only one element is visible at a time
export class StackedLayoutIndexes extends React.Component {
    render() {
        let children = React.Children.toArray(this.props.children);
        children = children.filter((child, i) => i == this.props.currentIndex);
        return (
            <div>
                { children }
            </div>
        )
    }
}
StackedLayoutIndexes.defaultProps = {
    currentIndex: 0
};
StackedLayoutIndexes.propTypes = {
    currentIndex: React.PropTypes.number,
    onBack: React.PropTypes.func
};

export class StackedLayoutRefs extends React.Component {
    render() {
        let children = React.Children.toArray(this.props.children);
        children = children.filter(child => child.ref == this.props.currentRef);
        return (
            <div>
                { children }
            </div>
        )
    }
}
StackedLayoutRefs.propTypes = {
    currentRef: React.PropTypes.string.isRequired
};

//IdsEdit extends React.Component {
//    propTypes: {
//        ids: React.PropTypes.array.isRequired,
//        onFinished: React.PropTypes.func.isRequired
//    },
//    save() {
//        let ids = this.refs.ids.getValue();
//        ids = splitStr(ids);
//        this.props.onFinished(ids);
//        log.debug('IdsEdit: {1}', ids);
//    },
//    render() {
//        let { ids } = this.props;
//        return (
//            <div>
//                <b>Список ID: </b>
//                <TextField
//                    ref="ids"
//                    multiLine={true}
//                    rows={10}
//                    rowsMax={20}
//                    fullWidth={true}
//                    defaultValue={ids.join('\n')}
//                    floatingLabelText="Edit accounts list" />
//                <RaisedButton
//                    onClick={() => this.save()}
//                    label="OK"
//                    primary={true} />
//            </div>
//        )
//    }
//});

//IdsSelect extends React.Component {
//    propTypes: {
//        ids: React.PropTypes.array.isRequired,
//        onEdit: React.PropTypes.func.isRequired
//    },
//    render() {
//        const { onEdit, ids } = this.props;
//        return (
//            <div>
//                <IconButton onClick={() => onEdit()} iconClassName="material-icons" tooltipPosition="bottom-center"
//                            tooltip="Edit">create</IconButton>
//                <b>Выбрано {ids.length} ID </b>
//            </div>
//        )
//    }
//});


export class ItemsSelect extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (this.props.onChange && !_.isEqual(nextProps.items, this.props.items))
            this.props.onChange();
    }
    render() {
        let { onEdit, items, groupName, itemName, itemsSize } = this.props;
        if (groupName)
            var group = `[${groupName}]`;
        if (itemsSize === undefined)
            itemsSize = _.size(items);
        return (
            <div className="items-select">
                <IconButton onClick={onEdit} iconClassName="material-icons" tooltipPosition="bottom-center"
                            tooltip="Изменить">create</IconButton>
                <div className="items-select-text">
                    <b>{group} Выбрано {itemsSize} {itemName} </b>
                </div>
            </div>
        )
    }
}
ItemsSelect.propTypes = {
    items: React.PropTypes.array,
    itemsSize: React.PropTypes.number,
    groupName: React.PropTypes.string,
    onEdit: React.PropTypes.func.isRequired,
    itemName: React.PropTypes.string,
    onChange: React.PropTypes.func
};


export class CheckBoxDetails extends React.Component {
    isChecked() {
        return this.refs.check.isChecked();
    }
    render() {
        var { onDetails, ...props } = this.props;
        if (onDetails)
            var detailsBtn = (
                <IconButton tooltip="Настройка" onClick={onDetails} className="checkbox-detail-icon">
                    <FontIcon className="material-icons">build</FontIcon>
                </IconButton>
            );
        return (
            <div className="checkbox-detail">
                <div>
                    {detailsBtn}
                </div>
                <div className="checkbox-check">
                    <Checkbox {...props} ref="check" />
                </div>
            </div>
        )
    }
}
CheckBoxDetails.propTypes = {
    onDetails: React.PropTypes.func
};


export class DropDownDetails extends React.Component {
    isChecked() {
        return this.refs.check.isChecked();
    }
    render() {
        var { onDetails, ...props } = this.props;
        if (onDetails)
            var detailsBtn = (
                <IconButton tooltip="Настройка" onClick={onDetails} className="dropdown-detail-icon">
                    <FontIcon className="material-icons">build</FontIcon>
                </IconButton>
            );
        if (_.size(this.props.children))
            var dropdown = (
                <DropDownMenu {...props} ref="dropdown">
                    { this.props.children }
                </DropDownMenu>
            );
        return (
            <div className="dropdown-detail">
                <div className="dropdown-detail-check">
                    {detailsBtn}
                </div>
                <div className="dropdown-check">
                    {dropdown}
                </div>
            </div>
        )
    }
}
DropDownDetails.propTypes = {
    onDetails: React.PropTypes.func
};

export class CheckBoxWithFields extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: this.props.defaultChecked
        };
    }
    isChecked() {
        //if (this.refs.check)
            return this.refs.check.isChecked();
    }
    render() {
        let { children, ref, ...props } = this.props;
        //if (/*this.props.onChange && */!this.props.defaultChecked)
        //    children = '';
        //var onChange = (e, i, v) => this.setState({checked: v});
        let self = this;
        var onChange = (e, v) => {
            //if (!self.props.onChange)
            //    return;
            self.setState({checked: v});
            if (self.props.onChange)
                self.props.onChange(v);
        };
        if (this.state.checked === false)
            children = '';
        return (
            <div className="checkbox-with-fields">
                <div className="xs">
                    <Checkbox {...props} onCheck={onChange} ref="check" />
                </div>
                <div>
                    {children}
                </div>
            </div>
        )
    }
}

export class NumberRange extends React.Component {
    getValue() {
        let from = +this.refs.from.getValue(), to = +this.refs.to.getValue();
        return { from, to };
    }
    changed() {
        if (this.props.onChange)
            this.props.onChange(this.getValue());
    }
    render() {
        let { from, to } = this.props;
        return (
            <div>
                <TextField
                    ref="from"
                    type="number"
                    hintText="От"
                    className="num-range-field"
                    onChange={() => this.changed()}
                    defaultValue={from} />
                –
                <TextField
                    ref="to"
                    type="number"
                    hintText="До"
                    className="num-range-field"
                    onChange={() => this.changed()}
                    defaultValue={to} />
            </div>
        )
    }
}
NumberRange.propTypes = {
    from: React.PropTypes.number,
    to: React.PropTypes.number,
    onChange: React.PropTypes.func
};


const { STARTED, STOPPED, PAUSED, FINISHED, WAIT, INIT, RESUME } = TaskRunStates;
const { OK, BAN, FREEZE, LOGIN_FAILED, UPDATING } = AccStateType;

export function taskStateText(state) {
    switch (state) {
        case STARTED: return 'Запущена';
        case STOPPED: return 'Остановлена';
        case PAUSED: return 'Приостановлена';
        case FINISHED: return 'Завершена';
        case WAIT: return 'Запланирована';
        case INIT: return 'Создана';
    }
}
export function accStateText(state) {
    switch (state) {
        case OK:            return 'OK';
        case BAN:           return 'Бан';
        case FREEZE:        return 'Заморозка';
        case LOGIN_FAILED:  return 'Ошибка входа';
        case UPDATING:      return 'Обновление';
    }
}
export function taskNameText(taskName) {
    switch (taskName) {
        case 'DialogsHistoryTask': return 'Сохранение диалогов';
        case 'IdsIterateTask':     return 'По списку пользователей';
        case 'IdsCollectTask':     return 'Сбор ID';
        case 'RenameAccsTask':     return 'Очистка аккаунтов';
        case 'UnfreezeAccsTask':   return 'Разморозка аккаунтов';
        case 'RegAccsTask':        return 'Регистрация аккаунтов';
        case 'BindPhoneTask':      return 'Привязка номеров к аккаунтам';
        default:
            return 'Неизвестный тип задачи';
    }
}
