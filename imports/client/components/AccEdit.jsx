import React from 'react';
import * as _ from 'lodash';
import store from '../redux/store';
import { log } from "../../common/logger/Logger";
import { Actions } from "../redux/actions";
import AccInfo from "../../common/AccInfo";
import UserAccs from '../../common/collections/UserAccs';
import { RaisedButton, TextField, DropDownMenu, FlatButton,
         FontIcon, Dialog, Toolbar, ToolbarGroup, MenuItem
} from 'material-ui';

export default class AccEditManager extends React.Component {
    getAccList(listName) {
        let ids = this.props.lists[listName];
        return _.transform(ids, (accs, _id) => {
            let accData = UserAccs.findOne(_id);
            if (accData)
                accs.push(new AccInfo().deserialize(accData));
        });
    }
    loadList(listName = this.props.selectedList) {
        if (!(listName in this.props.lists)) {
            if (_.isEmpty(this.props.lists)) return;
            listName = _.head(_.keys(this.props.lists));
            store.dispatch(Actions.accManager.switchList(listName));
        }
        let accs = this.getAccList(listName);
        let accsStr = _.map(accs, acc => acc.toString()).join('\n');
        this.refs.accEdit.setText(accsStr);
    }
    saveList() {
        var s = this.refs.accEdit.getText();
        return store.dispatch(Actions.accManager.saveList(this.props.selectedList, s)).then(result => {
            log.debug('saveList: {1}', result);
        });
    }
    removeList() {
        store.dispatch(Actions.accManager.removeList(this.props.selectedList));
        //Meteor.defer(() => this.switchToFirst());
        Meteor.defer(() => this.loadList());
    }
    save() {
        store.dispatch(Actions.accManager.save(this.props.lists));
    }
    switchList(listName) {
        this.saveList().then(() => {
            store.dispatch(Actions.accManager.switchList(listName));
            this.loadList(listName);
        });
    }
    addList(listName) {
        store.dispatch(Actions.accManager.addList(listName));
        Meteor.defer(() => this.switchList(listName));
    }
    switchToFirst() {
        let listNames = _.keys(this.props.lists);
        if (listNames.length) {
            store.dispatch(Actions.accManager.switchList(listNames[0]));
            this.loadList();
        } else
            this.refs.accEdit.setText('');
    }
    componentDidMount() {
        store.dispatch(Actions.accManager.load()).then(() => {
            //this.switchToFirst();
            this.loadList();
        });
    }
    finished() {
        this.saveList().then(() => {
          debugger;
          this.save();
          let { lists, selectedList } = this.props;
          debugger;
          log.debug('RA: {1}', this.props.onFinished);
          this.props.onFinished({
              selectedList,
              accIds: lists[selectedList]
          });
          debugger;
        });
    }
    render() {
        let listNames = _.keys(this.props.lists);
        return (
            <AccEdit
                ref="accEdit"
                listNames={listNames}
                selectedList={this.props.selectedList}
                onSwitchList={listName => this.switchList(listName)}
                onAddList={listName => this.addList(listName)}
                onRemoveList={() => this.removeList()}
                onFinished={() => this.finished()}
            />
        )
    }
}
AccEditManager.propTypes = {
    onFinished: React.PropTypes.func,
    selectedList: React.PropTypes.string.isRequired,
    lists: React.PropTypes.object.isRequired
};

export class AccEdit extends React.Component {
    constructor(props) {
        super(props);
        ACC_EDIT = this;
        this.state = { listNameDialogHidden: true/*, newListName: ''*/ };
    }
    setText(text) {
        this.refs.accEdit.getInputNode().value = text;//.setValue(text);
    }
    getText() {
        return this.refs.accEdit.getValue();
    }
    addListBegin() {
        this.setState({ listNameDialogHidden: false });
        Meteor.defer(() => this.refs.newListName.focus());
    }
    addListEnd() {
        this.setState({ listNameDialogHidden: true/*, newListName: ''*/ });
        var listName = this.refs.newListName.getValue();
        if (listName == '') return;
        this.props.onAddList(listName);
    }
    render() {
        // const { addListBegin, addListEnd, saveList } = this;
        const { listNames, selectedList, onSwitchList, onRemoveList, onFinished } = this.props;
        let lists = _.map(listNames, listName => ({ payload: listName, text: listName }));

        // let listIndex = () => {
        //     let i = _.findIndex(lists, list => list.payload == selectedList);
        //     return _.max([0, i]);
        // };
        const checkEnterDown = e => {
            if (e.key == 'Enter')
                this.addListEnd();
        };
        return (
            <div className="acc-edit-box1">
                <Dialog open={!this.state.listNameDialogHidden} title="Новый список">
                    <TextField ref="newListName"  hintText="Название списка..." onKeyDown={checkEnterDown} />
                </Dialog>
                <Toolbar>
                    <ToolbarGroup key={0} float="left">
                        <DropDownMenu value={selectedList} onChange={(e, i, item) => onSwitchList(item)}>
                            { lists.map((item, i) => (
                                <MenuItem key={i} value={item.payload} primaryText={item.text} />
                            )) }
                        </DropDownMenu>
                    </ToolbarGroup>
                    <ToolbarGroup key={1} float="right">
                        <FlatButton primary={true} label="Удалить" labelPosition="after" onClick={onRemoveList} disabled={!lists.length}>
                            <FontIcon className="material-icons">delete</FontIcon>
                        </FlatButton>
                        <FlatButton primary={true} label="Добавить" labelPosition="after" onClick={() => this.addListBegin()}>
                            <FontIcon className="material-icons">note_add</FontIcon>
                        </FlatButton>
                    </ToolbarGroup>
                </Toolbar>

                <TextField
                    ref="accEdit"
                    multiLine={true}
                    rows={10}
                    rowsMax={15}
                    fullWidth={true}
                    disabled={!lists.length}
                    hintText="Фотмат: ЛОГИН:ПАРОЛЬ ПРОКСИ_IP:ПОРТ|ПРОКСИ_ЛОГИН:ПРОКСИ_ПАРОЛЬ"
                    floatingLabelText="Редактирование списка аккаунтов" />
                <div className="right-btn">
                    <RaisedButton
                        onClick={onFinished} disabled={!lists.length}
                        label="OK" primary={true} />
                </div>
            </div>
        )
    }
}
AccEdit.propTypes = {
    listNames: React.PropTypes.arrayOf(React.PropTypes.string),
    selectedList: React.PropTypes.string,
    onSwitchList: React.PropTypes.func,
    onAddList: React.PropTypes.func,
    onRemoveList: React.PropTypes.func,
    onFinished: React.PropTypes.func
};
