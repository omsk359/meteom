import React from 'react';
import { Card, CardActions, CardHeader, CardText, CardTitle } from 'material-ui';

const About = () => {
    return (
        <div>
            <Card>
                <CardTitle
                  title="Автоматизация действий ВКонтакте"
                  subtitle="Основные возможности"
                  actAsExpander={true}
                  showExpandableButton={true}
                />
                <CardText expandable={true} style={{paddingTop: '0px'}}>
                  <ul>
                      <li>Регистрация аккаунтов</li>
                      <li>Очистка аккаунтов, переименование, изменение некоторых настроек</li>
                      <li>Сбор ID пользователей по поисковым ссылкам ВК</li>
                      <li>Фильтрация пользователей по критериям (время последнего посещения/проверка является ли другом/находится ли в списке и т.п.)</li>
                      <li>Рассылка сообщений/приглашений в друзья и т.п.</li>
                      <li>Разморозка временно забаненных аккаунтов</li>
                      <li>Сбор диалогов в базу</li>
                      <li>Планирование времени запуска задач, периодический запуск</li>
                      <li>Интеграция с сервисом распознания капчи</li>
                      <li>Интеграция с 4-мя сервисами аредны мобильных номеров для приема СМС</li>
                      <li>Возможность задания HTTP прокси для каждого аккаунта</li>
                      <li>Параллельный запуск множества задач</li>
                      <li>Рандомизация временных задержек, текста сообщений</li>
                      <li>Ведение списков id пользователей (они формируются на основании успешности/неуспешности выполнения действий)</li>
                      <li>Организация конвейеров задач на основании списков</li>
                  </ul>
                  <b>TODO:</b>
                  <ul>
                      <li>Мессенджер</li>
                      <li>Возможность очистки выбранных списков перед запуском задачи</li>
                  </ul>
                </CardText>
            </Card>
            <Card>
                <CardTitle
                  title="Технические особенности реализации"
                  actAsExpander={true}
                  showExpandableButton={true}
                />
                <CardText expandable={true} style={{paddingTop: '0px'}}>
                  <ul>
                      <li>Функциональность задач наращивается путем ООП наследования: базовый класс ResumableTask реализует возможность управления потоком задачи, далее от нее наследуется ScheduledTask для добавления слоя планирования запуска и пероидичности задачи и т.д.</li>
                      <li>Задачи написаны в синхронном стиле, что существенно упрощает код и способствует изоляции ошибок в разных потоках выполнения.</li>
                      <li>Для каждого аккаунта выделен свой вспомогательный объект, где ведется подсчет ошибок HTTP запросов. На основании этого, принимается вывод о продолжении/прекращении процесса (ошибки прокси, ошибки интернет соединения, ошибки ВК API и т.п.). Предусмотрено выполнение повторных запросов при возникновении ошибок.</li>
                      <li>Для каждого аккаунта/задачи/подзадачи предусмотрен собственный логгер. Для использования общего логгера для нескольких объектов, ссылка на него сохраняется в каждом из них.</li>
                      <li>Задачи сохраняют свое состояние в базу данных с помощью механизма сериализации/десериализации. Могут быть восстановлены и возобновлены автоматически после перезапуска сервера. Во избежании слишком частого сохранения в базу, применяется throttle-функция.</li>
                  </ul>
                </CardText>
            </Card>
            <Card>
                <CardTitle
                  title="Особенности интерфейса"
                  actAsExpander={true}
                  showExpandableButton={true}
                />
                <CardText expandable={true} style={{paddingTop: '0px'}}>
                  <ul>
                      <li>Реактивность интерфейса (например, после добавления новой задачи или удаления старой в одной вкладке, список задач будет автоматически обновлен в другой вкладке).</li>
                      <li>При обновлении состояния задачи в базе данных, ее состояние обновится в интерфейсе.</li>
                      <li>Подписка на журнал выполнения задачи или отдельной ее подзадачи по WebSocket (а также загрузка предыдущей истории).</li>
                      <li>Сохранение состояния интерфейса (redux) в локальное хранилище браузера, благодаря чему после перезагрузки страницы, она восстанавливает свое состояние.</li>
                  </ul>
                </CardText>
            </Card>
        </div>
    )
};

export default About
