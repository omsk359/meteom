import React from 'react';
import * as _ from 'lodash';
import store from '../redux/store';
import { log } from "../../common/logger/Logger";
import { splitStr } from "../../common/helpers";
import { RaisedButton, TextField } from 'material-ui';

export default class MultiLineEdit extends React.Component {
    save() {
        let lines = this.refs.text.getValue();
        lines = splitStr(lines, this.props.splitter);
        this.props.onFinished(lines);
        log.debug('MultiLineEdit: {1}', lines); //
    }
    render() {
        let lines = this.props.lines || [];
        //let desc = prepareDescText(this.props.desc);
        let desc = this.props.desc;
        if (desc)
            var br = <br />;
        //<span><p>{ desc }{ br }</p></span><br />
        return (
            <div>
                <b>{this.props.title}</b>
                <TextField
                    ref="text" id="text"
                    multiLine={true}
                    rows={this.props.rows}
                    rowsMax={this.props.rowsMax}
                    fullWidth={true}
                    defaultValue={lines.join(this.props.splitter)}
                    floatingLabelText={this.props.floatingText} />
                <span><p dangerouslySetInnerHTML={{__html: desc}}></p></span>

                <div className="right-btn">
                    <RaisedButton onClick={() => this.save()} label="OK" primary={true} />
                </div>
            </div>
        )
    }
}
MultiLineEdit.defaultProps = {
    splitter: '\n',
    rows: 10,
    rowsMax: 15
};
MultiLineEdit.propTypes = {
    lines: React.PropTypes.arrayOf(React.PropTypes.string),
    title: React.PropTypes.string,
    floatingText: React.PropTypes.string,
    onFinished: React.PropTypes.func,
    desc: React.PropTypes.string,
    splitter: React.PropTypes.string,
    rows: React.PropTypes.number,
    rowsMax: React.PropTypes.number
};
