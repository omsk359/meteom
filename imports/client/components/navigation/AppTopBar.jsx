import * as React from "react";
import { createContainer } from 'meteor/react-meteor-data';
import { browserHistory } from 'react-router'
import { clearStorage } from "../../redux/store";
import { log } from "../../../common/logger/Logger";
import { getUserData, setUserData } from "../../helpers";
import { RaisedButton, Toolbar, ToolbarGroup, ToolbarTitle, DropDownMenu, IconMenu, SelectField,
         ToolbarSeparator, Dialog, TextField, MenuItem, FontIcon, AppBar, IconButton, Avatar
} from 'material-ui';
import Chip from 'material-ui/Chip';
import { blue600, indigo900 } from 'material-ui/styles/colors';

const styles = {
    login: {
        color: 'white',
        display: 'inline-block',
        position: 'relative',
        top: '-5px'
    },
    pathSelected: {
        color: 'red'
    },
    chip: {
        // vertically align
        position: 'relative',
        top: '50%',
        transform: 'translateY(-50%)'
    },
    chipText: {
        color: 'white'
    }
};

export class AppTopBarDumb extends React.Component {
    goTo(url) {
        browserHistory.push(url);
    }
    loginAction(action) {
        switch (action) {
            case 'clearStorage':
                clearStorage();
                return location.reload();
            case 'logout':
                return Meteor.logout(() => {
                    window.location.href = '/';
                });
            case 'profile':
                return this.goTo('/login');
            case 'appSettings':
                return this.refs.settings.open();
            case 'logoutOther':
                return Meteor.logoutOtherClients(() => {
                    browserHistory.replace('/');
                });
        }
    }
    // showSettings() {
    //     this.refs.settings.open();
    // }
    render() {
//        const path = window.location.pathname; // not reactive
        const path = this.props.location.pathname;
        const path2Name = {
            '/tasks': 'Задачи',
            '/messenger': 'Сообщения',
            '/login': 'Профиль',
            '/about': 'О проекте'
        };
        const path2Title = path => {
            if (path in path2Name)
                return path2Name[path];
            if (new RegExp('^/task/').test(path))
                return (
                    <div style={styles.chip}>
                        <Chip onTouchTap={browserHistory.goBack} backgroundColor={blue600}>
                            <Avatar backgroundColor={indigo900} icon={<FontIcon className="material-icons">backspace</FontIcon>} />
                            <span style={styles.chipText}>К списку задач</span>
                        </Chip>
                    </div>
                )
        };
        APP_TOP_BAR = this;
        BH = browserHistory;
        return (
            <div>
                <AppBar title={path2Title(path)} style={{marginBottom: '30px'}}
                        iconElementLeft={
                            <IconMenu
                                iconButtonElement={
                                    <IconButton tooltip="Навигация">
                                       <FontIcon className="material-icons">list</FontIcon>
                                   </IconButton>
                                }
                                targetOrigin={{horizontal: 'left', vertical: 'top'}}
                                anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                                touchTapCloseDelay={10}
                                onChange={(e, item) => this.goTo(item)}
                                >
                                    {
                                        _.map(path2Name, (name, pathKey) => (
                                            <MenuItem
                                                style={path == pathKey ? styles.pathSelected : {}}
                                                key={pathKey} value={pathKey} primaryText={name}
                                            />
                                        ))
                                    }
                            </IconMenu>
                        }
                        iconElementRight={
                            <div>
                                <div style={styles.login} class="appTopBar-login">
                                    <span>{_.get(this.props.currentUser, 'emails[0].address')}</span>
                                </div>
                                <IconMenu
                                    iconButtonElement={
                                        <IconButton tooltip="Аккаунт">
                                           <FontIcon className="material-icons">more_vert</FontIcon>
                                       </IconButton>
                                    }
                                    targetOrigin={{horizontal: 'right', vertical: 'top'}}
                                    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                                    onChange={(e, item) => this.loginAction(item)}
                                >
                                    <MenuItem value='appSettings' primaryText='Настройки приложения' rightIcon={
                                            <FontIcon className="material-icons">settings_applications</FontIcon>
                                        } />
                                    <MenuItem value='profile' primaryText='Профиль' rightIcon={
                                            <FontIcon className="material-icons">account_box</FontIcon>
                                        } />
                                    <MenuItem value='clearStorage' primaryText='Очистить кэш' rightIcon={
                                            <FontIcon className="material-icons">restore_page</FontIcon>
                                        } />
                                    <MenuItem value='logoutOther' primaryText='Выход на других устройствах' />
                                    <MenuItem value='logout' primaryText='Выход' rightIcon={
                                            <FontIcon className="material-icons">exit_to_app</FontIcon>
                                        } />
                                </IconMenu>
                          </div>
                        }
                    />
                    <UserSettingsDialog ref="settings" />
                </div>
        );
    }
}

const AppTopBar = createContainer(props => ({
    ...props,
    currentUser: Meteor.user()
}), AppTopBarDumb);

export default AppTopBar


export class UserSettingsDumb extends React.Component {
    save() {
        let settings = {
            ruCaptchaKey: this.refs.ruCaptchaKey.getValue(),
            simSmsKey: this.refs.simSmsKey.getValue(),
            smsVkKey: this.refs.smsVkKey.getValue(),
            smsLikeKey: this.refs.smsLikeKey.getValue(),
            smsAreaKey: this.refs.smsAreaKey.getValue()
        };
        this.props.onSave(settings);
    }
    render() {
        const { settings } = this.props;
        return (
            <div>
                <span>Ключ RuCaptcha: </span>
                <TextField id="1" ref="ruCaptchaKey" defaultValue={settings.ruCaptchaKey} />
                <br />
                <span>Ключ SimSms: </span>
                <TextField id="2" ref="simSmsKey" defaultValue={settings.simSmsKey} />
                <br />
                <span>Ключ SmsVk: </span>
                <TextField id="3" ref="smsVkKey" defaultValue={settings.smsVkKey} />
                <br />
                <span>Ключ SmsLike: </span>
                <TextField id="4" ref="smsLikeKey" defaultValue={settings.smsLikeKey} />
                <br />
                <span>Ключ SmsArea: </span>
                <TextField id="5" ref="smsAreaKey" defaultValue={settings.smsAreaKey} />
                <br />

                <RaisedButton onClick={() => this.save()} label="Сохранить" primary={true} />
            </div>
        );
    }
}
UserSettingsDumb.propTypes = {
    settings: React.PropTypes.object.isRequired,
    onSave: React.PropTypes.func.isRequired
};

export class UserSettingsDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hidden: true };
    }
    close() { this.setState({hidden: true}); }
    open() { this.setState({hidden: false}); }

    render() {
        const { settings } = this.props;
        return (
            <div>
                <Dialog title="Настройки приложения" open={!this.state.hidden} onRequestClose={() => this.close()}>
                    <UserSettings {...this.props} onFinished={() => this.close()} />
                </Dialog>
            </div>
        );
    }
}

const UserSettings = createContainer(props => {
    const userDataHandle = Meteor.subscribe('userData');
    const loading = !userDataHandle.ready();

    return {
        loading,
        settings: getUserData('userSettings') || {},
        onSave: settings => {
            setUserData('userSettings', settings);
            props.onFinished();
        }
    };
}, UserSettingsDumb);

//export UserSettings
