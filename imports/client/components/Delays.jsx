import React from 'react';
import { parseDelays, delaysToStr } from "../../common/helpers";
import { RaisedButton, TextField } from 'material-ui';

export default class Delays extends React.Component {
    render() {
        let onClick = () => {
            let delays = parseDelays(this.refs.delays.getValue());
            this.props.onFinished(delays)
        };
        return (
            <div>
                <b>Задержки</b><br />
                <TextField ref="delays"
                    defaultValue={delaysToStr(this.props.delays)}
                    fullWidth={true}
                    hintText="0: 10-20, 1: 30-60, 3: 100-200..." /><br /><br />
                <RaisedButton
                    onClick={onClick}
                    label="OK"
                    primary={true} />
            </div>
        )
    }
}
Delays.propTypes = {
    onFinished: React.PropTypes.func,
    delays : React.PropTypes.object
};
