import React from 'react';
import * as _ from 'lodash';
import store from '../redux/store';
import { log } from "../../common/logger/Logger";
import { TaskRunStates } from "../../common/types";
import { Actions } from "../redux/actions";
import { taskNameText, taskStateText } from "./helpers";
import { connect, Provider } from 'react-redux';
import TasksCreator from './TasksCreator/TasksCreator';
import { Link } from 'react-router';
import { Table, TableHeader, TableBody, TableRow, TableHeaderColumn,
         TableRowColumn, Snackbar, FlatButton, IconButton, FontIcon
} from 'material-ui';

// const {
//     Table, TableHeader, TableBody, TableRow, TableHeaderColumn, TableRowColumn, FlatButton, DropDownMenu, IconButton, FontIcon, Dialog, List, ListItem, Paper, Tabs, Tab
// } = mui;

// Override Meteor._debug to filter for custom msgs
Meteor._debug = (function (super_meteor_debug) {
    return function (error, info) {
        if (!(info && _.has(info, 'msg')))
            super_meteor_debug(error, info);
    }
})(Meteor._debug);

let taskCreators = [ 'idsIterateTaskCreator', 'dialogsHistoryTaskCreator', 'idsCollectTaskCreator', 'renameAccsTaskCreator', 'unfreezeAccsTaskCreator', 'regAccsTaskCreator', 'bindPhoneTaskCreator' ];

export function getCtrlMenu(taskId, runState, includeRemove) {
    const { STARTED, STOPPED, PAUSED, FINISHED, WAIT, INIT, RESUME } = TaskRunStates;
    const REMOVE = includeRemove ? 'REMOVE' : null;
    const actionFunc = {
        [RESUME]   : taskId => store.dispatch(Actions.tasks.resume(taskId)),
        [STARTED]  : taskId => store.dispatch(Actions.tasks.start(taskId)),
        [PAUSED]   : taskId => store.dispatch(Actions.tasks.pause(taskId)),
        [STOPPED]  : taskId => store.dispatch(Actions.tasks.stop(taskId)),
        [REMOVE]   : taskId => store.dispatch(Actions.tasks.remove(taskId))
    };
    const actionIcons = {
        [RESUME]   : 'play_circle_filled',
        [STARTED]  : 'play_arrow',
        [PAUSED]   : 'pause',
        [STOPPED]  : 'stop',
        [REMOVE]   : 'delete'
    };
    let funcs = [];
    switch (runState) {
        case PAUSED:
            funcs = [STOPPED, RESUME]; break;
        case STARTED:
        case WAIT:
            funcs = [STOPPED, PAUSED]; break;
        case INIT:
            funcs = [REMOVE, STARTED]; break;
        case STOPPED:
        case FINISHED:
            funcs = [REMOVE]; break;
    }
    funcs = _.compact(funcs);
    return _.map(funcs, aType =>
        <IconButton key={aType} onClick={() => actionFunc[aType](taskId)}>
            <FontIcon className="material-icons" color={aType == REMOVE ? '#f00000' : '#000000'} >{actionIcons[aType]}</FontIcon>
        </IconButton>
    )
}

export class TaskTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            infoBarShow: false,
            errBarShow: false
        };
    }
    showLog(taskId) {
        log('switch to: {1}', taskId);
        store.dispatch(Actions.taskTable.selectTask(taskId))
    }
    logSwitch(event, selectedIndex, menuItem) {
        //delete Meteor.default_connection._stream.eventCallbacks[Streamy._applyPrefix(this.state.selectedTaskId)];
        this.showLog(menuItem.payload);
    }
    removeTask(taskId) {
        Meteor.call('removeTask', taskId);
    }
    stopTask(taskId) {
        Meteor.call('stopTask', taskId);
    }
    createTask() {
        this.refs.tasksSelect.open();
    }
    startTask(taskId) {
        this.showLog(taskId);
        Meteor.call('startTask', taskId, () => {});
    }
    resumeTask(taskId) {
        this.showLog(taskId);
        Meteor.call('resumeTask', taskId, () => {});
    }
    pauseTask(taskId) {
        Meteor.call('pauseTask', taskId, () => {});
    }

    hideMsg() {
        store.dispatch(Actions.hideMsg());
    }
    hideErr() {
        store.dispatch(Actions.hideErr());
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.infoMsg !== nextProps.infoMsg)
            if (nextProps.infoMsg)
                this.setState({ infoBarShow: true });
            else
                this.setState({ infoBarShow: false });
        if (this.props.errMsg !== nextProps.errMsg) {
            if (nextProps.errMsg)
                this.setState({ errBarShow: true });
            else if (this.props.errMsg)
                this.setState({ errBarShow: false });
        }

    }

    render() {
        let { tasks } = this.props;
        tasks = _.sortBy(tasks, 'createdAt').reverse();
        let taskCreatorState = { ...this.props.taskCreatorState, ..._.pick(this.props, taskCreators) };
        const { STARTED, STOPPED, PAUSED, FINISHED, WAIT, INIT, RESUME } = TaskRunStates;
        //<IconButton onClick={actionFunc[runState].bind(this, task._id)}>
        //    <FontIcon className="material-icons">{actionIcons[runState]}</FontIcon>
        //</IconButton>

        return (
            <div className="tasks-table-box">

                <TasksCreator ref="tasksSelect" {...taskCreatorState} />
                <Table height="300px" selectable={false}>
                    <TableHeader displaySelectAll={true} className="task-table-header">
                        <TableRow>
                            <TableHeaderColumn style={{textAlign: 'center', width: '50px'}}>
                                <IconButton tooltip="Новая задача" onClick={() => this.createTask()}>
                                    <FontIcon className="material-icons">note_add</FontIcon>
                                </IconButton>
                            </TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}} tooltip='ID задачи'>ID</TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}} tooltip='Тип задачи'>Тип</TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}} tooltip='Состояние задачи'>Состояние</TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}} tooltip='Журнал выполнения задачи'>Лог</TableHeaderColumn>
                            <TableHeaderColumn style={{textAlign: 'center'}} >Подробности</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        { _.map(tasks, task => {
                            //log.debug('subTasks: {1}; task: {2}', task.data.tasks, task);
                            //if (task.data.tasks) {
                            //    var taskMenuItems = _.map(task.data.tasks, subTask => { return { payload: subTask._id, text: subTask.title } });
                            //    taskMenuItems.unshift({ payload: task._id, text: 'Full' });
                            //    log.debug('taskMenuItems: {1}', taskMenuItems);
                            //    var logCtrl = <DropDownMenu menuItems={taskMenuItems} onChange={this.logSwitch} menuItemStyle={{zIndex: 9999}} />
                            //} else
                                var logCtrl = <FlatButton disabled={runState != STARTED && runState != PAUSED} secondary={true} label="Показать" linkButton={true} onClick={() => this.showLog(task._id)} />;
                            let runState = _.get(task, 'data.state.runState');
                            let actionIcons = {
                                [STARTED] : 'pause',
                                [PAUSED]: 'play_circle_filled',
                                [INIT]: 'play_arrow',
                                [WAIT]: 'query_builder',
                                [STOPPED]: '',
                                [FINISHED]: 'done'
                            };
                            let actionFunc = {
                                [STARTED]: this.pauseTask,
                                [PAUSED]: this.resumeTask,
                                [INIT]: this.startTask,
                                [WAIT]: () => {},
                                [STOPPED]: () => {},
                                [FINISHED]: () => {}
                            };
                            //let canRemove = runState != STARTED && runState != PAUSED;
                            let canRemove = _.includes([INIT, STOPPED, FINISHED], runState);
                            //<IconButton tooltip="Удалить" tooltipPosition="center-left">
                            //    <FontIcon className="material-icons" color={canRemove ? '#f00000' : '#E8E8E8'} onClick={this.removeTask.bind(this, task._id)}>delete</FontIcon>
                            //</IconButton>
                            //<TableRowColumn style={{textAlign: 'center', width: '50px'}}>
                            //</TableRowColumn>
                            return (
                                <TableRow key={task._id}>
                                    <TableRowColumn style={{textAlign: 'left', width: '70px'}}>
                                        {getCtrlMenu(task._id, runState, true)}
                                    </TableRowColumn>
                                    <TableRowColumn style={{textAlign: 'center'}}>{task._id}</TableRowColumn>
                                    <TableRowColumn style={{textAlign: 'center'}}>{taskNameText(task.taskName)}</TableRowColumn>
                                    <TableRowColumn style={{textAlign: 'center'}}>{taskStateText(runState)}</TableRowColumn>
                                    <TableRowColumn>{logCtrl}</TableRowColumn>
                                    <TableRowColumn style={{textAlign: 'center'}}><Link to={`/task/${task._id}`}>
                                        <FontIcon className="material-icons" color="#0000f0" style={{margin: '0 auto'}}>forward</FontIcon>
                                    </Link></TableRowColumn>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>

                <Snackbar
                    ref="infoBar"
                    message={this.props.infoMsg}
                    open={this.state.infoBarShow}
                    action="hide"
                    autoHideDuration={3000}
                    onDismiss={this.hideMsg}
                    onActionTouchTap={this.hideMsg} />
                <Snackbar
                    ref="errBar"
                    message={this.props.errMsg}
                    open={this.state.errBarShow}
                    action="hide"
                    onDismiss={this.hideErr}
                    onActionTouchTap={this.hideErr} />
            </div>
        )
    }
}

// if (typeof accManager != 'undefined')
//     accManager = accManager || '';

//Meteor.subscribe('tasks');
//Meteor.startup(function() { // work around files not being defined yet
    // trigger action when this changes
    //log.debug('trackCollection ------- TRACK TaskData');
//});

// AppContainer is responsible for fetching data from the store and
// listening for changes. In a larger app you would have a container
// for each major component.

export default class TaskTableContainer extends React.Component {
    componentWillMount() {
        store.dispatch(Actions.tasks.update());
    }
    render() {
        return (
            <Provider store={store}>
                <TaskTableConnected />
            </Provider>
        );
    }
}

const TaskTableConnected = connect(state => ({
    ...state.taskTable, ...state.common, ...state.tasks,
    taskCreatorState: { ...state.taskCreator, accManagerStore: state.accManager, userIdListManager: state.userIdListManager },
    ..._.pick(state, taskCreators)
}))(TaskTable);

