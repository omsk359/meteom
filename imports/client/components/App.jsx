import { Router, Route, IndexRedirect, IndexLink, Link, browserHistory } from 'react-router';
import { Accounts, STATES } from 'meteor/std:accounts-ui';
import React from 'react';
import { AccEdit } from "./AccEdit";
import TaskTableContainer from "./TaskBoard";
import AccTableContainer from './AccTable';
import TaskDetailsContainer from "./MultiTaskDetails";
import MessengerContainer from "./Messager";
import AppTopBar from './navigation/AppTopBar';
import About from './About';

// A history object must be created to maintain the history for our router
// browserHistory = history.createHistory();

// The view to displayed as the index view
class Index extends React.Component {
  render() {
    return (
        <div>
          <p>This is the index route.</p>
            <IndexLink to="/acc-edit">Edit acc list</IndexLink>
            <IndexLink to="/acc-table">Show acc table</IndexLink>
            <IndexLink to="/tasks">Tasks</IndexLink>
        </div>
    )
  }
}

class MainPage extends React.Component {
    render() {
        return (
            <div>
                <p>Пока пусто.</p>
            </div>
        )
    }
}

const requireAuth = (nextState, replace) => {
    if (!Meteor.userId())
        replace({ nextPathname: nextState.location.pathname }, '/login')
};

export class Routes extends React.Component {
    render() {
        //log.debug('Routes props: {1}', this.props);
        //<IndexRoute component={MainPage} onEnter={requireAuth}/>

        return (
            <Router history={browserHistory}>
                <Route path="/" component={AppRoot}>
                    <IndexRedirect to="/about" />
                    <Route path="acc-edit" component={AccEdit} onEnter={requireAuth}/>
                    <Route path="acc-table" component={AccTableContainer} onEnter={requireAuth}/>
                    <Route path="task/:id" component={TaskDetailsContainer}/>
                    <Route path="messenger" component={MessengerContainer} onEnter={requireAuth}/>
                    <Route path="login" component={ Accounts.ui.LoginForm } formState={ STATES.SIGN_IN } />
                    <Route path="signup" component={ Accounts.ui.LoginForm } formState={ STATES.SIGN_UP } />
                    <Route path="tasks" component={TaskTableContainer} onEnter={requireAuth}/>
                    <Route path="about" component={About} onEnter={requireAuth}/>
                    <Route path="test2" component={Test2Comp} />
                </Route>
            </Router>
        )
    }
}

class Test2Comp extends React.Component {
    render() {
        //log.debug('AppRoot props: {1}', this.props);
        return (
            <Dialog title="Login" open={true} >
                <FlatButton label="Primary" primary={true} />
            </Dialog>
        );
    }
}


class AppRoot extends React.Component {
    render() {
        return (
            <div>
                { Meteor.userId() ? <AppTopBar {...this.props} /> : '' }
                {this.props.children}
            </div>
        );
    }
}
