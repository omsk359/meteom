import React from 'react';
import * as _ from 'lodash';
import store from '../../../redux/store';
import { TaskActionTypes } from "../../../../common/types";
import { Actions } from "../../../redux/actions";
import { TaskFilterTypes } from "../../../../common/types";
import { TasksCreatorRefs as Refs } from "../TasksCreator";
import { merge } from '../../../../common/helpers';
import { CheckBoxDetails } from "../../helpers";
import { RaisedButton, TextField } from 'material-ui';

export default class InviteTaskConfig extends React.Component {
    changed() {
        let params = {
            idsOkCntMax: this.refs.idsOkCntMax.getValue(),
            msgEnable: this.refs.msgEnable.isChecked()
        };
        const types = merge(TaskActionTypes, TaskFilterTypes);
        store.dispatch(Actions.idsIterateTC.setParams({[types.INVITE]: params}));
    }
    render() {
        const { INVITE, ADD_TO_USER_ID_LISTS, ADD_TO_OLD_INVITE_DB } = TaskActionTypes;
        const { idsOkCntMax, msgEnable, msg } = this.props.params[INVITE] || {};
        const { USER_ID_LISTS, NOT_IN_OLD_INVITE_DB } = TaskFilterTypes;
        const { accs, switchTo, accList, onFinished, isReady } = this.props;
        let checkActions = [
            //{ value: ADD_TO_INVITE_DB, text: 'Добавить в базу Invite', checked: _.includes(this.props.onActions[INVITE].onOK, ADD_TO_INVITE_DB) },
            { value: ADD_TO_OLD_INVITE_DB, text: 'Добавить в базу om-author', checked: _.includes(this.props.onActions[INVITE].onOK, ADD_TO_OLD_INVITE_DB) },
            //{ value: ADD_TO_USER_ID_LISTS, text: 'Добавить в списки', checked: _.includes(this.props.onActions[INVITE].onOK, ADD_TO_USER_ID_LISTS), details: Refs.UserIdListsInvite }
        ];
        // { causeAct: { onOK: [act, ...] } } -> { act: { on: 'onOK', causeAct: causeAct } }
        //let onActionsMap = _.transform(this.props.onActions, (onActions, onObj, causeAction) =>
        //    _.each(onObj, (acts, on) =>
        //        _.each(acts, act =>
        //            onActions[act] = { on, causeAction }
        //        )
        //    )
        //);
        let onToggleAction = (aType, checked) => {
            store.dispatch(Actions.idsIterateTC.setOnAction({ causeAction: INVITE, action: aType, enable: checked }));
        };
        let actions = _.map(checkActions, item => (
            <CheckBoxDetails
                key={item.value} value={item.value}
                label={item.text}
                className="action-onOK"
                defaultChecked={item.checked}
                disabled={item.disabled}
                onDetails={item.details && (() => switchTo(item.details))}
                onCheck={(e, checked) => onToggleAction(item.value, checked)}
            />
        ));

        let checkFilters = [
            //{ value: NOT_IN_INVITE_DB, text: 'Нет в базе Invite', disabled: false, hidden: true },
            { value: NOT_IN_OLD_INVITE_DB, text: 'Проверка по базе om-author', disabled: false }
        ];
        let onToggleFilter = (fType, checked) => {
            store.dispatch(Actions.idsIterateTC.setFilter({ action: INVITE, filter: fType, enable: checked }));
        };
        let selectedFilters = _.flatten(_.values(this.props.filters));
        let filters = _.map(checkFilters, item => (
            <CheckBoxDetails
                key={item.value} name={item.value} value={item.value}
                label={item.text}
                defaultChecked={_.includes(selectedFilters, item.value)}
                disabled={item.disabled}
                onDetails={item.details && (() => switchTo(item.details))}
                onCheck={(e, checked) => onToggleFilter(item.value, checked)}
            />
        ));
        let msgCheck = (
            <CheckBoxDetails
                ref="msgEnable" label="Сообщение" defaultChecked={msgEnable}
                onDetails={() => switchTo(Refs.InviteMsgEdit)}
                onCheck={() => this.changed()}
            />
        );
        return (
            <div>
                <div className="checkbox-group-title">Параметры инвайтинга</div>
                <div style={{display: 'flex'}}>
                    <div style={{marginRight: '30px'}}>
                        <span>Макс. ограничение: </span>
                        <TextField ref="idsOkCntMax" type="number" defaultValue={idsOkCntMax} onChange={() => this.changed()} className="num-field" /><br />
                    </div>
                    { msgCheck }
                </div><hr />
                <div className="checkbox-group-title">Фильтры инвайтинга</div>
                <div className="checkbox-group">
                    { filters }
                </div><br /><hr />
                <div className="checkbox-group-title">Действия при успешной отправке</div>
                <div className="checkbox-group">
                    { actions }
                </div>

                <div className="right-btn">
                    <RaisedButton onClick={onFinished} label="OK" disabled={!isReady[INVITE]} primary={true} style={{ margin: '10px 10px' }} />
                </div>
            </div>
        )
    }
}
InviteTaskConfig.propTypes = {
    idsSource: React.PropTypes.object,
    actions: React.PropTypes.arrayOf(React.PropTypes.string),
    switchTo: React.PropTypes.func.isRequired,
    onFinished: React.PropTypes.func.isRequired
};
