import React from 'react';
import * as _ from 'lodash';
import store from '../../../redux/store';
import { Actions } from "../../../redux/actions";
import { IDS_ITERATE } from "../../../redux/reducers";
import { TaskActionTypes } from "../../../../common/types";
import { TaskFilterTypes } from "../../../../common/types";
import { IdsSourceTypes } from "../../../../common/types";
import IdsIterateFilters from "./Filters";
import IdsIterateActions from "./Actions";
import { TasksCreatorRefs as Refs } from "../TasksCreator";
import IdsIterateSource from "./IdsSource";
import { CreateTaskBtns } from "../TasksCreator";
import { RaisedButton } from 'material-ui';
import { ItemsSelect } from '../../helpers';

export default class IdsIterateTaskCreator extends React.Component {
    componentWillMount() {
        store.dispatch(Actions.accManager.saveList(this.props.accList));
    }
    allFilters() {
        let { actions, filters } = this.props;
        return _.flatten(_.values(_.pick(filters, [...actions, IDS_ITERATE.COMMON_FILTERS])));
    }
    create() {
        let { idsSource, actions, filters, accs, onActions, delays, taskScheduler } = this.props;
        //filters = _.flatten(_.values(_.pick(filters, [...actions, IDS_ITERATE.COMMON_FILTERS])));
        filters = this.allFilters();
        const { INVITE, SEND_MSG, DELETE_FRIEND, ADD_TO_USER_ID_LISTS, NOPE } = TaskActionTypes;
        const { USER_ID_LISTS } = TaskFilterTypes;
        let params = {
            idsQueue: idsSource.type,
            actions, filters, accs, onActions, delays, taskScheduler
        };
        switch (params.idsQueue) {
            case IdsSourceTypes.IDS_COMMON_LIST:
            case IdsSourceTypes.IDS_COPY_LIST:
                //params.ids = _.map(idsSource.ids, Number); break;
                _.assign(params, _.pick(idsSource, ['userIdListName', 'listInfLoop']));
                break;
            case IdsSourceTypes.SEARCH_URLS:
                params.idsSearchParams = {
                    accs: _.slice(accs, 0, 10),
                    urls: idsSource.urls,
                    resetLastParams: idsSource.resetLastParams
                };
                break;
        }
        _.assign(params, _.pick(this.props.params, actions));
        _.assign(params, _.pick(this.props.params, filters));
        //if (_.includes(filters, USER_ID_LISTS))
        //    params[USER_ID_LISTS] = _.flatten(_.values(_.pick(params[USER_ID_LISTS], actions))); // [USER_ID_LISTS]: { [INVITE]: [LIST1, ...] } -> [USER_ID_LISTS]: [LIST1, ...]

        params.onActions = { [params.actions.join(',')]: onActions[IDS_ITERATE.SELECTED_ACTIONS] };

        let onActionsTypes = _.flattenDeep(_.values(_.pick(params.onActions, actions)).map(a => _.values(a)));

        _.assign(params, _.pick(this.props.params, onActionsTypes));

        params.maxCheckCnt = 4;

        this.props.onCreate(params);
    }
    render() {
        let { isReady, actions, accs, accList, switchTo, idsSource, filters, params } = this.props;
        const { ADD_TO_USER_ID_LISTS, REMOVE_FROM_USER_ID_LISTS } = TaskActionTypes;
        let isReadyCreate = () => isReady.idsIterateTask;

        const checkFilters = IdsIterateFilters.getCheckFilters();
        filters = this.allFilters();
        let filtersChosen = _.map(filters, filter => _.get(_.find(checkFilters, ['value', filter]), 'text'));

        const checkActions = IdsIterateActions.getCheckActions();
        let actionsChosen = _.map(actions, filter => _.get(_.find(checkActions, ['value', filter]), 'text'));

        let listActionsChosen = [];
        listActionsChosen.push(..._.map(params[ADD_TO_USER_ID_LISTS].onOK, listName => ({listName, onOK: true, action: ADD_TO_USER_ID_LISTS})));
        listActionsChosen.push(..._.map(params[REMOVE_FROM_USER_ID_LISTS].onOK, listName => ({listName, onOK: true, action: REMOVE_FROM_USER_ID_LISTS})));
        listActionsChosen.push(..._.map(params[ADD_TO_USER_ID_LISTS].onFail, listName => ({listName, onOK: false, action: ADD_TO_USER_ID_LISTS})));
        listActionsChosen.push(..._.map(params[REMOVE_FROM_USER_ID_LISTS].onFail, listName => ({listName, onOK: false, action: REMOVE_FROM_USER_ID_LISTS})));
        listActionsChosen = _.map(listActionsChosen, (list, i) =>  {
            let color = list.onOK ? 'green' : 'red';
            let action = list.action == ADD_TO_USER_ID_LISTS ? '\u2795' : '\u2716';
            action = `(${action})`;
            return (
                <span key={i} style={{color}}>{action}{list.listName}{'\u00A0\u00A0'}</span>
            )
        });

        return (
            <div>
                <hr />
                <div style={{display: 'flex', margin: '0px 0px'}}>
                    <ItemsSelect items={accs} itemName="аккаунтов" groupName={accList} onEdit={() => switchTo(Refs.AccEdit)} />
                    <div className="ids-iterate-delays">
                        <RaisedButton label="Задержки..." secondary={true} onClick={() => switchTo(Refs.IdsIterateDelays)} />
                    </div>
                </div>

                <hr />
                <div className="ids-iterate-source">
                    <IdsIterateSource {...this.props} {...idsSource} />
                </div>
                <hr />
                <div className="ids-iterate-filters">
                    <RaisedButton label="Фильтры..." secondary={true} onClick={() => switchTo(Refs.IdsIterateFilters)} style={{marginRight: '10px'}} />
                    <i>{filtersChosen.join(', ')}</i>
                </div>
                <div className="ids-iterate-actions">
                    <RaisedButton label="Действия..." secondary={true} onClick={() => switchTo(Refs.IdsIterateActions)} style={{marginRight: '10px'}} />
                    <i>{actionsChosen.join(', ')}</i>
                </div>
                <hr />
                <div className="ids-iterate-on-actions">
                    <RaisedButton label="Действия над списками..." secondary={true} onClick={() => switchTo(Refs.IdsIterateOnActions)} style={{marginRight: '10px'}} />
                    <i>{listActionsChosen}</i>
                </div><br />

            <CreateTaskBtns isReadyCreate={isReadyCreate} {...this.props} onCreate={() => this.create()}
                                onChangeScheduler={taskScheduler => store.dispatch(Actions.idsIterateTC.setTaskScheduler(taskScheduler))} />
            </div>
        )
    }
}
IdsIterateTaskCreator.propTypes = {
    onCreate: React.PropTypes.func.isRequired
};
