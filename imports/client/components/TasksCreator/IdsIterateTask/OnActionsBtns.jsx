import React from 'react';
import { TaskActionTypes } from "../../../../common/types";
import { TasksCreatorRefs as Refs } from "../TasksCreator";
import {  RaisedButton } from 'material-ui';

const styles = {
    listNames: {
        marginLeft: '10px',
        fontStyle: 'italic',
        lineHeight: '2'
    },
    listBtn: {
        float: 'left'
    },
    header: {
        fontWeight: 'bold',
        paddingBottom: '20px',
        marginLeft: '15px'
    }
};

export default class IdsIterateOnActions extends React.Component {
    render() {
        const { ADD_TO_USER_ID_LISTS, REMOVE_FROM_USER_ID_LISTS } = TaskActionTypes;
        const { switchTo, params } = this.props;

        let onOkAddChosen = params[ADD_TO_USER_ID_LISTS].onOK || [];
        let onOkRemoveChosen = params[REMOVE_FROM_USER_ID_LISTS].onOK || [];
        let onFailAddChosen = params[ADD_TO_USER_ID_LISTS].onFail || [];
        let onFailRemoveChosen = params[REMOVE_FROM_USER_ID_LISTS].onFail || [];

        return (
            <div>
                <div style={styles.header}>При успешном выполнении действия:</div>
                <RaisedButton style={styles.listBtn} label="Добавить в списки..." secondary={true} onClick={() => switchTo(Refs.UserIdListsAddOnOk)} />
                <span style={styles.listNames}>{onOkAddChosen.join(', ')}</span><br /><br />
                <RaisedButton style={styles.listBtn} label="Удалить из списков..." secondary={true} onClick={() => switchTo(Refs.UserIdListsRemoveOnOk)} />
                <span style={styles.listNames}>{onOkRemoveChosen.join(', ')}</span><br /><br/><hr />

                <div style={styles.header}>При неудачном выполнении действия:</div>
                <RaisedButton style={styles.listBtn} label="Добавить в списки..." secondary={true} onClick={() => switchTo(Refs.UserIdListsAddOnFail)} />
                <span style={styles.listNames}>{onFailAddChosen.join(', ')}</span><br /><br />
                <RaisedButton style={styles.listBtn} label="Удалить из списков..." secondary={true} onClick={() => switchTo(Refs.UserIdListsRemoveOnFail)} />
                <span style={styles.listNames}>{onFailRemoveChosen.join(', ')}</span><br />

                <br />
                <div className="right-btn">
                    <RaisedButton label="OK" primary={true} onClick={this.props.onFinished} />
                </div>
            </div>
        )
    }
}
IdsIterateOnActions.propTypes = {
    switchTo: React.PropTypes.func.isRequired,
    onFinished: React.PropTypes.func.isRequired
};
