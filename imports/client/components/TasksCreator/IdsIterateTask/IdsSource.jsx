import React from 'react';
import * as _ from 'lodash';
import store from '../../../redux/store';
import { Actions } from "../../../redux/actions";
import { IdsSourceTypes } from "../../../../common/types";
import { TasksCreatorRefs as Refs } from "../TasksCreator";
import { CheckBoxDetails, DropDownDetails } from "../../helpers";
import { DropDownMenu, MenuItem } from 'material-ui';
import { ItemsSelect } from '../../helpers';

export default class IdsIterateSource extends React.Component {
    constructor(props) {
        super(props);
        this.state = { idsListNames: [] };
    }
    componentWillMount() {
        store.dispatch(Actions.userIdListManager.loadListNames()).then(idsListNames =>
            this.setState({ idsListNames: idsListNames.payload })
        );
        //store.dispatch(Actions.userIdListManager.switchList(this.props.userIdListName));
    }
    //componentDidMount() {
    //    let dropdownKostil = () => { // update interface fo fix dropdown render bug in old material-ui
    //        let val = this.props.idsSource.listInfLoop;
    //        Meteor.setTimeout(() => {
    //            this.listInfLoopChanged(!val);
    //        }, 1000);
    //        Meteor.setTimeout(() => {
    //            this.listInfLoopChanged(val);
    //        }, 1500);
    //        //Meteor.defer(() => this.listInfLoopChanged(val));
    //    };
    //    dropdownKostil();
    //},
    componentWillReceiveProps(nextProps) {
        if (nextProps.userIdListName != this.props.userIdListName)
            store.dispatch(Actions.userIdListManager.switchList(nextProps.userIdListName));
    }
    onChange(e, i, item) {
        //store.dispatch(Actions.idsIterateTC.setIdsSource(item.payload));
        store.dispatch(Actions.idsIterateTC.setIdsSource({ type: item }));
    }
    listInfLoopChanged(enabled) {
        store.dispatch(Actions.idsIterateTC.setIdsSource({listInfLoop: enabled}));
    }
    render() {
        const { IDS_COMMON_LIST, IDS_COPY_LIST, SEARCH_URLS, FRIENDS, FRIEND_REQUESTS_IN, FRIEND_REQUESTS_OUT } = IdsSourceTypes;
        const { switchTo, idsSource, userIdListName } = this.props;
        if (idsSource.type == IDS_COMMON_LIST || idsSource.type == IDS_COPY_LIST) {
            let lists = _.map(this.state.idsListNames, listName => ({payload: listName, text: listName}));
            // let listIndex = () => {
            //     let i = this.state.idsListNames.indexOf(this.props.userIdListName);
            //     return i >= 0 ? i : 0;
            // };
            let onSwitchList = userIdListName => {
                store.dispatch(Actions.idsIterateTC.setIdsSource({userIdListName}));
                //store.dispatch(Actions.idsIterateTC.setParams({idsSource: {userIdListName}}));
                //store.dispatch(Actions.userIdListManager.switchList(userIdListName));
            };
            var itemsSelect = (
                <div style={{display: 'flex'}}>
                    <div className="ids-iterate-source-title">Список ID: &nbsp;&nbsp;&nbsp;&nbsp;</div>
                    <DropDownDetails value={userIdListName}
                                 onChange={(e, i, item) => onSwitchList(item)}
                                 onDetails={() => switchTo(Refs.IdLists)}>
                        { lists.map((item, i) => (
                            <MenuItem key={i} value={item.payload} primaryText={item.text} />
                        )) }
                    </DropDownDetails>
                </div>
            );
            //let listInfLoopChanged = enabled =>
            //    store.dispatch(Actions.idsIterateTC.setIdsSource({listInfLoop: enabled}));
            var listInfLoop = (
                <div style={{ marginTop: '10px' }}>
                    <CheckBoxDetails
                        ref="listInfLoop" label="Цикл по списку" defaultChecked={idsSource.listInfLoop}
                        onCheck={(e, checked) => this.listInfLoopChanged(checked)}
                    />
                </div>
            );
        }
        if (idsSource.type == SEARCH_URLS)
            itemsSelect = (
                <ItemsSelect items={idsSource.urls} itemName="URL" onEdit={() => { switchTo(Refs.SearchUrlsEdit, idsSource) }} />
            );
        return (
            <div>
                <div style={{ display: 'flex' }}>
                    <div className="ids-iterate-source-title">Источник ID: </div>
                    <div className="ids-iterate-source">
                        <DropDownMenu value={idsSource.type} onChange={(...args) => this.onChange(...args)}>
                            <MenuItem value={IDS_COMMON_LIST} primaryText='Заданный список (общий для всех акков)' />
                            <MenuItem value={IDS_COPY_LIST} primaryText='Заданный список (дублируется для каждого акка)' />
                            <MenuItem value={SEARCH_URLS} primaryText='По поисковым ссылкам ВК' />
                            <MenuItem value={FRIENDS} primaryText='По друзьям' />
                            <MenuItem value={FRIEND_REQUESTS_IN} primaryText='Входящие заявки в друзья' />
                            <MenuItem value={FRIEND_REQUESTS_OUT} primaryText='Исходящие заявки в друзья' />
                        </DropDownMenu>
                    </div>
                </div>
                <div style={{ display: 'flex' }}>
                    { itemsSelect } { listInfLoop }
                </div>
            </div>
        )
    }
}
IdsIterateSource.propTypes = {
    //idsSource: React.PropTypes.shape({
    type: React.PropTypes.string.isRequired,
    userIdListName: React.PropTypes.string,
    urls: React.PropTypes.array,
    //}),
    switchTo: React.PropTypes.func.isRequired
};
