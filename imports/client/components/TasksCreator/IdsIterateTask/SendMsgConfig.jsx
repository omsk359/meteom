import React from 'react';
import store from '../../../redux/store';
import { Actions } from "../../../redux/actions";
import { TaskActionTypes } from "../../../../common/types";
import { TaskFilterTypes } from "../../../../common/types";
import { TasksCreatorRefs as Refs } from "../TasksCreator";
import { CheckBoxDetails } from "../../helpers";
import { RaisedButton, TextField } from 'material-ui';

export default class SendMsgConfig extends React.Component {
    changed() {
        let params = {
            idsOkCntMax: this.refs.idsOkCntMax.getValue()
        };
        store.dispatch(Actions.idsIterateTC.setParams({[TaskActionTypes.SEND_MSG]: params}));
    }
    render() {
        const { SEND_MSG, ADD_TO_USER_ID_LISTS } = TaskActionTypes;
        const { idsOkCntMax, msg } = this.props.params[SEND_MSG] || {};
        const { USER_ID_LISTS } = TaskFilterTypes;
        const { switchTo, onFinished, isReady } = this.props;
        let checkActions = [
            { value: ADD_TO_USER_ID_LISTS, text: 'Добавить в списки', checked: _.includes(this.props.onActions[SEND_MSG].onOK, ADD_TO_USER_ID_LISTS), details: Refs.UserIdListsSendMsg }
        ];
        let onToggleAction = (aType, checked) => {
            store.dispatch(Actions.idsIterateTC.setOnAction({ causeAction: SEND_MSG, action: aType, enable: checked }));
        };
        let actions = _.map(checkActions, item => (
            <CheckBoxDetails
                key={item.value}
                value={item.value}
                label={item.text}
                className="action-onOK"
                defaultChecked={item.checked}
                disabled={item.disabled}
                onDetails={item.details && (() => switchTo(item.details))}
                onCheck={(e, checked) => onToggleAction(item.value, checked)}
            />
        ));

        //let checkFilters = [
        //    { value: USER_ID_LISTS, text: 'Нет в списках', details: Refs.UserIdListsSendMsgFilters }
        //];
        //let onToggleFilter = (fType, checked) => {
        //    store.dispatch(Actions.idsIterateTC.setFilter({ action: SEND_MSG, filter: fType, enable: checked }));
        //};
        //let selectedFilters = _.flatten(_.values(this.props.filters));
        //let filters = _.map(checkFilters, item => (
        //    <CheckBoxDetails
        //        key={item.value} name={item.value} value={item.value}
        //        label={item.text}
        //        defaultChecked={_.includes(selectedFilters, item.value)}
        //        disabled={item.disabled}
        //        details={!!item.details}
        //        onDetails={() => switchTo(item.details)}
        //        onCheck={(e, checked) => onToggleFilter(item.value, checked)}
        //    />
        //));
        //<div className="checkbox-group-title">Фильтры рассылки</div>
        //<div className="checkbox-group">
        //    { filters }
        //    </div><hr />
        //<div className="checkbox-group-title">Действия при успешной отправке</div>
        //<div className="checkbox-group">
        //    { actions }
        //</div>
        return (
            <div>
                <div className="checkbox-group-title">Параметры рассылки сообщений</div>
                <div style={{display: 'flex'}}>
                    <RaisedButton onClick={() => switchTo(Refs.SendMsgEdit)} label="Изменить сообщения..." secondary={true} style={{marginTop: '7px'}} />
                    <div style={{marginLeft: '40px'}}>
                        <span>Макс. ограничение: </span>
                        <TextField ref="idsOkCntMax" type="number" defaultValue={idsOkCntMax} onChange={() => this.changed()} className="num-field" id="idsOkCntMax" /><br />
                    </div>
                </div>

                <div className="right-btn">
                    <RaisedButton onClick={onFinished} label="OK" disabled={!isReady[SEND_MSG]} primary={true} style={{ margin: '10px 10px' }} />
                </div>
            </div>
        )
    }
}
SendMsgConfig.propTypes = {
    idsSource: React.PropTypes.object,
    actions: React.PropTypes.arrayOf(React.PropTypes.string),
    switchTo: React.PropTypes.func.isRequired,
    onFinished: React.PropTypes.func.isRequired
};
