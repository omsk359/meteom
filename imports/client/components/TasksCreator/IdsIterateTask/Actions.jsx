import React from 'react';
import * as _ from 'lodash';
import { TaskActionTypes } from "../../../../common/types";
import { Actions } from "../../../redux/actions";
import store from '../../../redux/store';
import { TasksCreatorRefs as Refs } from "../TasksCreator";
import { CheckBoxDetails } from "../../helpers";
import { RaisedButton } from 'material-ui';

export default class IdsIterateActions extends React.Component {

    static getCheckActions() {
        const { INVITE, SEND_MSG, DELETE_FRIEND, ADD_TO_USER_ID_LISTS, NOPE } = TaskActionTypes;
        return [
            { value: INVITE, text: 'Инвайт', details: Refs.InviteTaskConfig },
            { value: SEND_MSG, text: 'Сообщение', details: Refs.SendMsgConfig },
            { value: DELETE_FRIEND, text: 'Удалить друга' },
            //{ value: ADD_TO_INVITE_DB, text: 'Добавить в базу Invite', disabled: true },
            { value: NOPE, text: 'Ничего' },
            //{ value: ADD_TO_USER_ID_LISTS, text: 'Добавить в списки', details: Refs.UserIdListsAction }
            //{ value: ADD_TO_USER_ID_LISTS, text: 'Добавить в списки', checked: _.includes(this.props.onActions[IDS_ITERATE.SELECTED_ACTIONS].onOK, ADD_TO_USER_ID_LISTS), details: Refs.UserIdListsAction }
            //{ value: SEND_MSG, text: 'Добавить в списки', details: Refs.UserIdListsSelect },
        ];
    }
    actionReady(type) {
        if (type in this.props.isReady)
            return this.props.isReady[type];
        return true;
    }
    render() {
        const checkActions = IdsIterateActions.getCheckActions();
        const onToggle = (aType, checked) => {
            if (checked)
                store.dispatch(Actions.idsIterateTC.addAction(aType));
            else
                store.dispatch(Actions.idsIterateTC.removeAction(aType));
        };
        let actions = _.map(checkActions, item => (
            <CheckBoxDetails
                key={item.value} name={item.value} value={item.value}
                label={item.text}
                defaultChecked={_.includes(this.props.actions, item.value)}
                disabled={item.disabled || !this.actionReady(item.value)}
                onDetails={item.details && (() => this.props.switchTo(item.details))}
                onCheck={(e, checked) => onToggle(item.value, checked)}
            />
        ));
        return (
            <div>
                <div className="ids-iterate-header">Действия</div>
                <div>
                    { actions }
                </div>
                <div className="right-btn">
                    <RaisedButton label="OK" primary={true} onClick={this.props.onFinished} />
                </div>
            </div>
        )
    }
}
IdsIterateActions.propTypes = {
    idsSource: React.PropTypes.object.isRequired,
    switchTo: React.PropTypes.func.isRequired
};
