import React from 'react';
import * as _ from 'lodash';
import store from '../../../redux/store';
import { TaskFilterTypes } from "../../../../common/types";
import { TaskActionTypes } from "../../../../common/types";
import { Actions } from "../../../redux/actions";
import { TasksCreatorRefs as Refs } from "../TasksCreator";
import { merge } from '../../../../common/helpers';
import { CheckBoxDetails } from "../../helpers";
import { RaisedButton } from 'material-ui';

export default class IdsIterateFilters extends React.Component {

    static getCheckFilters() {
        const types = merge(TaskFilterTypes, TaskActionTypes);
        return [
            { value: types.ONLINE, text: 'Онлайн'/*, disabled: idsSource.type == IdsSourceTypes.SEARCH_URLS*/ },
            { value: types.LAST_SEEN, text: 'Последнее посещение', details: Refs.LastSeen },
            { value: types.FRIENDS, text: 'Есть в друзьях' },
            { value: types.USER_ID_LISTS, text: 'Нет в списках', details: Refs.UserIdListsFilters },
            //{ value: types.NOT_EXIST_OUT_MSG, text: 'Нет исходящих ссобщений' },
            //{ value: types.NOT_IN_INVITE_DB, text: 'Нет в базе Invite', disabled: true, hidden: true },
            //{ value: types.NOT_IN_OLD_INVITE_DB, text: 'Нет в базе Invite (rhcloud)', disabled: true, hidden: true }
        ];
    }
    render() {
        const checkFilters = IdsIterateFilters.getCheckFilters();
        const onToggle = (fType, checked) => {
            store.dispatch(Actions.idsIterateTC.setFilter({ filter: fType, enable: checked }));
            //if (checked)
            //    store.dispatch(Actions.idsIterateTC.addFilter({ filter: fType }));
            //else
            //    store.dispatch(Actions.idsIterateTC.removeFilter({ filter: fType }));
        };
        let selectedFilters = _.flatten(_.values(this.props.filters));
        let filters = _.map(checkFilters, item => (
            <CheckBoxDetails
                key={item.value} name={item.value} value={item.value}
                label={item.text}
                defaultChecked={_.includes(selectedFilters, item.value)}
                disabled={item.disabled}
                onDetails={item.details && (() => this.props.switchTo(item.details))}
                onCheck={(e, checked) => onToggle(item.value, checked)}
            />
        ));

        return (
            <div>
                <div className="ids-iterate-header">Фильтры</div>
                <div>
                    { filters }
                </div>
                <div className="right-btn">
                    <RaisedButton label="OK" primary={true} onClick={this.props.onFinished} />
                </div>
            </div>
        )
    }
}
IdsIterateFilters.propTypes = {
    idsSource: React.PropTypes.object.isRequired,
    actions: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
    switchTo: React.PropTypes.func.isRequired,
    onFinished: React.PropTypes.func.isRequired
};
