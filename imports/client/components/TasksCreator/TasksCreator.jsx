import React from 'react';
import * as _ from 'lodash';
import store from '../../redux/store';
import { log } from "../../../common/logger/Logger";
import { Actions } from "../../redux/actions";
import { TaskFilterTypes } from "../../../common/types";
import { TaskActionTypes } from "../../../common/types";
import { taskNameText } from "../helpers";
import { merge } from '../../../common/helpers';
import { RaisedButton, FontIcon, Dialog, List, ListItem } from 'material-ui';
import { StackedLayoutRefs } from "../helpers";
import DialogsHistoryTaskCreator from "./DialogsHistory";
import IdsIterateTaskCreator from "./IdsIterateTask/TaskCreator";
import SendMsgConfig from "./IdsIterateTask/SendMsgConfig";
import AccEditManager from "../AccEdit";
import IdListsManager from "../IdLists";
import InviteTaskConfig from "./IdsIterateTask/InviteConfig";
import IdsCollectTask from "./IdsCollectTask";
import SearchUrlsEdit from "../SearchUrlsEdit";
import DateTimePicker from "../DateTimePicker";
import RenameAccsTask from "./RenameAccs";
import MultiLineEdit from "../MultiLineEdit";
import ClearAcc from "./ClearAcc";
import TaskScheduler from "./TaskScheduler";
import ListMultiSelect from "../ListMultiSelect";
import IdsIterateFilters from "./IdsIterateTask/Filters";
import Delays from "../Delays";
import UnfreezeAccsTask from "./UnfreezeAccs";
import RegAccsTask from "./RegAccs";
import BindPhoneTask from "./BindPhoneTask";
import IdsIterateActions from "./IdsIterateTask/Actions";
import IdsIterateOnActions from "./IdsIterateTask/OnActionsBtns";
import TaskSchedulerBtn from "./TaskSchedulerBtn";

export const TasksCreatorRefs = {
    FirstPage: 'FirstPage',
    DialogsHistoryTask: 'DialogsHistoryTask',
    LikeOutRequestsTask: 'LikeOutRequestsTask',
    IdsIterateTask: 'IdsIterateTask',
    InviteTaskConfig: 'InviteTaskConfig',
    AccEdit: 'AccEdit',
    IdLists: 'IdListsManager',
    IdsEdit: 'IdsEdit',
    IdsIterateDelays: 'IdsIterateDelays',
    IdsCollectTask: 'IdsCollectTask',
    SearchUrlsEdit: 'SearchUrlsEdit',
    LastSeen: 'LastSeen',
    RenameAccsTask: 'RenameAccsTask',
    RenameAccsFirstNames: 'RenameAccsFirstNames',
    RenameAccsLastNames: 'RenameAccsLastNames',
    ClearAcc: 'ClearAcc',
    InviteMsgEdit: 'InviteMsgEdit',
    SendMsgEdit: 'SendMsgEdit',
    UnfreezeAccsTask: 'UnfreezeAccsTask',
    TaskScheduler: 'TaskScheduler',
    RegAccsTask: 'RegAccsTask',
    RegAccsFirstNames: 'RegAccsFirstNames',
    RegAccsLastNames: 'RegAccsLastNames',
    RegAccsProxiesEdit: 'RegAccsProxiesEdit',
    BindPhoneTask: 'BindPhoneTask',
    UserIdListsInvite: 'UserIdListsInvite',
    //UserIdListsInviteFilters: 'UserIdListsInviteFilters',
    UserIdListsSendMsg: 'UserIdListsSendMsg',
    //UserIdListsSendMsgFilters: 'UserIdListsSendMsgFilters',
    SendMsgConfig: 'SendMsgConfig',
    UserIdListsFilters: 'UserIdListsFilters',
    UserIdListsAction: 'UserIdListsAction',
    UserIdListsAddOnOk: 'UserIdListsAddOnOk',
    UserIdListsRemoveOnOk: 'UserIdListsRemoveOnOk',
    UserIdListsAddOnFail: 'UserIdListsAddOnFail',
    UserIdListsRemoveOnFail: 'UserIdListsRemoveOnFail',
    IdsIterateFilters: 'IdsIterateFilters',
    IdsIterateActions: 'IdsIterateActions',
    IdsIterateOnActions: 'IdsIterateOnActions'
};

export default class TasksCreator extends React.Component {
    constructor(props) {
        super(props);
//        this.switchTo = (...args) => this.switchTo(...args);
//        this.goBack = (...args) => this.goBack(...args);
    }
    open()  {
        store.dispatch(Actions.taskCreator.setHidden(false));
        this.switchTo(TasksCreatorRefs.FirstPage);
        //this.switchTo(TasksCreatorRefs.LastSeen);
    }
    close() { store.dispatch(Actions.taskCreator.setHidden(true))  }
    create(taskName, params) {
        store.dispatch(Actions.tasks.create(taskName, params)).then(() => this.close());
    }
    onRequestClose() {
        if (_.last(this.props.history) == TasksCreatorRefs.FirstPage)
            this.close();
        else
            this.goBack();
            //this.switchTo(TasksCreatorRefs.FirstPage);
    }
    switchTo = (refName, params) => {  // es7 class props init - autobinding
        store.dispatch(Actions.taskCreator.switchTo(refName, params));

        if (refName == TasksCreatorRefs.LastSeen) // fix pickers margin in the dialog
            location.reload();
    }
    goBack = result => {
        store.dispatch(Actions.taskCreator.switchBack(result));
    }
    componentWillMount() {
        let currView = _.last(this.props.history), Refs = TasksCreatorRefs;
        if (_.includes([Refs.TaskScheduler, Refs.SearchUrlsEdit], currView))
            this.goBack();
        //this.switchTo(TasksCreatorRefs.FirstPage);
    }
    render() {
        //log.debug('TasksCreator props: {1}', this.props);
        const { switchTo, goBack } = this;
        const { hidden, accManagerStore, userIdListManager, switchToParams } = this.props;
        const idsIterateProps = merge(this.props.idsIterateTaskCreator, { switchTo });
        const passProps = { switchTo, ...this.props }, Refs = TasksCreatorRefs;
        const create = ref => this.create.bind(this, ref);
        const currentView = _.last(this.props.history);
        let lastSeen = new Date(idsIterateProps.params[TaskFilterTypes.LAST_SEEN].date);
        const renameAccs = { ...this.props.renameAccsTaskCreator, switchTo };
        const unfreezeAccs = { ...this.props.unfreezeAccsTaskCreator, switchTo };
        const bindPhone = { ...this.props.bindPhoneTaskCreator, switchTo };
        let regAccs = { ...this.props.regAccsTaskCreator, switchTo };
        _.assign(regAccs, _.pick(this.props, ['firstNames', 'lastNames', 'proxies']));
        const { INVITE, SEND_MSG, ADD_TO_USER_ID_LISTS, REMOVE_FROM_USER_ID_LISTS } = TaskActionTypes;
        const { USER_ID_LISTS, NOT_IN_OLD_INVITE_DB } = TaskFilterTypes;
        //<IdsEdit ref={Refs.IdsEdit} ids={idsIterateProps.idsSource.ids} onFinished={goBack} />
        let sendMsgDesc = `
        `;
        //let taskScheduler = currentView;
        //ref = this.refs[taskScheduler];
        //let taskSchedulerBack = result => {
        //    store.dispatch(Actions.taskCreator.setTaskScheduler(result));
        //    goBack(result);
        //};
        let title = 'Добавить задачу';
        if (this.props.history.length > 1)
            title = `Добавить задачу: ${taskNameText(this.props.history[1])}`;
        //<MultiLineEdit ref={Refs.IdsEdit} lines={idsIterateProps.idsSource.ids} onFinished={goBack} title="Редактирование ID" floatingText="Список ID" />
        let userIdListNames = _.keys(userIdListManager.lists);
        let addListNames = idsIterateProps.params[ADD_TO_USER_ID_LISTS];
        let removeListNames = idsIterateProps.params[REMOVE_FROM_USER_ID_LISTS];
        let listNamesFilters = idsIterateProps.params[USER_ID_LISTS];
        let userIdListsProps = { switchTo, all: userIdListNames, onFinished: goBack };
        //<ListMultiSelect ref={Refs.UserIdListsSendMsgFilters} {...userIdListsProps} selected={listNamesFilters[SEND_MSG]} desc={`
        //                    <i>Если человек есть хотя бы в одном из выбранных списков, то он будет пропущен.</i>
        //                `} />
        //<ListMultiSelect ref={Refs.UserIdListsInvite} {...userIdListsProps} selected={listNames[INVITE]} desc={`
        //                    <i>При успешной отправке, ID человека будет добавлен в выбранные списки.</i>
        //                `} />
        return (
            <div>
                <Dialog title={title} open={!hidden} onRequestClose={() => this.onRequestClose()}>
                    <StackedLayoutRefs currentRef={currentView}>
                        <List ref={Refs.FirstPage}>
                            <ListItem key="1" onClick={() => switchTo(Refs.IdsIterateTask)} primaryText={taskNameText(Refs.IdsIterateTask)} leftIcon={<FontIcon className="material-icons">people</FontIcon>} />
                            <ListItem key="2" onClick={() => switchTo(Refs.DialogsHistoryTask)} primaryText={taskNameText(Refs.DialogsHistoryTask)} leftIcon={<FontIcon className="material-icons">save</FontIcon>} />
                            <ListItem key="3" onClick={() => switchTo(Refs.IdsCollectTask)} primaryText={taskNameText(Refs.IdsCollectTask)} leftIcon={<FontIcon className="material-icons">group_add</FontIcon>} />
                            <ListItem key="4" onClick={() => switchTo(Refs.RenameAccsTask)} primaryText={taskNameText(Refs.RenameAccsTask)} leftIcon={<FontIcon className="material-icons">delete_sweep</FontIcon>} />
                            <ListItem key="5" onClick={() => switchTo(Refs.UnfreezeAccsTask)} primaryText={taskNameText(Refs.UnfreezeAccsTask)} leftIcon={<FontIcon className="material-icons">sim_card</FontIcon>} />
                            <ListItem key="6" onClick={() => switchTo(Refs.RegAccsTask)} primaryText={taskNameText(Refs.RegAccsTask)} leftIcon={<FontIcon className="material-icons">sim_card</FontIcon>} />
                            <ListItem key="7" onClick={() => switchTo(Refs.BindPhoneTask)} primaryText={taskNameText(Refs.BindPhoneTask)} leftIcon={<FontIcon className="material-icons">sim_card</FontIcon>} />
                        </List>
                        <DialogsHistoryTaskCreator ref={Refs.DialogsHistoryTask} {...passProps} {...this.props.dialogsHistoryTaskCreator} onCreate={create(Refs.DialogsHistoryTask)} />
                        <IdsIterateTaskCreator ref={Refs.IdsIterateTask} {...idsIterateProps} onCreate={create(Refs.IdsIterateTask)} />
                        <InviteTaskConfig ref={Refs.InviteTaskConfig} {...idsIterateProps} onFinished={goBack} />
                        <SendMsgConfig ref={Refs.SendMsgConfig} {...idsIterateProps} onFinished={goBack} />
                        <AccEditManager ref={Refs.AccEdit} {...accManagerStore} onFinished={goBack} />
                        <IdListsManager ref={Refs.IdLists} {...userIdListManager} onFinished={goBack} />
                        <Delays ref={Refs.IdsIterateDelays} delays={idsIterateProps.delays} onFinished={goBack} />
                        <IdsCollectTask ref={Refs.IdsCollectTask} {...passProps} {...this.props.idsCollectTaskCreator} onCreate={() => this.create()} />
                        <SearchUrlsEdit ref={Refs.SearchUrlsEdit} searchUrlParams={switchToParams} onFinished={goBack} title="Редактирование ссылок" floatingText="Список URL" desc={`
                           <i>Параметры "Главное в жизни/в людях", "Отношение к курению/алко", "Военная служба" будут игнорированы.
                           Если в поисковом запросе отсутствует возраст, то он устанасвливается по умолчанию <b>17-30</b> лет.</i>
                        `} />
                        <DateTimePicker ref={Refs.LastSeen} date={lastSeen} onFinished={goBack} title="Последнее посещение" dateHint="Начальная дата" timeHint="Начальное время" />

                        <RenameAccsTask ref={Refs.RenameAccsTask} {...renameAccs} onCreate={create(Refs.RenameAccsTask)} />
                        <MultiLineEdit ref={Refs.RenameAccsFirstNames} lines={renameAccs.firstNames} onFinished={goBack} title="Редактирование имен" floatingText="Список имен" />
                        <MultiLineEdit ref={Refs.RenameAccsLastNames} lines={renameAccs.lastNames} onFinished={goBack} title="Редактирование фамилий" floatingText="Список фамилий" />
                        <ClearAcc ref={Refs.ClearAcc} params={renameAccs.clearParams} onFinished={goBack} />
                        <MultiLineEdit ref={Refs.InviteMsgEdit} lines={[idsIterateProps.params[INVITE].msg]} onFinished={goBack} title="Сообщение при отправке инвайтов" floatingText="{a|б|c}[2,3,', ',0]" desc={`
                            <i>Сообщение может содержать части в фигурных скобках, в которых разные варианты разделены cимволом |. Используется для вставки 1 из вариантов случайным образом.<br>
                            <span class="desc-example">Пример: &nbsp; <b>{Привет|Здравствуй}, %username%!</b> &nbsp; -> &nbsp; <b>Здравствуй, Катя!</b></span><br>
                            Если нужно вставить несколько вариантов, можно добавить после фигурных скобочек квадратные с указанием диапазона кол-ва вариантов.<br>
                            <span class="desc-example">Пример: &nbsp; <b>Вас интересует {а|б|в|г}[2,3]?</b> &nbsp; -> &nbsp; <b>Вас интересует б, а, г?</b></span></i>
                        `} />
                    <MultiLineEdit ref={Refs.SendMsgEdit} lines={idsIterateProps.params[SEND_MSG].msgs} rows={7} rowsMax={12} splitter={'\n\n'} onFinished={goBack} title="Список сообщений" floatingText="{a|б|c}[2,3,', ',0]" desc={`
                                <i>Одно сообщение можно разделить на несколько последовательных сообщений с заданными задержками.
                                Например, 3 сообщения со случайными задержками перед 2-м (30-60 сек) и 3-м сообщением (10-20 сек):<br>
                                <span class="desc-example"><b>{Привет, %username%!|Доброго времени}<br>
                                $[30-60]Как вы {считаете|думаете} ...?<br>
                                $[10-20]Вас вообще интересуют подобные темы?</b></span><br>
                                Также можно задать разные сообщения для разных аккаунтов разделив их пустой строкой.</i><br>
                        `} />
                        <UnfreezeAccsTask ref={Refs.UnfreezeAccsTask} {...unfreezeAccs} onCreate={create(Refs.UnfreezeAccsTask)} />
                        <TaskScheduler ref={Refs.TaskScheduler} onFinished={goBack} {...switchToParams} />
                        <RegAccsTask ref={Refs.RegAccsTask} {...regAccs} onCreate={create(Refs.RegAccsTask)} />
                        <MultiLineEdit ref={Refs.RegAccsProxiesEdit} lines={regAccs.proxies} onFinished={goBack} title="Редактирование прокси" floatingText="Список прокси" />
                        <MultiLineEdit ref={Refs.RegAccsFirstNames} lines={regAccs.firstNames} onFinished={goBack} title="Редактирование имен" floatingText="Список имен" />
                        <MultiLineEdit ref={Refs.RegAccsLastNames} lines={regAccs.lastNames} onFinished={goBack} title="Редактирование фамилий" floatingText="Список фамилий" />
                        <BindPhoneTask ref={Refs.BindPhoneTask} {...bindPhone} onCreate={create(Refs.BindPhoneTask)} />
                        <ListMultiSelect ref={Refs.UserIdListsFilters} {...userIdListsProps} selected={listNamesFilters} desc={`
                            <i>Если человек есть хотя бы в одном из выбранных списков, то он будет пропущен.</i>
                        `} />
                        <ListMultiSelect ref={Refs.UserIdListsAddOnOk} {...userIdListsProps} selected={addListNames.onOK} desc={`
                            <i>ID человека будет добавлен в выбранные списки (без дублей).</i>
                        `} />
                        <ListMultiSelect ref={Refs.UserIdListsRemoveOnOk} {...userIdListsProps} selected={removeListNames.onOK} desc={`
                            <i>ID человека будет удален из выбранных списков.<br>
                            Если происходит удаление из списка, из которого происходит <b>нециклическое</b>
                            чтение (как из источника ID) в какой-то задаче, то <b>некоторые ID могут быть пропущены</b>, даже если они не удалены!
                            Сюда также также относится вариант "Дублирование списка для всех акков", т.к. фактически список один и тот же.
                            </i>
                        `} />
                        <ListMultiSelect ref={Refs.UserIdListsAddOnFail} {...userIdListsProps} selected={addListNames.onFail}
                                         desc='<i>ID человека будет добавлен в выбранные списки (без дублей).</i>' />
                        <ListMultiSelect ref={Refs.UserIdListsRemoveOnFail} {...userIdListsProps} selected={removeListNames.onFail}
                                         desc='<i>ID человека будет удален из выбранных списков.</i>' />
                        <IdsIterateFilters ref={Refs.IdsIterateFilters} {...idsIterateProps} onFinished={goBack} />
                        <IdsIterateActions ref={Refs.IdsIterateActions} {...idsIterateProps} onFinished={goBack} />
                        <IdsIterateOnActions ref={Refs.IdsIterateOnActions} {...idsIterateProps} onFinished={goBack} />
                    </StackedLayoutRefs>
                </Dialog>
            </div>
        )
    }
}

export class CreateTaskBtns extends React.Component {
    render() {
        let { isReadyCreate, onCreate } = this.props;
        return (
            <div style={{display: 'block'}}>
                <div style={{float: 'right', display: 'flex'}}>
                    <TaskSchedulerBtn {...this.props} />
                    <div>
                        <RaisedButton label="Готово" primary={true} onClick={onCreate} disabled={!isReadyCreate()} />
                    </div>
                </div>
            </div>
        )
    }
}
CreateTaskBtns.defaultProps = {
    isReadyCreate: () => true
};
CreateTaskBtns.propTypes = {
    onCreate: React.PropTypes.func.isRequired,
    onChangeScheduler: React.PropTypes.func.isRequired,
    switchTo: React.PropTypes.func.isRequired,
    isReadyCreate: React.PropTypes.func,
    taskScheduler: React.PropTypes.object,
};
