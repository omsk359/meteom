import React from 'react';
import * as _ from 'lodash';
import store from '../../redux/store';
import { TasksCreatorRefs as Refs } from "./TasksCreator";
import { Actions } from "../../redux/actions";
import { CreateTaskBtns } from "./TasksCreator";
import { DropDownDetails } from "../helpers";
import { ItemsSelect } from '../helpers';
import { MenuItem } from 'material-ui';

export default class IdsCollectTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = { idsListNames: [] };
    }
    create() {
        let { accs, searchUrlParams, idsListName, taskScheduler, algorithm } = this.props;
        let params = {
            accs: [_.head(accs)],
            idsListName,
            idsSearchParams: { accs, ...searchUrlParams },
            taskScheduler,
            algorithm
        };
        this.props.onCreate(Refs.IdsCollectTask, params);
    }
    changed() {
        let urls = this.refs.urls.getValue();
        store.dispatch(Actions.idsCollectTC.setParams({ urls }));
    }
    componentWillMount() {
        store.dispatch(Actions.userIdListManager.loadListNames()).then(idsListNames =>
            this.setState({ idsListNames: idsListNames.payload })
        );
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.idsListName != this.props.idsListName)
            store.dispatch(Actions.userIdListManager.switchList(nextProps.idsListName));
    }
    // listIndex() {
    //     let i = this.state.idsListNames.indexOf(this.props.idsListName);
    //     return i >= 0 ? i : 0;
    // }
    render() {
        const { accs, switchTo, accList, searchUrlParams, idsListName, algorithm } = this.props;
        let isReadyCreate = () => !!accs.length && searchUrlParams.urls.length;
        let onSwitchList = idsListName => {
            store.dispatch(Actions.idsCollectTC.setParams({ idsListName }));
        };
        let onSwitchAlg = algorithm => {
            store.dispatch(Actions.idsCollectTC.setParams({ algorithm }));
        };
        let lists = _.map(this.state.idsListNames, listName => ({ payload: listName, text: listName }));
        let algList = [{ payload: 'iterative', text: 'Последовательный' }, { payload: 'parallel', text: 'Параллельный' }];
        return (
            <div>
                <ItemsSelect items={accs} itemName="аккаунтов" groupName={accList} onEdit={() => switchTo(Refs.AccEdit)} />
                <ItemsSelect items={searchUrlParams.urls} itemName="URL" onEdit={() => { switchTo(Refs.SearchUrlsEdit, searchUrlParams) }} />
                <span>Сохранить в: </span>
                <DropDownDetails value={idsListName}
                                 onChange={(e, i, item) => onSwitchList(item)}
                                 onDetails={() => switchTo(Refs.IdLists)}>
                    { lists.map((item, i) => (
                        <MenuItem key={i} value={item.payload} primaryText={item.text} />
                    )) }
                </DropDownDetails>
                <span>Алгоритм: </span>
                <DropDownDetails value={algorithm}
                                 onChange={(e, i, item) => onSwitchAlg(item)}>
                    { algList.map((item, i) => (
                        <MenuItem key={i} value={item.payload} primaryText={item.text} />
                    )) }
                </DropDownDetails>

                <CreateTaskBtns {...this.props} onCreate={() => this.create()} isReadyCreate={isReadyCreate}
                                                onChangeScheduler={taskScheduler => store.dispatch(Actions.idsCollectTC.setParams({taskScheduler}))} />
            </div>
        )
    }
}
IdsCollectTask.propTypes = {
    onCreate: React.PropTypes.func.isRequired,
    idsListName: React.PropTypes.string.isRequired
};
