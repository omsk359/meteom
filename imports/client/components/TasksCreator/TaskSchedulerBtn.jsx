import React from 'react';
import * as _ from 'lodash';
import { TasksCreatorRefs } from "./TasksCreator";
import { CheckBoxDetails } from "../helpers";

export default class TaskSchedulerBtn extends React.Component {
    changed() {
        let taskScheduler = {...this.props.taskScheduler, enabled: this.refs.check.isChecked()};
        this.props.onChangeScheduler(taskScheduler);
    }
    render() {
        let { taskScheduler, onChangeScheduler, switchTo } = this.props;
        return (
            <CheckBoxDetails
                ref="check" label="Запланировать"
                defaultChecked={!!_.get(taskScheduler, 'enabled')} onCheck={() => this.changed()}
                onDetails={() => switchTo(TasksCreatorRefs.TaskScheduler, { taskScheduler, onChange: onChangeScheduler }) }
            />
        )
    }
}
TaskSchedulerBtn.propTypes = {
    taskScheduler: React.PropTypes.object,
    onChangeScheduler: React.PropTypes.func.isRequired,
    switchTo: React.PropTypes.func.isRequired,
    onChange: React.PropTypes.func
};
