import React from 'react';
import * as _ from 'lodash';
import { CheckBoxWithFields } from "../helpers";
import { NumberRange } from "../helpers";
import { RaisedButton, Checkbox } from 'material-ui';

export default class ClearAcc extends React.Component {
    save() {
        let params = _.mapValues(_.omit(this.props.params, 'delay'), (val, param) => this.refs[param].isChecked());
        let delay = {
            enable: this.refs.delayCheck.isChecked()
        };
        if (this.refs.delayRange) {
            let range = this.refs.delayRange.getValue();
            delay.range = [ range.from, range.to ];
        }
        this.props.onFinished({ ...params, delay });
    }
    render() {
        const { params } = this.props;
        const checks = [
            { param: 'avatars', text: 'Очистить аватарки' },
            { param: 'wall', text: 'Очистить стену' },
            { param: 'messages', text: 'Очистить сообщения' },
            { param: 'info', text: 'Очистить личную информацию' },
            { param: 'friends', text: 'Удалить друзей' },
            { param: 'friendRequests', text: 'Удалить заявки в друзья' },
            { param: 'leaveGroups', text: 'Выйти из групп' },
            { param: 'video', text: 'Удалить видеозаписи' },
            { param: 'photoAlbums', text: 'Удалить фотоальбомы' },
            { param: 'photo', text: 'Удалить все фото' },
            { param: 'music', text: 'Удалить музыку' },
            { param: 'groupInvites', text: 'Удалить приглашения в группы' }
        ];

        return (
            <div>
                {_.map(checks, check => (
                    <Checkbox
                        ref={check.param} key={check.param}
                        label={check.text} defaultChecked={params[check.param]}
                    />
                ))}
                <CheckBoxWithFields ref="delayCheck" defaultChecked={params.delay.enable} label="Задержка между действиями">
                    <NumberRange ref="delayRange" from={params.delay.range[0]} to={params.delay.range[1]} />
                </CheckBoxWithFields>

                <div className="right-btn">
                    <RaisedButton label="OK" primary={true} onClick={() => this.save()} />
                </div>
            </div>
        )
    }
}
ClearAcc.propTypes = {
    params: React.PropTypes.object.isRequired,
    onFinished: React.PropTypes.func.isRequired
};
