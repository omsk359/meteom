import React from 'react';
import * as _ from 'lodash';
import store from '../../redux/store';
import { createContainer } from 'meteor/react-meteor-data';
import { Actions } from "../../redux/actions";
import { omitUndef } from "../../../common/helpers";
import { TasksCreatorRefs } from "./TasksCreator";
import { CheckBoxWithFields } from "../helpers";
import { CreateTaskBtns } from "./TasksCreator";
import { getUserData, setUserData } from "../../helpers";
import { TextField } from 'material-ui';
import { ItemsSelect } from '../helpers';

export class BindPhoneTaskDumb extends React.Component {
    create() {
        let params = _.pick(this.props, ['taskScheduler', 'accs','useSimSms', 'useSmsVk', 'useSmsLike', 'useSmsArea']);
        _.assign(params, _.pick(this.props.settings, ['simSmsKey', 'smsVkKey', 'smsLikeKey', 'smsAreaKey']));
        this.props.onCreate(params);
    }
    getSettings() {
        let settings = {
            simSmsKey: _.result(this.refs.simSmsKey, 'getValue'),
            smsVkKey: _.result(this.refs.smsVkKey, 'getValue'),
            smsLikeKey: _.result(this.refs.smsLikeKey, 'getValue'),
            smsAreaKey: _.result(this.refs.smsAreaKey, 'getValue')
        };
        return omitUndef(_.defaults(settings, this.props.settings));
    }
    changed() {
        let settings = this.getSettings();
        let params = {
            useSimSms: _.result(this.refs.useSimSms, 'isChecked'),
            useSmsVk: _.result(this.refs.useSmsVk, 'isChecked'),
            useSmsLike: _.result(this.refs.useSmsLike, 'isChecked'),
            useSmsArea: _.result(this.refs.useSmsArea, 'isChecked')
        };
        this.props.onSave(settings);
        store.dispatch(Actions.bindPhoneTC.setParams(params));
    }
    render() {
        const { simSmsKey, smsVkKey, smsLikeKey, smsAreaKey } = this.props.settings;
        const { accs, accList, switchTo, useSimSms, useSmsVk, useSmsLike, useSmsArea } = this.props;

        let isReadyCreate = () => (useSimSms && _.size(simSmsKey) > 5 || useSmsVk && _.size(smsVkKey) > 5 ||
                                   useSmsLike && _.size(smsLikeKey) > 5 || useSmsArea && _.size(smsAreaKey) > 5)
                            && !!accs.length;
        return (
            <div>
                <ItemsSelect items={accs} itemName="аккаунтов" groupName={accList} onEdit={() => switchTo(TasksCreatorRefs.AccEdit)} />

                <CheckBoxWithFields ref="useSimSms" defaultChecked={useSimSms} label="simsms.org" onChange={() => this.changed()}>
                    <TextField ref="simSmsKey" defaultValue={simSmsKey} hintText="Ключ API" onChange={() => this.changed()} />
                </CheckBoxWithFields>
                <CheckBoxWithFields ref="useSmsVk" defaultChecked={useSmsVk} label="smsvk.net" onChange={() => this.changed()}>
                    <TextField ref="smsVkKey" defaultValue={smsVkKey} hintText="Ключ API" onChange={() => this.changed()} />
                </CheckBoxWithFields>
                <CheckBoxWithFields ref="useSmsLike" defaultChecked={useSmsLike} label="smslike.ru" onChange={() => this.changed()}>
                    <TextField ref="smsLikeKey" defaultValue={smsLikeKey} hintText="Ключ API" onChange={() => this.changed()} />
                </CheckBoxWithFields>
                <CheckBoxWithFields ref="useSmsArea" defaultChecked={useSmsArea} label="sms-area.org" onChange={() => this.changed()}>
                    <TextField ref="smsAreaKey" defaultValue={smsAreaKey} hintText="Ключ API" onChange={() => this.changed()} />
                </CheckBoxWithFields>

                <CreateTaskBtns {...this.props} setParamsActionCreator={Actions.bindPhoneTC.setParams} onCreate={() => this.create()} isReadyCreate={isReadyCreate} />
            </div>
        )
    }
}
BindPhoneTaskDumb.propTypes = {
    onCreate: React.PropTypes.func.isRequired,
    onSave: React.PropTypes.func.isRequired,
    settings: React.PropTypes.object.isRequired
};

const BindPhoneTask = createContainer(() => {
    const userDataHandle = Meteor.subscribe('userData');
    const loading = !userDataHandle.ready();
    return {
        loading,
        settings: getUserData('userSettings') || {},
        onSave: settings => setUserData('userSettings', settings)
    };
}, BindPhoneTaskDumb);

export default BindPhoneTask
