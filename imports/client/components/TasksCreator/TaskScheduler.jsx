import React from 'react';
import { RaisedButton, TextField } from 'material-ui';

export default class TaskScheduler extends React.Component {
    onFinished() {
        if (this.props.onFinished)
            this.props.onFinished(this.props.taskScheduler);
        //_.invokeMap(this.props, 'onFinished', this.props.taskScheduler);
    }
    changed() {
        let cron = this.refs.cron.getValue();
        if (this.props.onChange)
            this.props.onChange({ ...this.props.taskScheduler, cron });
    }
    render() {
        let { cron } = this.props.taskScheduler || {};
        //<RaisedButton label="ОК" primary={true} onClick={_.ary(this.props.onFinished, 0)} />
        return (
            <div>
                <TextField ref="cron" onChange={this.changed}
                           defaultValue={cron}
                           fullWidth={true} /><br /><br />


                <div className="right-btn">
                    <RaisedButton label="ОК" primary={true} onClick={() => this.onFinished()} />
                </div>
            </div>
        )
    }
}
TaskScheduler.propTypes = {
    taskScheduler: React.PropTypes.shape({
        enabled: React.PropTypes.bool,
        cron: React.PropTypes.string
    }),
    onFinished: React.PropTypes.func.isRequired,
    onChange: React.PropTypes.func
};
