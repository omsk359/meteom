import React from 'react';
import * as _ from 'lodash';
import store from '../../redux/store';
import { Actions } from "../../redux/actions";
import { TasksCreatorRefs } from "./TasksCreator";
import { CheckBoxWithFields } from "../helpers";
import { NumberRange } from "../helpers";
import { CheckBoxDetails } from "../helpers";
import { RaisedButton, TextField, DropDownMenu, Checkbox, MenuItem } from 'material-ui';
import { ItemsSelect } from '../helpers';

export default class RenameAccsTask extends React.Component {
    create() {
        let { accs } = this.props;
        let renameParams = ['changeName', 'firstNames', 'lastNames', 'shuffleNames',
            'changeAge', 'ageFrom', 'ageTo', 'changeBdateVisibility', 'bdateVisibility',
            'changeGender', 'gender', 'changeRelation', 'relation', 'changeHomeTown', 'homeTown',
            'changeStatus', 'status', 'clear', 'clearParams'
        ];
        let params = {
            accs, ..._.pick(this.props, renameParams)
        };
        this.props.onCreate(params);
    }
    changed() {
        let params = {};
        params.changeName = this.refs.changeName.isChecked();
        if (this.refs.shuffleNames)
            params.shuffleNames = this.refs.shuffleNames.isChecked();
        params.changeAge = this.refs.changeAge.isChecked();
        if (this.refs.ageRange) {
            let ageRange = this.refs.ageRange.getValue();
            params.ageFrom = ageRange.from;
            params.ageTo = ageRange.to;
        }
        if (this.refs.changeBdateVisibility)
            params.changeBdateVisibility = this.refs.changeBdateVisibility.isChecked();
        if (this.refs.changeGender)
            params.changeGender = this.refs.changeGender.isChecked();
        if (this.refs.changeRelation)
            params.changeRelation = this.refs.changeRelation.isChecked();
        if (this.refs.changeHomeTown)
            params.changeHomeTown = this.refs.changeHomeTown.isChecked();
        if (this.refs.homeTown)
            params.homeTown = this.refs.homeTown.getValue();
        if (this.refs.changeStatus)
            params.changeStatus = this.refs.changeStatus.isChecked();
        if (this.refs.status)
            params.status = this.refs.status.getValue();
        if (this.refs.clear)
            params.clear = this.refs.clear.isChecked();
        store.dispatch(Actions.renameAccsTC.setParams(params));
    }
    render() {
        const { accs, switchTo, accList, changeName, changeAge, changeBdateVisibility, changeGender, changeRelation, changeHomeTown, changeStatus,
            firstNames, lastNames, shuffleNames, ageFrom, ageTo, bdateVisibility, gender, relation, homeTown, status, clear } = this.props;
        let isReadyCreate = () => !!accs.length;
        if (changeName)
            var renameFields = (
                <div style={{marginLeft: '25px'}}>
                    <ItemsSelect items={firstNames} itemName="имен" onEdit={() => switchTo(TasksCreatorRefs.RenameAccsFirstNames)} />
                    <ItemsSelect items={lastNames} itemName="фамилий" onEdit={() => switchTo(TasksCreatorRefs.RenameAccsLastNames)} />
                    <Checkbox ref="shuffleNames" onCheck={() => this.changed()} defaultChecked={shuffleNames} label="Перемешать имена/фамилии" /><br />
                </div>
            );
        return (
            <div>
                <ItemsSelect items={accs} itemName="аккаунтов" groupName={accList} onEdit={() => switchTo(TasksCreatorRefs.AccEdit)} />
                <CheckBoxDetails
                    ref="clear" label="Очистка страницы"
                    defaultChecked={clear} onCheck={() => this.changed()}
                    onDetails={() => this.props.switchTo(TasksCreatorRefs.ClearAcc)}
                />

            <Checkbox ref="changeName" onCheck={() => this.changed()} defaultChecked={changeName} label="Переименовать" />
                {renameFields}
                <CheckBoxWithFields ref="changeAge" defaultChecked={changeAge} label="Менять возраст" onCheck={() => this.changed()}>
                    <NumberRange ref="ageRange" from={ageFrom} to={ageTo} onChange={() => this.changed()} />
                </CheckBoxWithFields>
                <CheckBoxWithFields ref="changeBdateVisibility" defaultChecked={changeBdateVisibility} label="Видимость даты рождения" onCheck={() => this.changed()}>
                    <DropDownMenu value={bdateVisibility} onChange={(e, i, item) => store.dispatch(Actions.renameAccsTC.setParams({bdateVisibility: item}))}>
                        <MenuItem value={1} primaryText='Показывать дату рождения' />
                        <MenuItem value={2} primaryText='Показывать только месяц и день' />
                        <MenuItem value={0} primaryText='Не показывать дату рождения' />
                    </DropDownMenu>
                </CheckBoxWithFields>
                <CheckBoxWithFields ref="changeGender" defaultChecked={changeGender} label="Менять пол" onCheck={() => this.changed()}>
                    <DropDownMenu value={gender} onChange={(e, i, item) => store.dispatch(Actions.renameAccsTC.setParams({gender: item}))}>
                        <MenuItem value={1} primaryText='Женский' />
                        <MenuItem value={2} primaryText='Мужской' />
                    </DropDownMenu>
                </CheckBoxWithFields>
                <CheckBoxWithFields ref="changeRelation" defaultChecked={changeRelation} label="Семейное положение" onCheck={() => this.changed()}>
                    <DropDownMenu value={relation} onChange={(e, i, item) => store.dispatch(Actions.renameAccsTC.setParams({relation: item}))}>
                        <MenuItem value={0} primaryText='Не указано' />
                        <MenuItem value={1} primaryText='Не женат' />
                        <MenuItem value={2} primaryText='Есть друг' />
                        <MenuItem value={3} primaryText='Помолвлен' />
                        <MenuItem value={4} primaryText='Женат' />
                        <MenuItem value={5} primaryText='Всё сложно' />
                        <MenuItem value={6} primaryText='В активном поиске' />
                        <MenuItem value={7} primaryText='Влюблён' />
                    </DropDownMenu>
                </CheckBoxWithFields>
                <CheckBoxWithFields ref="changeHomeTown" defaultChecked={changeHomeTown} label="Родной город" onCheck={() => this.changed()}>
                    <TextField ref="homeTown" defaultValue={homeTown} />
                </CheckBoxWithFields>
                <CheckBoxWithFields ref="changeStatus" defaultChecked={changeStatus} label="Статус" onCheck={() => this.changed()}>
                    <TextField ref="status" defaultValue={status} />
                </CheckBoxWithFields><br />

                <div className="right-btn">
                    <RaisedButton label="Готово" primary={true} onClick={() => this.create()} disabled={!isReadyCreate()} />
                </div>
            </div>
        )
    }
}
RenameAccsTask.propTypes = {
    onCreate: React.PropTypes.func.isRequired
};
