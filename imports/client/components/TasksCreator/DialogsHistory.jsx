import React from 'react';
import store from '../../redux/store';
import { TasksCreatorRefs } from "./TasksCreator";
import { Actions } from "../../redux/actions";
import { CreateTaskBtns } from "./TasksCreator";
import { ItemsSelect } from '../helpers';

export default class DialogsHistoryTaskCreator extends React.Component {
    create() {
        let { accs, taskScheduler } = this.props;
        let params = {
            accs, taskScheduler
        };
        this.props.onCreate(params);
    }
    render() {
        const { accs, switchTo, accList } = this.props;
        let isReadyCreate = () => !!accs.length;
        return (
            <div>
                <ItemsSelect items={accs} itemName="аккаунтов" groupName={accList} onEdit={() => switchTo(TasksCreatorRefs.AccEdit)} />

                <CreateTaskBtns isReadyCreate={isReadyCreate} {...this.props} onCreate={() => this.create()}
                                onChangeScheduler={taskScheduler => store.dispatch(Actions.dialogsHistoryTC.setParams({taskScheduler}))} />
            </div>
        )
    }
}
DialogsHistoryTaskCreator.propTypes = {
    onCreate: React.PropTypes.func.isRequired,
    taskScheduler: React.PropTypes.object.isRequired
};
