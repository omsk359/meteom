import React from 'react';
import * as _ from 'lodash';
import store from '../redux/store';
import { log } from "../../common/logger/Logger";
// import * as Streamy from 'meteor/yuukan:streamy';
import { Streamy } from 'meteor/yuukan:streamy';
import { Paper } from 'material-ui';

export default class LogArea extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (!nextProps.streamId || nextProps.streamId == this.props.streamId)
            return;
        this.refs.log.innerHTML = '';
        delete Streamy.handlers()[Streamy._applyPrefix(this.props.streamId)];
        delete Streamy.handlers()[Streamy._applyPrefix(this.props.streamId + '_loadAllFinished')];
        Streamy.on(nextProps.streamId, item => {
            //console.log('[{1}] Received: {2}'.format(nextProps.streamId, item.data));

            var el = document.createElement('span');
            //el.innerHTML = '[task {1}]: {2}'.format(nextProps.streamId, item.data);
            el.innerHTML = item.data;
            this.refs.log.appendChild(el);
            this.refs.log.appendChild($('<br />').get(0));
            //log.debug('scrollTOp: {1}  = {2}', this.refs.log.scrollTop, this.refs.log.scrollHeight);
            this.refs.log.scrollTop = this.refs.log.scrollHeight;

            let ell = this.refs.log;
            $(ell).find('span:last')[0].scrollIntoView();
            //$(ell).stop().animate({
            //    scrollTop: ell.scrollHeight
            //}, 8000);
        });
    }
    componentWillMount() {
        Meteor.disconnect();
        Meteor.reconnect();
    }
    componentWillUnmount() {
        delete Streamy.handlers()[Streamy._applyPrefix(this.props.streamId)];
        //delete Streamy.handlers()[Streamy._applyPrefix(this.props.streamId + '_loadAll')];
        delete Streamy.handlers()[Streamy._applyPrefix(this.props.streamId + '_loadAllFinished')];
    }
    render() {
        return (
            <Paper ref="log1" zDepth={4} style={{height: 300 + 'px', overflowX: 'hidden', padding: 8 + 'px'}}>
                <div className="log-area" ref="log">
                </div>
            </Paper>
        )
    }
}
LogArea.propTypes = {
    streamId: React.PropTypes.string
};
