import React from 'react';
import { splitStr } from "../../common/helpers";
import { RaisedButton, TextField, Checkbox } from 'material-ui';

export default class SearchUrlsEdit extends React.Component {
    save() {
        let lines = this.refs.text.getValue();
        lines = splitStr(lines);
        this.props.onFinished({
            urls: lines,
            resetLastParams: this.refs.resetLastParams.isChecked()
        });
    }
    render() {
        let { urls, resetLastParams } = this.props.searchUrlParams;
        let desc = this.props.desc;
        return (
            <div>
                <b>{this.props.title}</b>
                <Checkbox
                    ref="resetLastParams"
                    defaultChecked={!!resetLastParams}
                    label="Удалить последние параметры поиска для URL" /><br />
                <TextField
                    ref="text" multiLine={true}
                    rows={10} rowsMax={15} fullWidth={true}
                    defaultValue={(urls || []).join('\n')}
                    floatingLabelText={this.props.floatingText} />
                <span><p dangerouslySetInnerHTML={{__html: desc}}></p></span>

                <div className="right-btn">
                    <RaisedButton onClick={() => this.save()} label="OK" primary={true} />
                </div>
            </div>
        )
    }
}
SearchUrlsEdit.propTypes = {
    searchUrlParams: React.PropTypes.shape({
        urls: React.PropTypes.arrayOf(React.PropTypes.string),
        resetLastParams: React.PropTypes.bool,
    }),
    onFinished: React.PropTypes.func.isRequired,
    title: React.PropTypes.string,
    floatingText: React.PropTypes.string,
    desc: React.PropTypes.string
};
