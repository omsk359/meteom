import React from 'react';
import * as _ from 'lodash';
import store from '../redux/store';
import { Actions } from "../redux/actions";
import { splitStr } from "../../common/helpers";
import { RaisedButton, TextField, DropDownMenu, FlatButton,
         FontIcon, Dialog, Toolbar, ToolbarGroup, MenuItem
} from 'material-ui';

export default class IdListsManager extends React.Component {
    loadList(listName = this.props.selectedList || _.head(_.keys(this.props.lists))) {
        if (!listName) return;
        let ids = this.props.lists[listName];
        this.refs.idListsEdit.setText(ids.join('\n'));
    }
    saveList() {
        var s = this.refs.idListsEdit.getText();
        store.dispatch(Actions.userIdListManager.saveList({
            listName: this.props.selectedList,
            ids: splitStr(s).map(Number)
        }));
    }
    removeList() {
        store.dispatch(Actions.userIdListManager.removeList(this.props.selectedList));
        Meteor.defer(() => this.switchToFirst());
    }
    save() {
        return store.dispatch(Actions.userIdListManager.save(this.props.lists));
    }
    switchList(listName) {
        this.saveList();
        store.dispatch(Actions.userIdListManager.switchList(listName));
        this.loadList(listName);
    }
    addList(listName) {
        store.dispatch(Actions.userIdListManager.addList(listName));
        Meteor.defer(() => this.switchList(listName));
    }
    switchToFirst() {
        let listNames = _.keys(this.props.lists);
        if (listNames.length) {
            store.dispatch(Actions.userIdListManager.switchList(listNames[0]));
            this.loadList();
        } else
            this.refs.idListsEdit.setText('');
    }
    componentDidMount() {
        store.dispatch(Actions.userIdListManager.load()).then(lists => {
            this.loadList();
            //this.switchToFirst();
        });
    }
    finished() {
        let saveAll = () =>
            this.save().then(() => {
                let { lists, selectedList } = this.props;
                this.props.onFinished({
                    selectedList,
                    ids: lists[selectedList]
                });
            });

        this.saveList();
        Meteor.defer(saveAll);
    }
    render() {
        let listNames = _.keys(this.props.lists);
        return (
            <IdListsEdit
                ref="idListsEdit"
                listNames={listNames}
                selectedList={this.props.selectedList}
                onSwitchList={listName => this.switchList(listName)}
                onAddList={listName => this.addList(listName)}
                onRemoveList={() => this.removeList()}
                onFinished={() => this.finished()}
            />
        )
    }
}
IdListsManager.propTypes = {
    onFinished: React.PropTypes.func,
    //selectedList: React.PropTypes.string,
    lists: React.PropTypes.object.isRequired
};

export class IdListsEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = { listNameDialogHidden: true };
    }
    setText(text) {
        this.refs.idsEdit.getInputNode().value = text;
    }
    getText() {
        return this.refs.idsEdit.getValue();
    }
    addListBegin() {
        this.setState({ listNameDialogHidden: false });
        Meteor.defer(() => this.refs.newListName.focus());
    }
    addListEnd() {
        this.setState({ listNameDialogHidden: true });
        var listName = this.refs.newListName.getValue();
        if (listName == '') return;
        this.props.onAddList(listName);
    }
    render() {
        const { listNames, selectedList, onSwitchList, onRemoveList, onFinished } = this.props;
        let lists = _.map(listNames, listName => ({ payload: listName, text: listName }));

        // let listIndex = () => {
        //     let i = _.findIndex(lists, list => list.payload == selectedList);
        //     return _.max([0, i]);
        // };
        const checkEnterDown = e => {
            if (e.key == 'Enter')
                this.addListEnd(e);
        };
        return (
            <div className="acc-edit-box1">
                <Dialog open={!this.state.listNameDialogHidden} title="Новый список">
                    <TextField ref="newListName" hintText="Название списка..." onKeyDown={checkEnterDown} />
                    <br /><br />
                    <i>Чтобы создать/выбрать список используя дату/время момента добавления в него ID, включите в название подстроку с воскл. знаками: <b>&nbsp;!ФОРМАТ!&nbsp;</b>,
                        где ФОРМАТ - строка в которой символы D, M, Y и т.д. заменяются, соответственно, на число, месяц, год.<br />
                        Например: <b>Инвайтинг (!DD.MM.YY!)</b> &nbsp; -> &nbsp; <b>Инвайтинг (03.03.16)</b><br />
                        При этом, ID также будут добавляться и в шаблонный список тоже.<br />
                        <a target="_blank" href="http://momentjs.com/docs/#/displaying/format/">Полное описание ФОРМАТА</a>
                    </i>
                </Dialog>
                <Toolbar>
                    <ToolbarGroup key={0} float="left">
                        <DropDownMenu value={selectedList} onChange={(e, i, item) => onSwitchList(item)}>
                            { lists.map((item, i) => (
                                <MenuItem key={i} value={item.payload} primaryText={item.text} />
                            )) }
                        </DropDownMenu>
                    </ToolbarGroup>
                    <ToolbarGroup key={1} float="right">
                        <FlatButton primary={true} label="Удалить" labelPosition="after" onClick={onRemoveList} disabled={!lists.length}>
                            <FontIcon className="material-icons">delete</FontIcon>
                        </FlatButton>
                        <FlatButton primary={true} label="Добавить" labelPosition="after" onClick={() => this.addListBegin()}>
                            <FontIcon className="material-icons">note_add</FontIcon>
                        </FlatButton>
                    </ToolbarGroup>
                </Toolbar>

                <TextField
                    ref="idsEdit" multiLine={true} rows={10} rowsMax={15} fullWidth={true}
                    disabled={!lists.length} floatingLabelText="Редактирование списка ID" />
                <div className="right-btn">
                    <RaisedButton onClick={onFinished} label="OK" disabled={!lists.length} primary={true} />
                </div>
            </div>
        )
    }
}
IdListsEdit.propTypes = {
    listNames: React.PropTypes.arrayOf(React.PropTypes.string),
    selectedList: React.PropTypes.string,
    onSwitchList: React.PropTypes.func,
    onAddList: React.PropTypes.func,
    onRemoveList: React.PropTypes.func,
    onFinished: React.PropTypes.func
};
