import React from 'react';
import * as _ from 'lodash';
import store from '../redux/store';
import { log } from "../../common/logger/Logger";
import { connect, Provider } from 'react-redux';
import DialogsHistory_DB from '../../common/collections/DialogsHistory_DB';
import { Actions } from "../redux/actions";
import { Badge, TextField, DropDownMenu, MenuItem, List, ListItem, Snackbar } from 'material-ui';

// Override Meteor._debug to filter for custom msgs
Meteor._debug = (function (super_meteor_debug) {
    return function (error, info) {
        if (!(info && _.has(info, 'msg')))
            super_meteor_debug(error, info);
    }
})(Meteor._debug);


function unreadDialogsAcc(acc) {
    let dialogs = DialogsHistory_DB.find({ userIdAcc: acc.userId }).fetch();
    //dialogs = _.filter(dialogs, d => !d.out && d.read_state);
    //dialogs = _.filter(dialogs, d => _.filter(d.history, msg => msg.id > d.in_read && !msg.out).length > 0);
    dialogs = _.filter(dialogs, d => unreadDialogCnt(d) > 0);
    return dialogs;
}

function unreadDialogCnt(dialog) {
    return _.filter(dialog.history, msg => msg.id > dialog.in_read && !msg.out).length;
}

function fixUnread(dialog) {
    _.each(dialog.history, msg => msg.read_state = msg.id <= dialog.in_read || msg.out);
    return dialog;
    //return { ...dialog, history: _.each(dialog.history, msg => msg.read_state = msg.id <= dialog.in_read || msg.out) }
}

export class Messenger extends React.Component {
    hideMsg() {
        store.dispatch(Actions.hideMsg());
    }
    hideErr() {
        store.dispatch(Actions.hideErr());
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.infoMsg !== nextProps.infoMsg)
            if (nextProps.infoMsg)
                this.refs.infoBar.show();
            else
                this.refs.infoBar.dismiss();
        if (this.props.errMsg !== nextProps.errMsg) {
            if (nextProps.errMsg)
                this.refs.errBar.show();
            else if (this.props.errMsg)
                this.refs.errBar.dismiss();
        }

        if (this.props.tasks !== nextProps.tasks) {
            this.setTask();
            this.selectAcc();
            this.selectDialog();
        }
    }

    setTask(taskId = this.props.taskId) {
        store.dispatch(Actions.messenger.setTaskId(taskId));
        let task = _.find(this.props.tasks, task => task._id == taskId);
        if (!task) return;

        const dialogAccs = task.data.state.params.accs;
        store.dispatch(Actions.accManager.accsByIds(dialogAccs)).then(action => {
            let accs = action.payload;
            store.dispatch(Actions.messenger.setAccs(accs));
            log.debug('MESSENGER accs: {1}', accs);

            //let userIds = _.map(accs, 'userId');
            //let dialogs = DialogsHistory_DB.find({userIdAcc: { $in: userIds }}).fetch();
            //store.dispatch(Actions.messenger.setHistory(dialogs));
        });
    }
    selectAcc(acc = this.props.acc) {
        store.dispatch(Actions.messenger.setAcc(acc));
        let dialogs = DialogsHistory_DB.find({ userIdAcc: acc.userId }).fetch();
        store.dispatch(Actions.messenger.setDialogs(dialogs));

        //this.sendMsg(acc, '1234', 'ssssssssss');
    }
    selectDialog(userId = this.props.userId) {
        store.dispatch(Actions.messenger.setUserId(userId));
        let dialog = _.find(this.props.dialogs, dialog => dialog.userIdDialog == userId);
        if (!dialog) return;
        dialog = fixUnread(dialog);
        log.debug('selectDialog: {1}', dialog);
        store.dispatch(Actions.messenger.setHistory(_.get(dialog, 'history')));
    }

    sendMsg(text) {
        let { acc, userId } = this.props;
        let { taskId } = this.props;
        store.dispatch(Actions.dialogsHistoryTC.callAccMethod(taskId, acc.login, 'messages.send', [userId, text])).then(result => {
            if (result.error)
                return store.dispatch(Actions.showErr(result.payload));
            if (_.isNumber(result.payload.response)) {
                store.dispatch(Actions.dialogsHistoryTC.taskRestart(taskId, acc.login));
                return store.dispatch(Actions.showMsg('Сообщение успешно отправлено'));
            }
            store.dispatch(Actions.showErr(result.payload));
        });
    }

    render() {
        const { tasks } = this.props;
        const dialogTasks = _.filter(tasks, task => task.taskName == 'DialogsHistoryTask');
        log.debug('MESSENGER dialogTasks: {1}', dialogTasks);
        log.debug('MESSENGER PROPS: {1}', this.props);

        var taskMenuItems = _.map(dialogTasks, task => { return { payload: task._id, text: task._id } });

        //log.debug('MESSENGER dialogs', this.props.dialogs);
        if (!dialogTasks.length)
            return (
                <div>
                    Сначала нужно добавить задачу Сбор диалогов
                </div>
            );
        if (this.props.taskId)
            var messengerBox = (
                <div className="messenger-box">
                    <div className="messenger-left-select">
                        <MessengerAccList accs={this.props.accs} onChange={acc => this.selectAcc(acc)} />
                        <MessengerDialogs dialogs={this.props.dialogs} onChange={d => this.selectDialog(d)} />
                    </div>
                    <div className="message-right-box">
                        <MessengerHistory history={this.props.history} />
                        <MessengerSendArea onSend={(...args) => this.sendMsg(...args)} />
                    </div>
                </div>
            );

        return (
            <div className="messenger">

                Задача:
                <DropDownMenu value={this.props.taskId} onChange={(e, i, item) => this.setTask(item)}>
                    { taskMenuItems.map((item, i) => (
                        <MenuItem key={i} value={item.payload} primaryText={item.text} />
                    )) }
                </DropDownMenu>

                {messengerBox}

                <Snackbar
                    ref="infoBar"
                    message={this.props.infoMsg}
                    action="hide"
                    autoHideDuration={3000}
                    onDismiss={() => this.hideMsg()}
                    onActionTouchTap={() => this.hideMsg()} />
                <Snackbar
                    ref="errBar"
                    message={this.props.errMsg}
                    action="hide"
                    onDismiss={() => this.hideErr()}
                    onActionTouchTap={() => this.hideErr()} />
            </div>
        )
    }
}

export class MessengerAccList extends React.Component {
    render() {
        //let accs = [ {login: '12345'}, {login: 'eeeeee'}, {login: '88888888'} ];
        return (
            <div className="messenger-acc-list">
                <List subheader="Аккаунты">
                    { _.map(this.props.accs, acc => {
                        let unreadCnt = unreadDialogsAcc(acc).length;
                        if (unreadCnt)
                            var rightIcon = <Badge badgeContent={unreadCnt} primary={true} />;
                        return (
                        <ListItem
                            key={acc.login}
                            primaryText={acc.login}
                            onClick={() => this.props.onChange(acc)}
                            rightIcon={rightIcon} />
                            );
                        })}
                </List>
            </div>
        )
    }
}
MessengerAccList.propTypes = {
    accs: React.PropTypes.array.isRequired,
    onChange: React.PropTypes.func.isRequired
};

export class MessengerDialogs extends React.Component {
    render() {
        //let accs = [ {login: '12345'}, {login: 'eeeeee'}, {login: '88888888'} ];
        return (
            <div className="messenger-dialogs-list">
                <List subheader="Диалоги">
                    { _.map(this.props.dialogs, dialog => {
                        let cnt = unreadDialogCnt(dialog);
                        if (cnt)
                            var rightIcon = <Badge badgeContent={cnt} primary={true} />;
                        return (
                            <ListItem
                                primaryText={dialog.userIdDialog}
                                onClick={() => this.props.onChange(dialog.userIdDialog)}
                                rightIcon={rightIcon} />
                            );
                        })}
                </List>
            </div>
        )
    }
}
MessengerDialogs.propTypes = {
    dialogs: React.PropTypes.array.isRequired,
    onChange: React.PropTypes.func.isRequired
};

export class MessengerHistory extends React.Component {
    componentDidUpdate() {
        this.refs.history.scrollTop = this.refs.history.scrollHeight;
    }
    render() {
        let { history } = this.props;
        //log.debug('history: {1}', history);
        return (
            <div ref="history" className="messenger-history">
                <List subheader="История сообщений">
                    { _.map(history, msg =>
                        <MessengerHistoryItem message={msg} />
                    )}
                </List>
            </div>
        )
    }
}
MessengerHistory.propTypes = {
    history: React.PropTypes.array.isRequired
};

export class MessengerHistoryItem extends React.Component {
    render() {
        let msg = this.props.message;
        //log.debug('message: {1}', msg);
        let classes = ['messenger-history-item'];
        classes.push(msg.out ? 'message-out' : 'message-in');
        classes.push(msg.read_state ? 'message-read' : 'message-unread');
        return (
            <div className={classes.join(' ')}>
                <ListItem
                    subheader={'id' + msg.user_id}
                    primaryText={msg.body} />
            </div>
        )
    }
}
MessengerHistoryItem.propTypes = {
    message: React.PropTypes.object.isRequired
};

export class MessengerSendArea extends React.Component {
    send(e) {
        let text = this.refs.text.getValue();
        if (e.ctrlKey) {
            this.refs.text.getInputNode().value = text + '\n';
            return;
        }
        e.preventDefault();
        this.props.onSend(text);
        this.refs.text.getInputNode().value = '';
        this.refs.text.blur();
    }
    render() {
        const checkEnterDown = e => {
            if (e.key == 'Enter')
                this.send(e);
        };
        return (
            <div className="messenger-send-area">
                <TextField
                    ref="text"
                    floatingLabelText="Текст сообщения"
                    rows={2}
                    fullWidth={true}
                    multiLine={true} onKeyDown={checkEnterDown} />
            </div>
        )
    }
}
MessengerSendArea.propTypes = {
    onSend: React.PropTypes.func.isRequired
};


//Meteor.subscribe('tasks');
//Meteor.startup(function() { // work around files not being defined yet
    // trigger action when this changes
    //log.debug('trackCollection ------- TRACK TaskData');
//});

// AppContainer is responsible for fetching data from the store and
// listening for changes. In a larger app you would have a container
// for each major component.

export default class MessengerContainer extends React.Component {
    componentWillMount() {
        store.dispatch(Actions.tasks.update());
        Meteor.subscribe('Dialogs');
    }
    render() {
        return (
            <Provider store={store}>
                <MessengerConnected />
            </Provider>
        );
    }
}

const MessengerConnected = connect(state => ({
    ...state.messenger, ...state.common, ...state.tasks, accManager: state.accManager
}))(Messenger);
