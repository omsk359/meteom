import React from 'react';
import moment from 'moment';
import { RaisedButton, DatePicker, TimePicker } from 'material-ui';

export default class DateTimePicker extends React.Component {
    save() {
        let date = this.refs.date.getDate();
        let time = this.refs.time.getTime();
        this.props.onFinished(time);
    }
    dateChanged(e, date) {
        let time = moment(this.refs.time.getTime());
        date = moment(date);
        time.date(date.date()).month(date.month()).year(date.year());
        this.refs.time.setTime(time.toDate());
    }
    render() {
        let isReady = () => true;
        let { date, title, dateHint, timeHint } = this.props;
        //onShow={() => $(React.findDOMNode(this.refs.date)).css('margin-top', '500px')}
        return (
            <div>
                <b>{title}</b>
                <DatePicker
                    ref="date"
                    defaultDate={date}
                    hintText={dateHint}
                    onChange={(...args) => this.dateChanged(...args)}
                    container="dialog" />
                <TimePicker
                    ref="time"
                    format="24hr"
                    defaultTime={date}
                    hintText={timeHint} />
                <RaisedButton label="OK" primary={true} onClick={() => this.save()} disabled={!isReady()} />
            </div>
        )
    }
}
DateTimePicker.propTypes = {
    date: React.PropTypes.instanceOf(Date),
    title: React.PropTypes.string,
    dateHint: React.PropTypes.string,
    timeHint: React.PropTypes.string,
    onFinished: React.PropTypes.func.isRequired
};
