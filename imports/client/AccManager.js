import UserAccs from '../common/collections/UserAccs';
import { splitStr } from "../common/helpers";
import AccInfo from "../common/AccInfo";
import { getUserData } from "./helpers";

Meteor.subscribe('Accs');
Meteor.subscribe('userData');

export default class AccManager {
    static addOldAccInfo(acc) {
        let userData = acc._id && UserAccs.findOne(acc._id) ||
                    acc.userId && UserAccs.findOne({userId: acc.userId}) ||
                    acc.login && UserAccs.findOne({login: acc.login});
        acc.deserialize(userData, true);
    }
    static parseFromStr(str, type) {
        str = _.trim(str);
        var arr = splitStr(str);
        return _.map(arr, accStr => new AccInfo(accStr, type));
    }
    static accsByIds(ids) {
        return _.transform(ids, (accs, _id) => {
            let accData = UserAccs.findOne(_id);
            if (accData)
                accs.push(new AccInfo().deserialize(accData));
        });
    }
    static getAllProxies() {
        let accs = UserAccs.find({}).fetch();
        return _.map(accs, acc => new Proxy().deserialize(acc.proxy));
    }
    static getRuCaptchaKey() {
        return getUserData('userSettings.ruCaptchaKey');
    }
    //constructor() {
    //    trackCollection(UserData, data => {
    //        this.updateLists();
    //    });
    //    trackCollection(UserAccs, data => {
    //        this.updateLists();
    //    });
    //    this.updateLists();
    //}
    //updateLists() {
    //    let listsDB = getUserData('accLists') || {};
    //    this.lists = _.transform(listsDB, (lists, list, listName) => {
    //        lists[listName] = AccManager.accsByIds(list);
    //    }, {});
    //    log.debug('updateLists(): {1}', this.lists);
    //}
    //getListNames() {
    //    return _.keys(this.lists);
    //}
    //getList(listName) {
    //    if (!this.lists[listName]) {
    //        this.lists[listName] = [];
    //        this.save();
    //    }
    //    return this.lists[listName];
    //}
    //save() {
    //    let listData = _.transform(this.lists, (listsDB, list, listName) => {
    //        listsDB[listName] = _.map(list, '_id');
    //    });
    //    log.debug('save accs: {1}', listData);
    //    setUserData('accLists', listData);
    //}
    //getListStr(listName) {
    //    return _.map(this.getList(listName), acc => acc.toString());
    //}
    //saveListStr(listName, str) {
    //    let accs = AccManager.parseFromStr(str);
    //    return this.saveList(listName, accs);
    //}
    //existInOtherLists(_id) {
    //    return _.some(this.lists, list => _.some(list, acc => acc._id == _id));
    //}
    // return: { LOGIN1: { opType: 'REMOVE', state: Error }, LOGIN2: { opType: 'CREATE', state: 'ID_1234' } }
    //saveList(listName, accs) {
    //    log.debug(`saveList(${listName})`);
    //    if (!listName) return Promise.resolve();
    //    _.each(accs, acc => AccManager.addOldAccInfo(acc));
    //    let list = this.getList(listName);
    //    log.debug(`[${listName}] {1}; all: {2}`, list, this.lists);
    //    let accsIsEq = (acc1, acc2) => acc1.userId == acc2.userId || acc1.login == acc2.login;
    //
    //    let self = this;
    //    return Meteor.runAsync(function* () {
    //        let removeAccs = _.filter(accs, acc => !_.some(list, oldAcc => accsIsEq(acc, oldAcc)));
    //        removeAccs = _.filter(removeAccs, acc => !self.existInOtherLists(acc._id));
    //        let removeIds = _.map(removeAccs, '_id');
    //        var numberAffected = yield Meteor.callPromise('removeAccs', removeIds);
    //        log.debug(`removed: ${numberAffected} accs`);
    //
    //        let toUpdate = _.map(accs, acc => acc.serialize());
    //        let results = yield Meteor.callPromise('updateAccs', toUpdate);
    //        log.debug(`Update results: {1}`, results);
    //        let successAccs = _.filter(toUpdate, acc => results[acc.login].state == 'OK');
    //        _.each(successAccs, acc => {
    //            if (results[acc.login].opType == AccOpTypes.ADD)
    //                acc._id = results[acc.login]._id;
    //        });
    //        self.lists[listName] = _.map(successAccs, accData => new AccInfo().deserialize(accData));
    //        self.save();
    //    });
    //}
    //removeList(listName) {
    //    delete this.lists[listName];
    //    this.save();
    //}
}

