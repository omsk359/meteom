import { log } from '../common/logger/Logger';
import UserData from '../common/collections/UserData';

export function setUserData(path, value) {
    var set_obj = {
        ['data.' + path]: value
    };
    log.debug('setUserData: [{1}] = {2}', ' data.' + path, value);
    UserData.update(UserData.findOne()._id, { $set: set_obj });
}

export function updateUserData(path, value) {
    // var data = _.get(UserData.findOne(), 'data.' + path);
    var set_obj = {
        ['data.' + path]: value
    };
    log.debug('setUserData: [{1}] = {2}', ' data.' + path, value);
    UserData.update(UserData.findOne()._id, { $set: set_obj });
}

export function getUserData(path) {
    if (!path)
        return UserData.findOne().data;
    var data = _.get(UserData.findOne(), 'data.' + path);
    return data;
}