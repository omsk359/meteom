import * as lodash from 'lodash';
_ = lodash;

import './logger';
import './methods';
import './publish';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

console.log('Server started');
