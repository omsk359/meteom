import fs from 'fs';

if (!fs.existsSync(Meteor.settings.logDir))
    fs.mkdirSync(Meteor.settings.logDir);

if (!fs.existsSync(Meteor.settings.tasksLogDir))
    fs.mkdirSync(Meteor.settings.tasksLogDir);
