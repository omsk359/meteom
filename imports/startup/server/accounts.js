import { Accounts } from 'meteor/std:accounts-basic';

// https://github.com/studiointeract/accounts-material

Accounts.config({
    sendVerificationEmail: false,
    forbidClientAccountCreation: false
});
