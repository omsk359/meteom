import TaskManager from "../../server/TaskManager";
import AccManager from "../../server/AccManager";
import { wrapMeteorError } from "../../server/helpers";

Meteor.methods({
    onlineTaskList: function(listName) {
        var mngr = TaskManager.instance();
        var uid = Meteor.userId();
        mngr.log.debug('user: {1}', uid);
        var tasks = mngr.tasksByUser(uid);
        _.each(tasks, task => mngr.stop(task._id));
        mngr.log.debug('all stopped');

        var accMngr = new AccManager(uid);
        var accs = accMngr.get(listName);
        mngr.log.debug('AccManager accs: {1}', accs);

        //var taskId = mngr.create('DialogsHistoryTask', uid, accs);
        var taskId = mngr.create('DialogsHistoryTask', { accs: accs }, uid);
        mngr.start(taskId);
    },
    createTask: function(taskName, params) {
        return wrapMeteorError(() => {
            var mngr = TaskManager.instance();
            var taskId = mngr.create(taskName, params, Meteor.userId()/* || 'uvBQZcGa2zX66aqXD'*/); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //mngr.start(taskId);
            //log.debug('task ID -> {1}', taskId);
            return taskId;
        });

    },
    stopTask: function(taskId) {
        wrapMeteorError(() => {
            var mngr = TaskManager.instance();
            mngr.stop(taskId, Meteor.userId());
        });
    },
    removeTask: function(taskId) {
        wrapMeteorError(() => {
            var mngr = TaskManager.instance();
            mngr.remove(taskId, Meteor.userId());
        });
    },
    pauseTask: function(taskId) {
        wrapMeteorError(() => {
            var mngr = TaskManager.instance();
            mngr.pause(taskId, Meteor.userId());
        });
    },
    resumeTask: function(taskId) {
        wrapMeteorError(() => {
            var mngr = TaskManager.instance();
            mngr.resume(taskId, Meteor.userId());
        });
    },
    startTask: function(taskId) {
        wrapMeteorError(() => {
            var mngr = TaskManager.instance();
            mngr.start(taskId);
        });
    }
});
