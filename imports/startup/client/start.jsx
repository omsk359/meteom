import { render } from 'react-dom';
import React from 'react';
import { Routes } from '../../client/components/App';
import { Provider } from 'react-redux';
import store from '../../client/redux/store';
import { log } from "../../common/logger/Logger";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import LightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';

import './accounts';
import '/imports/client/css/styles.css'

// revert commit: https://github.com/callemall/material-ui/pull/3513
const { palette } = LightBaseTheme;
const myTheme = {
    flatButton: {
        primaryTextColor: palette.accent1Color,
        secondaryTextColor: palette.primary1Color
    },
    floatingActionButton: {
        color: palette.accent1Color,
        secondaryColor: palette.primary1Color
    },
    raisedButton: {
        primaryColor: palette.accent1Color,
        secondaryColor: palette.primary1Color
    }
};

Meteor.startup(function() {
    injectTapEventPlugin();
    render((
        <MuiThemeProvider muiTheme={getMuiTheme(myTheme)}>
            <Provider store={store}>
                <Routes />
            </Provider>
        </MuiThemeProvider>
    ), document.getElementById("container"));
});
