import { Accounts } from 'meteor/std:accounts-basic';
import { browserHistory } from 'react-router';
import { T9n } from 'meteor/softwarerero:accounts-t9n';

T9n.setLanguage('ru');

// https://github.com/studiointeract/accounts-material

Accounts.ui.config({
    passwordSignupFields: 'EMAIL_ONLY',
    homeRoutePath: '/',
    profilePath: '/profile',
    onSignedInHook: () => browserHistory.replace('/tasks'),
    onPostSignUpHook: () => browserHistory.replace('/tasks'),
    minimumPasswordLength: 6
});
