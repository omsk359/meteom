import DialogsHistoryTask from "./DialogsScrapeTask";
import { DialogsHistoryTaskSingle } from "./DialogsScrapeTask";
import IdsIterateTask from "./IdsIterateTask";
import { IdsIterateTaskSingle } from "./IdsIterateTask";
import IdsCollectTask from "./lib/IdsCollectTask";
import { IdsCollectTaskSingle } from "./lib/IdsCollectTask";
import RenameAccsTask from "./RenameAccsTask";
import { RenameAccsTaskSingle } from "./RenameAccsTask";
import UnfreezeAccsTask from "./UnfreezeAccsTask";
import { UnfreezeAccsTaskSingle } from "./UnfreezeAccsTask";
import RegAccsTask from "./RegAccsTask";
import { RegAccsTaskSingle } from "./RegAccsTask";
import BindPhoneTask from "./BindPhoneTask";
import { BindPhoneTaskSingle } from "./BindPhoneTask";
import LikeOutRequestsTask from "./LikeOutRequestsTask";
import { LikeOutRequestsTaskSingle } from "./LikeOutRequestsTask";

export default function taskClassByName(taskName) {  // !! don't forget add new tasks to the allTasksTypes
    switch (taskName) {
        //case 'TestTask':        return TestTask;
        case 'DialogsHistoryTask'           : return DialogsHistoryTask;
        case 'DialogsHistoryTaskSingle'     : return DialogsHistoryTaskSingle;
        case 'IdsIterateTask'               : return IdsIterateTask;
        case 'IdsIterateTaskSingle'         : return IdsIterateTaskSingle;
        case 'IdsCollectTask'               : return IdsCollectTask;
        case 'IdsCollectTaskSingle'         : return IdsCollectTaskSingle;
        case 'RenameAccsTask'               : return RenameAccsTask;
        case 'RenameAccsTaskSingle'         : return RenameAccsTaskSingle;
        case 'UnfreezeAccsTask'             : return UnfreezeAccsTask;
        case 'UnfreezeAccsTaskSingle'       : return UnfreezeAccsTaskSingle;
        case 'RegAccsTask'                  : return RegAccsTask;
        case 'RegAccsTaskSingle'            : return RegAccsTaskSingle;
        case 'BindPhoneTask'                : return BindPhoneTask;
        case 'BindPhoneTaskSingle'          : return BindPhoneTaskSingle;

        case 'LikeOutRequestsTask'          : return LikeOutRequestsTask;
        case 'LikeOutRequestsTaskSingle'    : return LikeOutRequestsTaskSingle;
        // case 'SenderTask'          : return SenderTask;
        // case 'SenderTaskSingle'    : return SenderTaskSingle;
    }
}
