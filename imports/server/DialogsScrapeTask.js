import { MultiAccSubTask } from "./lib/MultiAcc";
import MultiAccTask from "./lib/MultiAcc";
export class DialogsHistoryTaskSingle extends MultiAccSubTask {
    constructor(parent, title) {
        super(parent.ownerId, 'DialogsHistoryTaskSingle', parent, title);
    }

    autoResponse(rules, dialog) {
        rules = [{ re: /прив/gi,  inCnt: [0, 1], outCnt: [0, 1], text: '42W' }];
        let hist = _.transform(dialog.history, (hist, msg, i) => {
            if (i && msg.out == _.last(hist))
                _.last(hist).body += '\n' + msg.body;
            else
                hist.push(msg);
        });
        if (_.last(hist).out)
            return;

        let inRange = (n, range) => {
            if (!(range instanceof Array) || range.length != 2)
                return true;
            return range[0] <= n && n <= range[1];
        };

        let cnts = _.countBy(hist, 'out'), inCnt = _.get(cnts, '[0].length') || 0, outCnt = _.get(cnts, '[1].length') || 0;
        let inMsg = _.last(hist).body;
        let rule = _.find(rules, rule => {
            return inRange(inCnt, rule.inCnt) && inRange(outCnt, rule.outCnt) && inMsg.test(rule.re);
        });
        return rule ? rule.text : null;
    }

    checkDialogNeedResponse(dialogDB) {
        //if (dialogDB.needResponse)
    }

    start() {
        super.start();
        this.wrapErrHandlers(() => {
            let { acc } = this;
            this.info('Получение диалогов...');
            let dialogs = acc.messages().getDialogs();
            this.info('Всего диалогов {1}/{2}', dialogs.items.length, dialogs.count);
            let userIdsDialogs = dialogs.items.map(dialog => dialog.message.user_id);
            let dialogsInDB = DialogsHistory_DB.find({ userIdAcc: acc.userId, userIdDialog: { $in: userIdsDialogs }, ownerId: this.ownerId }).fetch();
            let dialogsInDbMap = _.transform(dialogsInDB, (map, d) => map[d.userIdDialog] = d, {});
            let userIdsDialogsNeedToUpdate = dialogs.items.filter(d => {
                let dialogDB = dialogsInDbMap[d.message.user_id];
                return !(dialogDB && dialogDB.lastMsgId == d.message.id && d.in_read == dialogDB.in_read && d.out_read == dialogDB.out_read);
            });
            this.info('Нужно обновить {1} диалогов', userIdsDialogsNeedToUpdate.length);
            for (let i = 0; i < userIdsDialogsNeedToUpdate.length; i++) {
                let userId = userIdsDialogsNeedToUpdate[i].message.user_id;
                let { in_read, out_read } = userIdsDialogsNeedToUpdate[i];
                let dialogDB = dialogsInDB.filter(d => d.userIdDialog == userId);
                let histSize = _.get(dialogDB, '[0].histSize') || 0;
                let newHist = acc.messages().getHistory(userId, histSize, 1);
                let messages = newHist.items;
                this.debug('histSize: {1}; newHist.length: {2}', histSize, messages.length);
                DialogsHistory_DB.update(_.get(dialogDB, '[0]._id'), {
                    $addToSet: { history: { $each: messages } },
                    $set: {
                        lastMsgId: _.get(_.last(messages), 'id') || _.get(dialogDB, '[0].lastMsgId'),
                        histSize: histSize + messages.length,
                        ownerId: this.ownerId,
                        userIdAcc: acc.userId,
                        userIdDialog: userId,
                        in_read: in_read,
                        out_read: out_read
                    }
                }, { upsert: true });
                this.info('Получено новых {1} сообщений c пользователем id{2}', messages.length, userId);
            }
        });
    }
}

export default class DialogsHistoryTask extends MultiAccTask {
    constructor(ownerId, params) {
        super(ownerId, 'DialogsHistoryTask', params, DialogsHistoryTaskSingle);
    }

    startNow() {
        try {
            this.tasks.forEach(task => task.params = this.state.params);
            super.startNow();

            let responseClient = (client, response) =>
                Streamy.emit('DialogsHistoryTask_{1}'.format(this._id), response, client);

            let self = this;
            Streamy.on('DialogsHistoryTask_{1}'.format(self._id), (data, from) => {
                try {
                    if (data.taskRestart) {
                        let login = data.taskRestart;
                        let i = _.findIndex(self.tasks, task => task.acc.login);
                        return this.restartTaskNow(i);
                    }
                    let acc = _.find(self.accs, acc => acc.login == data.login);
                    if (!acc) return responseClient(from, {error: 'Wrong acc!'});
                    let method = data.method.split('.');
                    if (method.length != 2) return responseClient(from, {error: 'Wrong method!'});
                    let response = acc[method[0]]()[method[1]](...data.params);
                    log.debug('RECEIVED SERVER sendMsg: {1}', data);
                    responseClient(from, {response});
                } catch (e) {
                    responseClient(from, {error: e.message});
                }
            });

            this.waitChildrenAndPrint();
        } catch (e) {
            this.error('>>>>>>>>>>>>>>>> TASK EXCEPTION: {1}', e.stack);
        }
    }
}