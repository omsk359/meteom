import SomeError from "../../common/SomeError";
import { ERR_TYPE } from "../../common/SomeError";
import { _getStr } from "../../common/helpers";
import * as request from 'request';
import * as _ from 'lodash';
import Future from 'fibers/future';
// import { HTTP } from 'meteor/http';
import { HTTP } from 'meteor/dandv:http-more';

const netErrCntMax = 6, unknownErrCntMax = 5;

    export default class HttpHelpers {
    constructor(log, userAgent, cookieJar, proxy) {
        this.log = log;// || new Logger();
        this.userAgent = userAgent || 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0';
        this.cookieJar = cookieJar || request.jar();
        this.proxy = proxy;
        this.netErrCnt = 0;
        this.netErrCntMax = 6;
        this.unknownErrCnt = 0;
        this.timeout = 20 * 1000;
        this.headers = { 'User-Agent': this.userAgent };
        //this.headers = { 'User-Agent': this.userAgent, 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'Accept-Encoding': 'gzip, deflate, br', 'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3' };
    }

    getMatch(url, ctrlStr, tryCnt = 2, followRedirect = true) {
        return this.get(url, followRedirect, ctrlStr, tryCnt);
    }

    get(url, followRedirect = true, ctrlStr = '', tryCnt = 1) {
        if (followRedirect === undefined)
            followRedirect = true;
        var opts = { jar: this.cookieJar, headers: this.headers, followAllRedirects: followRedirect, timeout: this.timeout };
        if (this.proxy && !this.proxy.isEmpty())
            opts.proxy = this.proxy.toHttpString();
        //else
        //    throw new SomeError(ERR_TYPE.NO_PROXY);
        return this.requestMatch('get', url, opts, ctrlStr, tryCnt);
    }

    postMatch(url, ctrlStr, params, tryCnt = 2, followRedirect = true) {
        return this.post(url, params, followRedirect, ctrlStr, tryCnt);
    }

    post(url, params = {}, followRedirect = true, ctrlStr = '', tryCnt = 1) {
        var opts = { jar: this.cookieJar, headers: this.headers, followAllRedirects: followRedirect, form: params, timeout: this.timeout };
        if (this.proxy && !this.proxy.isEmpty())
            opts.proxy = this.proxy.toHttpString();
        //else
        //    throw new SomeError(ERR_TYPE.NO_PROXY);
        return this.requestMatch('post', url, opts, ctrlStr, tryCnt);
    }

    request(method, url, opts, ctrlStr = '', tryCnt = 1) {
        try {
            //this.log.debug('[HTTP] url: {1}', url);
            //this.log.debug('[HTTP opts]: {1}', _.omit(opts, ['jar', 'followAllRedirects']));
            var response = HTTP[method](url, opts);
            //this.log.debug('[HTTP] response: {1}', response);
            this.netErrCnt = this.unknownErrCnt = 0;
            return response;
        } catch (e) {
            let msg = _getStr(e, 'message');
            this.log.debug('REQUEST ERR: {1}', msg);
            if (msg.contains('tunneling socket could not be established') || msg.contains('ETIMEDOUT') ||
                msg.contains('socket hang up') || msg.contains('Bad Gateway') ||
                msg.contains('ESOCKETTIMEDOUT') || msg.contains('ECONNRESET')
            ) {
                //_getStr(e, 'message').contains('socket hang up')/* || _getStr(e, 'message').contains('ESOCKETTIMEDOUT')*/) {
                if (++this.netErrCnt > netErrCntMax) {
                    this.netErrCnt = 0;
                    throw new SomeError(ERR_TYPE.PROXY, this.proxy.check());
                }
                if (_.has(this.proxy, 'ip')) {
                    this.log.info('Ошибка прокси {1}. Повторная попытка.', this.proxy.ip);
                }
                return this.request(method, url, opts);
            }
            if (msg.contains('Internal Server Error')) {
                if (++this.unknownErrCnt > unknownErrCntMax) {
                    this.unknownErrCnt = 0;
                    throw new SomeError(ERR_TYPE.UNKNOWN, 'Internal Server Error', e);
                }
                return this.request(method, url, opts);
            }
            //if (_getStr(e, 'message').contains('ETIMEDOUT')) {
            //    if (++this.netErrCnt > this.netErrCntMax) {
            //        this.netErrCnt = 0;
            //        throw new SomeError(ERR_TYPE.PROXY, this.proxy.check());
            //    }
            //    this.log.info('Ошибка прокси {1}. Повторная попытка.', this.proxy.ip);
            //    return this.request(method, url, opts);
            //}
            throw e;
        }
    }

    requestMatch(method, url, opts, ctrlStr, tryCnt = 2) {
        let response = this.request(method, url, opts);
        // this.log.debug('requestMatch: {1}', response.content);
        if (typeof response.content == 'string' && response.content.contains(ctrlStr))
            return response;
        if (tryCnt <= 1)
            throw new SomeError(ERR_TYPE.HTTP, 'No found "{1}" for {2} with params {3}'.format(ctrlStr, url, _.get(opts, 'form') || opts));
        return this.requestMatch(method, url, opts, ctrlStr, tryCnt - 1);
    }


    ruCaptchaUploadTry(url, ruCaptchaKey, tryCnt = 1) {
        for (let i = 0; i < tryCnt; i++)
            try {
                let response = this.ruCaptchaUpload(url, ruCaptchaKey);
                if (!response.contains('|'))
                    throw new SomeError(ERR_TYPE.CAPTCHA, response);
                return response.split('|')[1];
            } catch (e) {
                //this.log.error('Ошибка загрузки каптчи: {1}', e.message);
                if (!(e instanceof SomeError) && i + 1 < tryCnt)
                    ;//this.log.info('Еще попытка...');
                else
                    throw new SomeError(ERR_TYPE.CAPTCHA, 'Upload error: ' + e.message, e);
            }
    }

    ruCaptchaUpload(url, ruCaptchaKey) {
        var opts = { jar: this.cookieJar, headers: this.headers, timeout: this.timeout };
        if (this.proxy && !this.proxy.isEmpty())
            opts.proxy = this.proxy.toHttpString();
        let inStream = request.get(url, opts);

        let future = new Future();
        request.post({ url: 'http://rucaptcha.com/in.php', timeout: this.timeout, formData: {
            key: ruCaptchaKey,
            file: inStream
        }}, (err, httpResponse, body) => {
            this.log.debug('ruCaptchaUpload cb');
            if (err)
                return future.throw(err);
            future.return(body || '!!EMPTY');
        });
        Meteor.setTimeout(() => {
            this.log.debug('ruCaptchaUpload Meteor.setTimeout');
            future.isResolved() || future.return('!!EMPTY');
        }, this.timeout * 1.5);
        future.wait();
        return future.get();
    }

    ruCaptchaWait(ruCaptchaId, ruCaptchaKey, delay = 2000, maxTryCnt = 20) {
        for (let i = 0; i < maxTryCnt; i++) {
            let result = this.get('http://rucaptcha.com/res.php?key={1}&action=get&id={2}'.format(ruCaptchaKey, ruCaptchaId));
            //this.log.debug('ruCaptchaWait: {1}', result);
            if (result.content.contains('OK|'))
                return result.content.slice(3);
            if (result.content === 'CAPCHA_NOT_READY')
                Meteor._sleepForMs(delay);
            else
                throw new SomeError(ERR_TYPE.CAPTCHA, result);
        }
    }
}
