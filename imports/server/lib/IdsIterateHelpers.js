import { TaskActionTypes } from "../../common/types";
import { TaskFilterTypes } from "../../common/types";
import { IdsSourceTypes } from "../../common/types";
import HttpHelpers from "./HttpHelpers";
import SomeError from "../../common/SomeError";
import { ERR_TYPE } from "../../common/SomeError";
import { IdsSearchByUrl } from "./IdsCollectTask";
import { listNamesDateFormat } from "../../common/helpers";
import * as SenderTask from "../../common/helpers";
import { getSynonyName } from "../../common/helpers";
import { variateText } from "../../common/helpers";
import { IdsQueueMultiFilter } from "./IdsQueue";
import { IdsProviderNextFunc } from "./IdsQueue";
import AccManager from "../AccManager";
import { splitMsgsDalay } from "../../common/helpers";
import { log } from "../../common/logger/Logger";
import UserIdLists_DB from '../../common/collections/UserIdLists_DB';

//Invite_DB = new Mongo.Collection('Invite');
//Invite_DB = new Mongo.Collection('InviteRealDB2');
var oldInviteDBHost = 'http://om2-author.rhcloud.com';
var http = new HttpHelpers();

const { INVITE, SEND_MSG, DELETE_FRIEND, ADD_TO_USER_ID_LISTS,
        REMOVE_FROM_USER_ID_LISTS, ADD_TO_OLD_INVITE_DB, NOPE, DELAY
} = TaskActionTypes;
const { ONLINE, LAST_SEEN, NOT_EXIST_OUT_MSG, USER_ID_LISTS, NOT_IN_OLD_INVITE_DB } = TaskFilterTypes;
const { IDS_COMMON_LIST, IDS_COPY_LIST, SEARCH_URLS } = IdsSourceTypes;

export const SearchUrls_DB = new Mongo.Collection('SearchUrlsDB');

export const ApplyIdsQueue = {
    [IDS_COMMON_LIST]: task => {
        let p = task.state.params;
        let listName = p.userIdListName;
        const maxCheckCnt = p.maxCheckCnt || 1000, delayMinutes = 2;
        let { listInfLoop } = p;
        let idsProvider = new IdsProviderNextFunc(idsProvider => {
            let i = _.get(task.state, IDS_COMMON_LIST + '.i') || 0;
            let listDB = UserIdLists_DB.findOne({ ownerId: task.ownerId, listName }, {
                fields: {
                    ids: { $slice: [ i, maxCheckCnt ] }
                }
            });
            if (!listDB)
                throw new SomeError(ERR_TYPE.TASK_WRONG_PARAM, `Список "${listName}" не найден!`);
            if (listDB.ids.length) {
                _.set(task.state, IDS_COMMON_LIST + '.i', i + maxCheckCnt);
                return listDB.ids;
            }
            if (!listInfLoop)
                return null;
            task.setState({ [IDS_COMMON_LIST]: { i: 0 } });
            if (!i) {
                task.info(`Список "${listName}" пуст. Пауза ${delayMinutes} минут.`);
                task.delay(delayMinutes * 60, true);
            }
            return [];
        });
        let idsQueue = new IdsQueueMultiFilter(idsProvider, maxCheckCnt, p.filters, task.log, listInfLoop ? 4 : 7, false, false);
        _.each(task.tasks, subTask => subTask.idsQueue = idsQueue);
    },
    [IDS_COPY_LIST]: task => {
        let p = task.state.params;
        let listName = p.userIdListName;
        const maxCheckCnt = p.maxCheckCnt || 1000, delayMinutes = 2;
        let { listInfLoop } = p;
        _.each(task.tasks, subTask => {
            let idsProvider = new IdsProviderNextFunc(idsProvider => {
                let i = _.get(subTask.state, IDS_COMMON_LIST + '.i') || 0;
                let listDB = UserIdLists_DB.findOne({ ownerId: task.ownerId, listName }, {
                    fields: {
                        ids: { $slice: [ i, maxCheckCnt ] }
                    }
                });
                if (!listDB)
                    throw new SomeError(ERR_TYPE.TASK_WRONG_PARAM, `Список ${listName} не найден!`);
                if (listDB.ids.length) {
                    _.set(subTask.state, IDS_COMMON_LIST + '.i', i + maxCheckCnt);
                    return listDB.ids;
                }
                if (!listInfLoop)
                    return null;
                subTask.setState({ [IDS_COMMON_LIST]: { i: 0 } });
                if (!i) {
                    subTask.info(`Список "${listName}" пуст. Пауза ${delayMinutes} минут.`);
                    subTask.delay(delayMinutes * 60, true);
                }
                return [];
            });
            subTask.idsQueue = new IdsQueueMultiFilter(idsProvider, maxCheckCnt, p.filters, subTask.log, 4, true, false);
        });
    },
    [SEARCH_URLS]: task => {
        let p = task.state.params;
        let log = task.log;

        var mngr = new AccManager(task.ownerId);
        let accs = mngr.getAccs(p.idsSearchParams.accs);
        _.each(accs, acc => acc.log = acc.http.log = log);

        if (p.idsSearchParams.resetLastParams)
            SearchUrls_DB.remove({ownerId: task.ownerId, url: {$in: p.idsSearchParams.urls}});

        _.defaults(p.idsSearchParams, { usedUrls: [] });
        let { urls, usedUrls } = p.idsSearchParams;
        if (typeof urls == 'string')
            urls = _.compact(urls.split(/\s+/));
        //var fields = 'country,bdate,sex,online,online_mobile,can_send_friend_request,last_seen,photo_200_orig,universities';
        var fields = '';

        let idsProvider = new IdsProviderNextFunc(idsProvider => {

            let { idsSearchState } = task.state;
            if (!idsSearchState) {
                let idsSearchStates = SearchUrls_DB.find({ ownerId: task.ownerId, url: {$in: urls} }).fetch();
                let idsSearchStatesToRemove = _.filter(idsSearchStates, st => !_.size(st.lastParams.criteriaOrder)); // remove finished urls
                _.each(idsSearchStatesToRemove, idsSearchState => {
                    SearchUrls_DB.remove({ _id: idsSearchState._id });
                    usedUrls.push(idsSearchState.url);
                });
                idsSearchState = _.find(idsSearchStates, st => _.size(st.lastParams.criteriaOrder));

                if (!idsSearchState) {
                    urls = _.filter(urls, url => !_.includes(usedUrls, url));
                    if (!urls.length) return null;
                    idsSearchState = {};
                    let initParams = IdsSearchByUrl.url2SearchParams(idsSearchState.url = urls[0]);
                    idsSearchState.lastParams = {
                        criteriaOrder: [],
                        lastParams: initParams,
                        fixedFields: _.keys(_.pick(initParams, ['sex', 'online', 'birth_month', 'birth_day', 'country', 'status', 'city']))
                    };
                }
                task.state.idsSearchState = idsSearchState;
            }
            let { criteriaOrder, lastParams, fixedFields } = idsSearchState.lastParams;
            let response = IdsSearchByUrl.search(accs, lastParams, criteriaOrder, fields, fixedFields, log);
            log.debug('SEARCH_URLS RESULT: {1}/{2}', response.items.length, response.count);
            log.debug('SEARCH_URLS NEXT criteriaOrder: {1}; params: {2}', criteriaOrder, lastParams);

            // TODO: Throttle
            SearchUrls_DB.update({ _id: _.get(idsSearchState, '_id') }, {
                ownerId: task.ownerId,
                url: idsSearchState.url,
                lastParams: idsSearchState.lastParams
            }, { upsert: true });

            return _.map(response.items, 'id');
        });
        let idsQueue = new IdsQueueMultiFilter(idsProvider, p.maxCheckCnt || 1000, p.filters, task.log);
        _.each(task.tasks, subTask => subTask.idsQueue = idsQueue);
    },
    [IdsSourceTypes.FRIENDS]: task => {
        let p = task.state.params, silentLvl = task.periodicTask ? 4 : 10;
        _.each(task.tasks, subTask => subTask.idsQueue = new IdsQueueMultiFilter(() => {
            let result = subTask.acc.friends().get();
            return result.items;
        }, 1000, p.filters, subTask.log, silentLvl, true));
    },
    [IdsSourceTypes.FRIEND_REQUESTS_IN]: task => {
        let p = task.state.params, silentLvl = task.periodicTask ? 4 : 10;
        _.each(task.tasks, subTask => subTask.idsQueue = new IdsQueueMultiFilter(() => {
            let result = subTask.acc.friends().getRequests(false);
            return result.items;
        }, 1000, p.filters, subTask.log, silentLvl, true));
    },
    [IdsSourceTypes.FRIEND_REQUESTS_OUT]: task => {
        let p = task.state.params, silentLvl = task.periodicTask ? 4 : 10;
        _.each(task.tasks, subTask => subTask.idsQueue = new IdsQueueMultiFilter(() => {
            let result = subTask.acc.friends().getRequests();
            return result.items;
        }, 1000, p.filters, subTask.log, silentLvl, true));
    }
};

export const IdsFilters = {
    [ONLINE]: (ids, subTask) => {
        let usersInfo = subTask.idsQueue.getInfos(ids);
        let filtered = _.filter(ids, id => _.get(usersInfo[id], 'online'));
        return filtered;
    },
    [LAST_SEEN]: (ids, subTask) => {
        let startDate = +new Date(subTask.params[LAST_SEEN].date);
        let usersInfo = subTask.idsQueue.getInfos(ids);
        let filtered = _.filter(ids, id => _.has(usersInfo, `[${id}].last_seen.time`) && usersInfo[id].last_seen.time * 1000 >= startDate);
        return filtered;
    },
    [NOT_EXIST_OUT_MSG]: (ids, subTask) => {
    },
    [TaskFilterTypes.FRIENDS]: (ids, subTask) => {
        let friends = subTask.acc.friends().get();
        return _.intersection(ids, friends.items);
    },
    [NOT_IN_OLD_INVITE_DB]: (ids, subTask) => {
        http.log = subTask.log;
        do {
            var result = http.post(oldInviteDBHost + '/api/vk/db/id/check', { ids: ids.join('\n') });
            result = JSON.parse(_.get(result, 'content'));
        } while (result && result.tooMuchRequests);
        ids = _.initial(result.results.split('\n'));
        return ids;
    },
    [USER_ID_LISTS]: (ids, subTask) => {
        //ids = _.map(ids, id => id + '');
        let listNames = subTask.params[USER_ID_LISTS];
        listNames = listNamesDateFormat(listNames);
        let dbData = UserIdLists_DB.aggregate([{
            $match: { ownerId: subTask.ownerId, listName: { $in: listNames } }
        }, {
            $project: { _id: 1, ids: { $setDifference: [ids, '$ids'] } }
        }, {
            $unwind: '$ids'
        }, {
            $group: {_id: 1, ids: {$addToSet: '$ids'}}
        }]);
        let filtered = _.get(dbData, '[0].ids');
        return filtered;
    }
    //[NOT_IN_INVITE_DB]: (ids, subTask) => {
    //    //ids = _.map(ids, id => id + '');
    //    let dbData = Invite_DB.find({ownerId: subTask.ownerId, userId: {$in: ids}}).fetch();
    //    let dbIds = dbData.map(item => item.userId);
    //    return _.difference(ids, dbIds);
    //}
};

export const Actions = {
    addToUserIdLists: function(listNames, userIds, ownerId, log = log, silentIfOk = true) {
        let ok = true;
        _.each(listNames, listName => {
            let result = Actions.addToUserIdList(listName, userIds, ownerId);
            ok &= result == 1;
            if (!silentIfOk || !ok)
                log.debug('Добавление в список "{2}": {1}', result == 1 ? 'OK' : result, listName);
        });
        return ok;
    },
    addToUserIdList: function(listName, userIds, ownerId) {
        userIds = userIds.map(Number);
        return UserIdLists_DB.update({ ownerId, listName }, { $addToSet: { ids: { $each: userIds } } }, { upsert: true });
    },
    removeFromUserIdLists: function(listNames, userIds, ownerId) {
        userIds = userIds.map(Number);
        return UserIdLists_DB.update({ ownerId, listName: { $in: listNames } }, { $pull: { ids: { $in: userIds } } }, { multi: true });
    }
};


export const IdActions = {
    [INVITE]: (subTask, userId, idsOk_i) => {
        let params = subTask.params[INVITE] || {}, { msg, msgEnable } = params;
        let { log, acc, idsQueue } = subTask;
        if (msgEnable && msg) {
            var newMsg = SenderTask.variateText(msg);
            if (newMsg.contains('%username%')) {
                let userName = idsQueue.getInfo(userId).first_name;
                let synonyName = getSynonyName(userName);
                if (synonyName != userName)
                    log.info('Замена "{1}" -> "{2}"', userName, synonyName);
                newMsg = newMsg.replace(/%username%/gi, synonyName);
            }
            log.info('id{1} -> {2}', userId, newMsg);
        }
        try {
            var result = acc.friends().add(userId, newMsg);
        } catch (e) {
            if (e.name != ERR_TYPE.VK_API) throw e;
            if (e.message.contains('Cannot add this user to friends as they have put you on their blacklist'))
                result = 175;
            else
                throw e;
        }
        let ok = result == 1 || result == 2, status;
        switch (result) {
            case 1:     status = 'Отправлена!'; break;
            case 2:     status = 'Одобрена!'; break;
            case 175:   status = 'В ЧЕРНОМ СПИСКЕ ПОЛЬЗОВАТЕЛЯ!'; break;
            default:    status = 'ОШИБКА ({1})'.format(result);
        }
        let resultMsg = 'Заявка в друзья id{1}: {2}'.format(userId, status);
        return { ok, resultMsg };
    },
    [SEND_MSG]: (subTask, userId, idsOk_i) => {
        let params = subTask.params[SEND_MSG] || {}, { msgs } = params;
        let subTask_i = subTask.parent.tasks.indexOf(subTask), msg_i = subTask_i % msgs.length;
        let { log, acc, idsQueue, state } = subTask;
        var newMsg = variateText(msgs[msg_i]);
        if (newMsg.contains('%username%')) {
            let userName = idsQueue.getInfo(userId);
            userName = _.get(userName, 'first_name');
            if (userName === undefined)
                throw new SomeError(ERR_TYPE.TASK_LOGIC, `Ошибка при получении имени id${userId}`);
            let synonyName = getSynonyName(userName);
            if (synonyName != userName)
                log.info('Замена "{1}" -> "{2}"', userName, synonyName);
            newMsg = newMsg.replace(/%username%/gi, synonyName);
        }
        log.info('id{1} -> {2}', userId, newMsg);
        let msgsDelay = splitMsgsDalay(newMsg);
        try {
            var ok = false;
            for (let i = _.get(state, SEND_MSG + '.i') || 0; i < msgsDelay.length; i++) {
                let delay = _.random.apply(_, msgsDelay[i].delay);
                if (delay)
                    subTask.delay(delay);
                try {
                    var result = acc.messages().send(userId, msgsDelay[i].text);
                } catch (e) {
                    if (e.name != ERR_TYPE.VK_API) throw e;
                    let errMsg = result => log.info('ОШИБКА ОТПРАВКИ СООБЩЕНИЯ id{1}: {2}', userId, result);
                    if (e.message.contains('Permission to perform this action is denied')) {
                        errMsg('Доступ запрещен');
                        break;
                    }
                    if (e.message.contains('Flood control: same message already sent'))
                        errMsg('Повторная отправка того же сообщения');
                    continue;
                }
                _.set(subTask.state, SEND_MSG + '.i', i + 1);
                ok = _.isNumber(result);
                log.info('Отправка сообщения{3} id{1}: {2}', userId, ok ? 'OK' : 'ОШИБКА - {1}'.format(result), msgsDelay.length > 1 ? `#${i + 1}` : '');
            }
            return {ok};
        } finally {
            _.set(subTask.state, SEND_MSG + '.i', 0);
        }

        //
        //log.info('id{1} -> {2}', userId, newMsg);
        //try {
        //    var result = acc.messages().send(userId, newMsg);
        //} catch (e) {
        //    if (e.name != ERR_TYPE.VK_API) throw e;
        //    if (e.message.contains('Permission to perform this action is denied'))
        //        result = 'ДОСТУП ЗАПРЕЩЕН';
        //    else
        //        throw e;
        //}
        //let ok = _.isNumber(result);
        //let resultMsg = 'Отправка сообщения id{1}: {2}'.format(userId, ok ? 'OK' : 'ОШИБКА - {1}'.format(result));
        //return { ok, resultMsg };
    },

    [DELETE_FRIEND]: (subTask, userId) => {
        try {
            var result = subTask.acc.friends().delete(userId);
        } catch (e) {
            throw e;
        }
        let ok = result.success, status;
        if (result.friend_deleted)
            status = 'Друг id{1} удален.';
        else if (result.out_request_deleted)
            status = 'Отменена исходящая заявка id{1}';
        else if (result.in_request_deleted)
            status = 'Отменена входящая заявка id{1}';
        else if (result.suggestion_deleted)
            status = 'Отменена рекомендация друга id{1}';
        if (ok)
            var resultMsg = status.format(userId);
        else
            resultMsg = 'ОШИБКА: {1}'.format(result);
        return { ok, resultMsg };
    },

    [ADD_TO_USER_ID_LISTS]: (subTask, userId) => {
        let { ownerId, log } = subTask, { state } = subTask.parent;
        let listNames = subTask.params[ADD_TO_USER_ID_LISTS] || {};
        //listNames = listNames[subTask.currActionType];
        listNames = listNames[subTask.ok ? 'onOK' : 'onFail'];
        if (!_.size(listNames))
            return { ok: false };
        listNames = listNamesDateFormat(listNames);
        //state.okIds.push(userId);
        let ok = Actions.addToUserIdLists(listNames, [userId], ownerId, log);
        return { ok }
    },
    [REMOVE_FROM_USER_ID_LISTS]: (subTask, userId) => {
        let { ownerId, log } = subTask, { state } = subTask.parent;
        let listNames = subTask.params[REMOVE_FROM_USER_ID_LISTS] || {};
        //listNames = listNames[subTask.currActionType];
        listNames = listNames[subTask.ok ? 'onOK' : 'onFail'];
        //state.okIds.push(userId);
        if (!_.size(listNames))
            return { ok: false };
        listNames = listNamesDateFormat(listNames);
        let ok = Actions.removeFromUserIdLists(listNames, [userId], ownerId, log);
        log.debug('Удаление из списоков "{2}": {1}', _.isNumber(ok) ? 'OK' : result, listNames.join(', '));
        return { ok }
    },
    //[ADD_TO_INVITE_DB]: (subTask, userId) => {
    //    let acc = subTask.acc, state = subTask.parent.state, ownerId = subTask.ownerId;
    //    state.okIds.push(userId);
    //    let dbId = Invite_DB.insert({ ownerId: ownerId, userId: +userId, userIdAcc: +subTask.acc.getUserId() });
    //    subTask.debug('Add to DB: {1}', dbId);
    //    return { ok: !!dbId, resultMsg: 'Add to DB: {1}'.format(dbId) }
    //},
    [ADD_TO_OLD_INVITE_DB]: (subTask, userId) => {
        http.log = subTask.log;
        do {
            var result = http.post(oldInviteDBHost + '/api/vk/db/id/push', { ids: userId });
            result = JSON.parse(_.get(result, 'content'));
        } while (result && result.tooMuchRequests);
        let ok = result && result.ok;
        if (ok)
            var msg = result.submitted ? 'Добавлен' : 'Уже есть в базе!';
        else
            msg = 'ОШИБКА: {1}'.format(result);
        return { ok, resultMsg: 'Отправляем id{1} в базу RHCloud: {2}'.format(userId, msg) }
    },
    [NOPE]: (subTask, userId) => ({
        ok: true,
        resultMsg: 'User - id{1}'.format(userId)
    }),
    [DELAY]: (subTask, userId) => {
        let maxCnt = _.max(_.map(_.pick(subTask.state.idsOkCnts, subTask.currActionTypes), okCnt => okCnt.ok)); // sorry
        subTask.delay(subTask.parent.getDelay(maxCnt));
        return { ok: true }
    }
};

// !!!!!!!!!!!!!!!!!!!!
Meteor.startup(() => {
    TestDB = new Mongo.Collection('TestDB');

    let FETCH = 0, INSERT = 0, UPSERT = 1;
    if (FETCH) {
        let listNames = [ 'list1', 'list2' ], ids = [2,11,8,4,100]; // 11, 8, 100
        //let dbData = TestDB.aggregate([{
        //    $match: {ownerId: 2}
        //}, {
        //    $project: {_id: 1, ids: 1, listName: 1}
        //}, {
        //    $unwind: "$ids"
        //}]);
        let dbData = TestDB.aggregate([{
            $match: {ownerId: 2, listName: {$in: listNames}}
        }, {
            $project: {_id: 1, ids: {$setDifference: [ids, '$ids']}}
        }, {
            $unwind: '$ids'
        }, {
            $group: {_id: 1, ids: {$addToSet: '$ids'}}
        }]);
        let filtered = _.get(dbData, '[0].ids');
    }
    if (INSERT && TestDB.find({}).count() == 0) {
        TestDB.insert({ ownerId: 2, listName: 'list1', ids: [1,2,3] });
        TestDB.insert({ ownerId: 2, listName: 'list2', ids: [3,4,8,5] });
        TestDB.insert({ ownerId: 3, listName: 'list1', ids: [99,100] });
    }
    if (UPSERT && TestDB.find({}).count() == 4) {
        //_.each()
        //TestDB.update({ ownerId: 2, listName: {$in: ['list7', 'list10']} }, { $addToSet: {ids: 34} }, {multi: true, upsert: true});
    }
});
