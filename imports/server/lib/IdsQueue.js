import VkAcc from "../VkApi";
import SomeError from "../../common/SomeError";
import { ERR_TYPE } from "../../common/SomeError";
import HttpHelpers from "./HttpHelpers";
import ResumableTask from "./ResumableTask";
import Logger from "../../common/logger/Logger";
import { IdsFilters } from "./IdsIterateHelpers";
import Future from 'fibers/future';

export class IdsQueue extends ResumableTask {
    constructor(idsReady, idsToCheck, checker, parentTask) {
        super(parentTask.ownerId, 'IdsQueue', parentTask, 'IdsQueue');
        this.idsReady = idsReady || [];
        this.idsToCheck = idsToCheck || [];
        this.checker = checker;
        this.log = parentTask.log || new Logger();
        this.next_i = 0;
        //if (checker)
        //    this.checkerLoop();
        this.waitChangesFuture = null;
    }
    getNextId() { // return ID or null
        if (this.next_i < this.idsReady.length)
            return this.idsReady[this.next_i++];
        if (!this.idsToCheck.length)
            return null;
        this.waitChangesFuture = new Future();
        this.log.info('Ждем проверки ID (в очереди {1} шт.)...', this.idsToCheck.length);
        this.waitChangesFuture.wait();
        if (this.waitChangesFuture.get() === null)
            return null;
        //this.waitChangesFuture = null;
        return this.idsReady[this.next_i++];
    }
    start() {
        Future.task(() => {
            super.start();
            while (this.idsToCheck.length) {
                let id = this.idsToCheck[0], ok = this.checker(id);
                this.idsToCheck.shift();
                this.log.info('Проверка id{1} -> {2}', id, ok);
                if (ok) {
                    this.idsReady.push(id);
                    if (this.waitChangesFuture)
                        this.waitChangesFuture.return(id);
                }
            }
            if (this.waitChangesFuture)
                this.waitChangesFuture.return(null);
        });
    }
}

export class IdsQueueWithInfo extends IdsQueue {
    constructor(idsReady, idsToCheck, checker, logger, infoFields) {
        super(idsReady, idsToCheck, checker, logger);
        this.cachedInfo = {};
        this.http = new HttpHelpers();
        this.http.log = logger;
        this.fields = infoFields;
    }
    getInfo(id) {
        if (this.cachedInfo[id])
            return this.cachedInfo[id];
        const MAX_CNT = 200;
        var userIds = _.union([id], _.filter(this.idsReady, id => this.cachedInfo[id]), _.filter(this.idsToCheck, id => this.cachedInfo[id]));
        userIds = userIds.slice(0, MAX_CNT);
        this.log.info('Обновляем кэш информации пользователей (по запросу id{1})', id);
        var result = VkAcc.apiNoAuth('users.get', { user_ids: userIds, fields: this.fields }, null, this.http);
        _.each(result, user => this.cachedInfo[user.id] = user);
        return this.cachedInfo[id];
    }
}


//IdsProvider = class IdsProvider {
//    constructor(ids, getNextIdsFunc) {
//        //this.ids = _.map(ids, Number);
//        this.ids = ids || [];
//        this.getNextIdsFunc = getNextIdsFunc;
//        this._finished = false;
//    }
//    setGetNextIdsFunc(func) {
//        this.getNextIdsFunc = func;
//    }
//    hasGetNextIdsFunc() {
//        return !!this.getNextIdsFunc;
//    }
//    nextIds() { // return NULL at the end
//        let ids = this.ids || [];
//        this.ids = [];
//        if (this.isFinished())
//            return null;
//        if (!ids.length && this.getNextIdsFunc)
//            ids = this.getNextIdsFunc(this);
//        else if (!ids.length)
//            ids = null;
//        if (!ids)
//            this.finished();
//        return ids;
//    }
//    finished()   { this._finished = true; }
//    isFinished() { return !!this._finished; }
//}

export class IdsProvider {
    constructor() {
        this._finished = false;
        this._isCommon = true;
    }
    nextIds() { return [] }
    finished()   { this._finished = true; }
    isFinished() { return this._finished; }
    setResetable(resetable = false) {
        this._isResetable = resetable;
    }
    reset() {
        if (this._isResetable)
            this._finished = false;
    }
}
export class IdsProviderIds extends IdsProvider {
    constructor(ids) {
        super();
        this.ids = ids;
    }
    nextIds() {
        if (this.isFinished()) return null;
        //let ids = this.ids;
        //this.ids = [];
        this.finished();
        return this.ids;
        //return ids;
    }
}
export class IdsProviderDeferFunc extends IdsProvider {
    constructor(func) {
        super();
        this.func = func;
    }
    nextIds() {
        if (this.isFinished()) return null;
        this.finished();
        return this.func();
    }
}
export class IdsProviderNextFunc extends IdsProvider {
    constructor(getNextIdsFunc) {
        super();
        this.getNextIdsFunc = getNextIdsFunc;
    }
    nextIds() {
        if (this.isFinished()) return null;
        let ids = this.getNextIdsFunc(this);
        if (!ids)
            this.finished();
        return ids;
    }
}



export class IdsQueueMultiFilter {
    constructor(idsProviderOrIdsOrFunc, maxCheckCnt, filterNames, logger, silentLvl = 10, resetable = false, backgroundUpdates = true) { // filter: ids => filteredIds
        if (idsProviderOrIdsOrFunc instanceof Array)
            idsProviderOrIdsOrFunc = new IdsProviderIds(idsProviderOrIdsOrFunc);
            //idsProviderOrIdsOrFunc = new IdsProvider(idsProviderOrIdsOrFunc);
        if (typeof idsProviderOrIdsOrFunc == 'function') {
            //let deferFunc = idsProviderOrIdsOrFunc;
            //idsProviderOrIdsOrFunc = new IdsProvider([], idsProviderOrIdsOrFunc);
            idsProviderOrIdsOrFunc = new IdsProviderDeferFunc(idsProviderOrIdsOrFunc);
        }
        this.idsProvider = idsProviderOrIdsOrFunc;
        this.idsProvider.setResetable(resetable);
        this.idsToCheck = [];
        this.idsReady = [];
        this.filters = {};
        this.maxCheckCnt = maxCheckCnt;
        this.next_i = 0;
        this.minIdsLimit = backgroundUpdates ? 100 : 0; // 0 - to disable background updates
        this.fields = 'country,sex,online,online_mobile,can_send_friend_request,last_seen';
        this.silentLvl = silentLvl; // 0..10  (0 = mute)
        this.delaySec = 8;

        _.each(filterNames, filterName => this.addFilter(filterName));

        // getInfo
        this.cachedInfo = {};
        this.http = new HttpHelpers();
        if (logger)
            this.log = this.http.log = logger;
    }
    getIdsProvider() {
        return this.idsProvider;
    }
    setSilentLvl(lvl) {
        this.silentLvl = lvl;
    }
    waitUpdating(log) {
        Future.task(() => {
            while (this.updating) {
                this.silentLvl >= 7 && log.info('Ожидается обновление ID');
                Meteor._sleepForMs(this.delaySec * 1000);
            }
            this.silentLvl >= 6 && log.info('Обновление ID завершено');
        }).wait();
    }
    update(log) {
        this.updating = true;
        Future.task(() => {
            let totalIds = _.size(this.idsToCheck) + _.size(this.idsReady);// - this.next_i;
            //log.info('idsToCheck: {1}, idsReady: {2}, next_i: {3}', _.size(this.idsToCheck), _.size(this.idsReady), this.next_i);
            this.silentLvl >= 5 && log.info('Обновление списка ID. Сейчас доступно {1} <= {2} ID', totalIds, this.minIdsLimit);
            try {
                var ids = this.idsProvider.nextIds();

                this.addIds(ids);
                totalIds = _.size(this.idsToCheck) + _.size(this.idsReady);// - this.next_i;
                if (totalIds)
                    this.silentLvl >= 3 && log.info('Обновление ID завершено. Доступно {1} ID', totalIds);
                else
                    this.silentLvl >= 4 && log.info('Обновление ID завершено. Нет доступных ID');
            } catch (e) {
                log.error('idsProvider.nextIds: {1}', e.stack);
                throw e;
            } finally {
                this.updating = false;
            }
            //if (_.size(ids))
            //    this.idsToCheck = this.idsToCheck ? _.union(this.idsToCheck, ids) : ids;
            //this.addIds(ids);
            //totalIds = _.size(this.idsToCheck) + _.size(this.idsReady);// - this.next_i;
            //if (totalIds)
            //    this.silentLvl >= 3 && log.info('Обновление ID завершено. Доступно {1} ID', totalIds);
            //else
            //    this.silentLvl >= 4 && log.info('Обновление ID завершено. Нет доступных ID');
        });
    }
    nextId(subTask) {
        let log = {
            info: (...params) => {
                if (subTask.isStopped() || subTask.isFinished())
                    throw new SomeError(ERR_TYPE.TASK_STOP);
                this.log.info(...params);
            },
            error: (...params) => {
                if (subTask.isStopped() || subTask.isFinished())
                    throw new SomeError(ERR_TYPE.TASK_STOP);
                this.log.error(...params);
            }
        };
        let totalIds = _.size(this.idsToCheck) + _.size(this.idsReady);
        //while (totalIds <= this.minIdsLimit && !this.idsProvider.isFinished()) {
        while (!totalIds && !this.idsProvider.isFinished()) {
            if (!this.updating)
                this.update(log);
            this.waitUpdating(log);
            totalIds = _.size(this.idsToCheck) + _.size(this.idsReady);
        }
        if (!this.updating && totalIds <= this.minIdsLimit && !this.idsProvider.isFinished())
            this.update(log);

        //if (totalIds <= this.minIdsLimit && !this.idsProvider.isFinished()) {
        //    if (!totalIds && this.updating) {
        //        this.waitUpdating(log);
        //        totalIds = _.size(this.idsToCheck) + _.size(this.idsReady);// - this.next_i;
        //    }
        //
        //    if (!totalIds && this.idsProvider.isFinished())
        //        return null;
        //
        //    if (!this.updating && totalIds <= this.minIdsLimit && !this.idsProvider.isFinished()) {
        //        this.update(log);
        //        if (!totalIds)
        //            this.waitUpdating(log);
        //    }
        //    //totalIds = _.size(this.idsToCheck) + _.size(this.idsReady);// - this.next_i;
        //}

        //if (this.next_i < this.idsReady.length)
        //    return this.idsReady[this.next_i++];
        //this.idsReady = [];
        //this.next_i = 0;
        if (this.idsReady.length)
            return this.idsReady.shift();

        if (!_.size(this.idsToCheck))
            return null;

        var idsToCheck = this.idsToCheck.splice(0, this.maxCheckCnt || this.idsToCheck.length);
        this.idsReady = _.reduce(this.filters, (filtered, filter, filterName) => {
            if (!filtered.length) return [];
            let result = filter(filtered, subTask);
            if (result.length)
                this.silentLvl >= 2 && this.log.info('После фильтра {1} осталось {2}/{3} ID', filterName, result.length, filtered.length);
            else
                this.silentLvl >= 4 && this.log.info('После фильтра {1} осталось {2}/{3} ID', filterName, result.length, filtered.length);
            return result;
        }, idsToCheck);
        //this.idsReady = _.union(this.idsReady, filtered);
        //this.idsReady = filtered;
        try {
            this.TMP_CNT = (this.TMP_CNT || 0) + 1;
            if (this.TMP_CNT > 100)
                log.info('asdsadsadsa');
            return this.nextId(subTask); // if !filtered.length && maxCheckCnt > 0 -> try again
        } catch (e) {
            let exitState = e.message || e;
            this.error('e: {1}', e.stack);
            this.error('e: {1}', exitState);
            throw e;
        }
    }
    addIds(ids) {
        this.idsToCheck = _.union(this.idsToCheck, ids);
    }
    addFilter(filterName) {
        this.filters[filterName] = IdsFilters[filterName];
        //this.filters.push(filter);
    }

    getInfo(id) {
        if (!this.cachedInfo[id])
            this.getInfos([id]);
        return this.cachedInfo[id];
    }
    getInfos(ids) {
        const MAX_CNT = 200;
        let toRequest = _.difference(ids, _.keys(this.cachedInfo));
        var userIds = _.union(toRequest, _.filter(this.idsReady, id => this.cachedInfo[id]), _.filter(this.idsToCheck, id => this.cachedInfo[id]));
        userIds = userIds.slice(0, MAX_CNT);
        this.log.info('Обновляем кэш информации пользователей');
        var result = VkAcc.apiNoAuth('users.get', { user_ids: userIds, fields: this.fields }, null, this.http);
        _.each(result, user => this.cachedInfo[user.id] = user);
        return _.pick(this.cachedInfo, ids);
        //return _.map(ids, id => this.cachedInfo[id]);
    }
}
