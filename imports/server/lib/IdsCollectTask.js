import VkAcc from "../VkApi";
import { getQueryParams } from "../../common/helpers";
import SomeError from "../../common/SomeError";
import { ERR_TYPE } from "../../common/SomeError";
import { IdsSourceTypes } from "../../common/types";
import MultiAccTask from "./MultiAcc";
import { ApplyIdsQueue } from "./IdsIterateHelpers";
import { Actions } from "./IdsIterateHelpers";
import { MultiAccSubTask } from "./MultiAcc";
import HttpHelpers from "./HttpHelpers";
import AccManager from "../AccManager";
import * as _ from 'lodash';
import { log } from "../../common/logger/Logger";
import Future from 'fibers/future';

export class IdsCollectTaskSingle extends MultiAccSubTask {
    constructor(parent, title) {
        super(parent.ownerId, 'IdsCollectTaskSingle', parent, title);
    }

    parallel() {
        let { idsListName, idsSearchParams } = this.params;
        this.state.url_i = this.state.url_i || 0;
        let { accs, urls } = idsSearchParams;
        let mngr = new AccManager(this.ownerId);
        accs = mngr.getAccs(accs);
        let title = this.title;

        let doneCnt, doneTotalCnt = 0, totalCnt;
        let onReady = result => {
            this.title = result.acc.login;
            this.info('Запрос #{1} -> {2}', result.requestCnt, _.omit(result.params, ['count', 'fields']));
            doneCnt += result.items.length;
            doneTotalCnt += result.items.length;
            totalCnt = result.totalCnt;
            this.setState({ doneCnt, doneTotalCnt, totalCnt });
            this.info('Результат: {1}/{2}; Всего {3}/{4}', result.items.length, result.count, doneCnt, totalCnt);

            let ids = _.map(result.items, 'id');
            let ok = Actions.addToUserIdList(idsListName, ids, this.ownerId);
            if (!ok) throw new SomeError(ERR_TYPE.TASK_LOGIC, 'Add to userIdLists FAIL');
        };

        //while (_.size(urls)) {
        for (let i = this.state.url_i || 0; i < urls.length; i++) {
            doneCnt = 0;
            let url = urls[i], urlParams = IdsSearchByUrl.url2SearchParams(url);
            this.info('URL: {1}', url);
            this.debug('URL params: {1}', urlParams);

            IdsSearchByUrl.parallelSearch(accs, urlParams, onReady, this.log);
            this.info('Всего собрано {1}/{2} ID с URL {3}', doneCnt, totalCnt, url);
            this.setState({ url_i : i + 1 });
        }
        //this.setState({ url_i : urls.length });
        this.title = title;
    }

    iterative() {
        let { idsQueue } = this, userId, ids = [];
        idsQueue.delaySec = 0.5;
        idsQueue.silentLvl = 2;
        let { idsListName } = this.params;
        let addToDB = ids => {
            let ok = Actions.addToUserIdLists([idsListName], ids, this.ownerId, this.log);
            if (!ok) throw new SomeError(ERR_TYPE.TASK_LOGIC, 'Add to userIdLists FAIL');
        };
        let cnt = 0;
        while (userId = idsQueue.nextId(this)) {
            ids.push(userId);
            if (ids.length % 1000 == 0) {
                //if (++cnt % 1000 == 0) {
                this.info('Собрано {1} ID', cnt += ids.length);
                addToDB(ids);
                ids = [];
                //this.delay(2);
            }
        }
        addToDB(ids);
        this.info('Сбор закончен! Всего: {1} ID', cnt += ids.length);
    }

    start() {
        super.start();
        this.wrapErrHandlers(() => {
            if (this.params.algorithm == 'parallel')
                this.parallel();
            else
                this.iterative();
        });
    }
}


export default class IdsCollectTask extends MultiAccTask {
    constructor(ownerId, params) {
        super(ownerId, 'IdsCollectTask', params, IdsCollectTaskSingle);
    }

    startNow() {
        try {
            this.tasks.forEach(task => task.params = this.state.params);

            if (_.get(this.state, 'params.algorithm') != 'parallel')
                ApplyIdsQueue[IdsSourceTypes.SEARCH_URLS](this);

            super.startNow();
            this.waitChildrenAndPrint();
        } catch (e) {
            this.error('>>>>>>>>>>>>>>>> TASK EXCEPTION: {1}', e.stack);
        }
    }
}



export const VkDatabase = new Mongo.Collection('VkDatabase');

function vkDbUpdateCities(country_id, http) {
    //let cities = VkAcc.allWithOffsetStatic('database.getCities', { country_id: country_id, need_all: 1 }, 1000, 0, http);
    let cities = VkAcc.allWithOffsetStatic('database.getCities', { country_id: country_id }, 1000, 0, http);
    log.debug('cities[{1}]: {2}', country_id, cities);
    VkDatabase.insert({ type: 'cities', data: cities.items, country: country_id });
    return cities;
}


Meteor.startup(() => {
    //IdsSearchByUrl.TEST_Parallel();
});


export class IdsSearchByUrl {
    static url2SearchParams(url) {
        var qParams = getQueryParams(url);
        var params = {};
        var cParamsList = [ 'country', 'city', 'school', 'school_year', 'school_class', 'university',
            'age_from', 'age_to', 'sex', 'status', 'online', 'religion', 'company', 'position', 'sort' ];
        var ignoreParams = [ 'name', 'section', 'people_priority', 'personal_priority', 'smoking', 'alcohol', 'mil_country', 'mil_unit', 'mil_year_from', 'uni_city', 'uni_country' ];
        var qParamsMap = {
            'faculty': 'university_faculty',
            'chair': 'university_chair',
            'uni_year': 'university_year',
            'photo': 'has_photo',
            'byear': 'birth_year',
            'bmonth': 'birth_month',
            'bday': 'birth_day',
            'group': 'group_id'
        };
        var result = {};
        if (qParams['c[section]'] != 'people')
            throw new Error('It\'s not a people search url: {1}'.format(url));
        _.each(qParams, function(val, qParam) {
            var match = qParam.match(/^c\[(.*)\]$/);
            if (match) {
//                this.debug('cParam: {1}', qParam);
                var param = match[1];
                if (param == 'sort')
                    val = val == 3 ? 0 : val;
                if (_.includes(cParamsList, param))
                    result[param] = val;
                else if (qParamsMap[param])
                    result[qParamsMap[param]] = val;
                else if (!_.includes(ignoreParams, param))
                    throw new Error('Unknown search param: {1}'.format(qParam));
            }
        });
        return result;
    }

    static TEST_() {
        Future.task(() => {
            try {
                let acc = new VkAcc('48669626546:jB9iBLsBm5h2ERRlZb7E  64.110.27.34:60099|15RB:CPhiklKD');
                acc.token = '27379ee172ce0085160b16b1e52cdb1867b23b3549ee105587fe6d8130a9ce11d27bbc1e4d91368764a9b'
                //let initParams = IdsSearchByUrl.url2SearchParams('https://vk.com/search?c[age_from]=20&c[age_to]=22&c[city]=444&c[country]=2&c[name]=1&c[photo]=1&c[section]=people');
                //let initParams = IdsSearchByUrl.url2SearchParams('https://vk.com/search?c[group]=111572021&c[name]=1&c[photo]=1&c[section]=people&c[sex]=1');
                let initParams = IdsSearchByUrl.url2SearchParams('https://vk.com/search?c[bday]=2&c[bmonth]=3&c[byear]=1980&c[country]=3&c[name]=1&c[online]=1&c[photo]=1&c[section]=people&c[sex]=2&c[sort]=3');
                initParams = _.omit(initParams, ['age_from', 'age_to']);

                const fixedFields = _.keys(_.pick(initParams, ['sex', 'online', 'birth_month', 'birth_day', 'country', 'status', 'city']));
                let fields = 'country,bdate,sex,online,online_mobile,can_send_friend_request,last_seen,photo_200_orig,universities';

                let criteriaOrder = [], params = initParams;
                //let http = new HttpHelpers(log);
                do {
                    let response = IdsSearchByUrl.search([acc], params, criteriaOrder, fields, fixedFields, log);
                    log.info('CURRENT criteriaOrder: {1}; params: {2}', criteriaOrder, params);
                    log.info('RESULT: {1}/{2}', response.items.length, response.count);
                    log.info('NEXT criteriaOrder: {1}; params: {2}', criteriaOrder, params);
                } while (criteriaOrder.length);
                log.info('pppp11111111111111111111pppppp');
            } catch (e) {
                log.error('ERR: {1}', e.message);
                log.error('ERR stack: {1}', e.stack);
            }
        });
    }

    static TEST_Parallel() {
        Future.task(() => {
            try {
                let acc = new VkAcc('79879180659:ytrewq1955 212.109.221.155:8080|LEBASU:LEBASU');
                acc.token = '1d430e504f50df7e16341fe2a01bdbdd841b81b5fd6428888571c95eb9f8d9af1bdce7bbc71abf850be71';
                //let initParams = IdsSearchByUrl.url2SearchParams('https://vk.com/search?c[age_from]=20&c[age_to]=22&c[city]=444&c[country]=2&c[name]=1&c[photo]=1&c[section]=people');
                //let initParams = IdsSearchByUrl.url2SearchParams('https://vk.com/search?c[group]=111572021&c[name]=1&c[photo]=1&c[section]=people&c[sex]=1');
                let initParams = IdsSearchByUrl.url2SearchParams('https://vk.com/search?c[age_from]=19&c[age_to]=20&c[bday]=2&c[country]=3&c[name]=1&c[photo]=1&c[section]=people&c[sex]=2&c[sort]=3');

                let items = [];
                let onReady = result => {
                    log.info('Запрос #{1}', result.requestCnt);
                    log.info('Search params: {1}', result.params);
                    items = _.unionBy(items, result.items);
                    log.info('_RESULT: {1}/{2} -> {3}/{4}', result.items.length, result.count, items.length, result.totalCnt);
                };
                IdsSearchByUrl.parallelSearch([acc], initParams, onReady, log);

                log.info('pppp11111111111111111111pppppp');
            } catch (e) {
                log.error('ERR: {1}', e.message);
                log.error('ERR stack: {1}', e.stack);
            }
        });
    }



    static parallelSearch(accs, initParams, onReady, log = log, fields = '', delay = 0) {
        let ageFrom = initParams.age_from || 17;
        let ageTo = initParams.age_to || 30;
        delete initParams.age_from;
        delete initParams.age_to;

        var scrapeByParams = function(acc, params, criteriaLvl, criteriaOrder, nextCriteria) {
            params = _.clone(params || {});
            lockAcc(acc);

            let p = merge(params, {count: maxCount, fields});
            _.defaults(p, { age_from: ageFrom, age_to: ageTo });
            while (accs.length)
                try {
                    var response = acc.users().search(p);
                    break;
                } catch (e) {
                    if (e.name == ERR_TYPE.AUTH || e.name == ERR_TYPE.VK_API) {
                        _.pull(accs, acc);
                        log.debug('Search ERR: {1}', e.message);
                        acc = nextAcc();
                        continue;
                    }
                    throw e;
                }
            if (!accs.length)
                throw new SomeError(ERR_TYPE.TASK_WRONG_PARAM, 'All accs are bad');
            if (criteriaLvl < 0)
                totalResponceCnt = response.count;
            requestCnt++;

            //log.debug('response search: {1}', response);
            //log.info('Search params: {1}', params);
            //log.info('response cnt: {1}/{2}', _.get(response, 'items.length'), _.get(response, 'count'));
            if (!_.isNumber(_.get(response, 'count')))
                throw new SomeError(ERR_TYPE.TASK_LOGIC, 'Empty response!');
            if (response.count > maxCount) {
                nextCriteria(params, criteriaLvl + 1, criteriaOrder, response.count);
                return unlockAcc(acc);
            }
            onReady(merge(response, { totalCnt: totalResponceCnt, acc, params: p, criteriaOrder, requestCnt }));
            unlockAcc(acc);
        };
        const maxCount = 1000;
        var requestCnt = 0, totalResponceCnt;

        const fixedFields = _.keys(_.pick(initParams, ['sex', 'online', 'birth_month', 'birth_day', 'country', 'status', 'city']));


        let nextAccIndex = 0;
        let futuresQueue = [];
        var nextAcc = function() {
            if (lockAccs.length == accs.length) {
                let waitFuture = new Future();
                futuresQueue.push(waitFuture);
                waitFuture.wait();
                return waitFuture.get();
            }
            let readyAccs = _.difference(accs, lockAccs);
            if (nextAccIndex >= readyAccs.length)
                nextAccIndex = 0;
            return readyAccs[nextAccIndex++];
        };
        let lockAccs = [];
        let lockAcc = acc => {
            if (!_.includes(lockAccs, acc))
                lockAccs.push(acc);
        };
        let unlockAcc = acc => {
            _.pull(lockAccs, acc);
            if (!lockAccs.length)
                futureFinished.return();
            let waitFuture = futuresQueue.shift();
            waitFuture && waitFuture.return(acc);
        };
        let futureFinished = new Future();


        // return true if hasn't nextCriteria
        var nextCriteria = function(params, lvl, criteriaOrder, lastCnt) {
            criteriaOrder = _.clone(criteriaOrder);
            IdsSearchByUrl.updateCriteriaOrder(criteriaOrder, lastCnt, fixedFields);
            //log.info('criteriaOrder: {1}', criteriaOrder.slice(lvl));
            if (lvl >= criteriaOrder.length)
                return true;
            log.info('Next lvl: {1}', criteriaOrder[lvl]);

            let future = new Future(), doneCnt = 0, criteriaLength = 0;
            while (IdsSearchByUrl.nextParam(criteriaOrder[lvl], params, ageFrom, ageTo)) {
                //log.debug('NEW FIBER({2}): {1}', params, criteriaLength);
                criteriaLength++;
                let acc = nextAcc();
                lockAcc(acc);
                let p = _.clone(params);
                Future.task(() => {
                    scrapeByParams(acc, p, lvl, criteriaOrder, nextCriteria);
                    if (++doneCnt >= criteriaLength && !future.isResolved())
                        future.return();
                });
            }

            return false;
        };
        scrapeByParams(nextAcc(), initParams, -1, [], nextCriteria);
        futureFinished.wait();
    }


    // return true if discover the end of criteria
    static nextParam(criteria, params, ageFrom, ageTo) {
        switch (criteria) {
            case 'age' :
                if ((params.age_to || 0) >= ageTo) {
                    delete params.age_from;
                    delete params.age_to;
                    return false;
                }
                params.age_from = params.age_to = params.age_to ? ++params.age_to : ageFrom;
                return true;
            case 'sex' :
                if (params.sex == 2) {
                    delete params.sex;
                    return false;
                }
                params.sex = params.sex ? 2 : 1;
                return true;
            case 'online' :
                if (params.online == 1) {
                    delete params.online;
                    return false;
                }
                params.online = params.online === 0 ? 1 : 0;
                return true;
            case 'birth_month' :
                if (params.birth_month == 12) {
                    delete params.birth_month;
                    return false;
                }
                params.birth_month = params.birth_month || 0;
                params.birth_month++;
                return true;
            case 'birth_day' :
                if (params.birth_day == 31) {
                    delete params.birth_day;
                    return false;
                }
                params.birth_day = params.birth_day || 0;
                params.birth_day++;
                return true;
            case 'city' :
                let dbCities = VkDatabase.findOne({ type: 'cities', country: params.country });
                if (!dbCities)
                    dbCities = vkDbUpdateCities(params.country, new HttpHelpers(log));
                else
                    dbCities = dbCities.data;
                dbCities = _.map(dbCities, 'id');

                if (params.city == _.last(dbCities)) {
                    delete params.city;
                    return false;
                }
                params.city = params.city || -1;
                let i = _.findIndex(dbCities, city => city == params.city);
                //let i = _.findIndex(dbCities, params.city) || -1;
                params.city = dbCities[i + 1];
                return true;
            case 'status' :
                let statuses = [1, 2, 5, 6, 7];
                if (params.status == 7) {
                    delete params.status;
                    return false;
                }
                params.status = params.status || -1;
                let j = _.findIndex(statuses, st => st == params.status);
                params.status = statuses[j + 1];
                return true;
            case 'country' :
                // https://api.vk.com/method/database.getCountries?need_all=1
                var countries = {
                    //'Russia': 1,
                    'Украина': 2,
                    'Беларусь': 3,
                    'Казахстан': 4,
                    'Азербайджан': 5
                };
                var countriesArr = _.map(countries, (val, key) => ({ country: key, cid: val }));
                var currIndex = countriesArr.indexOf(params.country);
                if (currIndex < 0) // params.country is undefined
                    currIndex = 0;
                else
                    countriesArr++;
                if (currIndex >= countriesArr.length) {
                    delete params.country;
                    return false;
                }
                params.country = countriesArr[currIndex].cid;
                return true;
        }
    }

    // select nextCriteria by lastCnt and move it to front in the criteriaOrder
    // return false if criteriaOrder is full
    static updateCriteriaOrder(criteriaOrder, lastCnt, fixedFields) {
        let lessBorderCnt = {
            sex: 2000, 						// if lastCnt < 1500 this criteria would be first
            //online: 1500,
            //age: (ageTo - ageFrom) * 1000,
            age: 10 * 1000,
            birth_month: 12 * 1000,
            birth_day: 30 * 1000,
            //status: 100,
            city: 200,
            country: 500, // last: tail < 1000 sort by descending
        };
        lessBorderCnt = _.omit(lessBorderCnt, fixedFields);
        if (!_.includes(fixedFields, 'country') && !_.includes(criteriaOrder, 'country'))
            lessBorderCnt = _.omit(lessBorderCnt, 'city');

        // transform lessBorderCnt to array, sort, find next criteria by the lastCnt, split array, reverse front part and swap it with tail part of arr
        let lessBorderArr = _.map(lessBorderCnt, (val, key) => ({ criteria: key, lessBorder: val }));
        lessBorderArr = _.sortBy(lessBorderArr, 'lessBorder');
        let bestCriteriaIndex = _.sortedIndexBy(lessBorderArr, { lessBorder: lastCnt} , item => item.lessBorder);
        lessBorderArr = lessBorderArr.concat(lessBorderArr.splice(0, bestCriteriaIndex).reverse());

        let nextCriteria = _.find(lessBorderArr, item => {
            return !_.includes(criteriaOrder, item.criteria);
        });
        if (!nextCriteria)
            return false;
        criteriaOrder.push(nextCriteria.criteria);
        return true;
    }




    static search(accs, params, criteriaOrder, fields, fixedFields = [], log) {
        //let log = http.log;
        if (params.age_from != params.age_to) { // first launch
            params.ageFromOrig = params.age_from;
            params.ageToOrig = params.age_to;
            delete params.age_from;
            delete params.age_to;
        }
        var ageFrom = params.ageFromOrig || 17;
        var ageTo = params.ageToOrig || 30;

        let nextResult = function(criteriaOrder, params) {
            const maxCount = 1000;
            let criteriaOrderFull = false, acc = nextAcc();
            while (!criteriaOrderFull) {
                let p = merge(params, {count: maxCount, fields: fields});
                _.defaults(p, { age_from: ageFrom, age_to: ageTo });
                while (accs.length)
                    try {
                        var response = acc.users().search(p);
                        break;
                    } catch (e) {
                        if (e.name == ERR_TYPE.AUTH || e.name == ERR_TYPE.VK_API) {
                            _.pull(accs, acc);
                            log.debug('Search ERR: {1}', e.message);
                            acc = nextAcc();
                            continue;
                        }
                        throw e;
                    }
                if (!accs.length)
                    throw SomeError(ERR_TYPE.TASK_WRONG_PARAM, 'All accs are bad');
                //log.debug('response search: {1}', response);
                //log.info('Search params: {1}', params);
                //log.debug('response cnt: {1}/{2}', _.get(response, 'items.length'), _.get(response, 'count'));
                if (!_.isNumber(_.get(response, 'count')))
                    throw new Error('Empty response!');
                if (response.count > maxCount) {
                    if (!criteriaOrder.length)
                        //criteriaOrder.push('city');
                        criteriaOrder.push('age');
                    else
                        if (!IdsSearchByUrl.updateCriteriaOrder(criteriaOrder, response.count, fixedFields)) // criteriaOrder is full
                            criteriaOrderFull = true;
                }
                //if (!IdsSearchByUrl.nextParam(_.last(criteriaOrder), params, ageFrom, ageTo)) { // end of a criteria
                //    criteriaOrder.splice(criteriaOrder.length - 1, 1); // remove last
                //    if (criteriaOrder.length)
                //        IdsSearchByUrl.nextParam(_.last(criteriaOrder), params, ageFrom, ageTo);
                //}
                while (!IdsSearchByUrl.nextParam(_.last(criteriaOrder), params, ageFrom, ageTo)) { // end of a criteria
                    criteriaOrder.splice(criteriaOrder.length - 1, 1); // remove last
                    if (!criteriaOrder.length)
                        break;
                }
                if (response.count <= maxCount)
                    break;
            }
            if (_.includes(criteriaOrder, 'age')) // criteriaOrder[0] must be is 'age' to fix all results
                fixBDates(response.items, params['age_from']);
            return response;
        };



        var nextAcc = function() {
            return _.sample(accs);
        };
        var fixBDates = function(users, age) {
            _.each(users, function(user) {
                user.age = age;
//				if (user.bdate.split('.').length == 2)
//					var bdate = moment(user.bdate, 'DD.MM.YYYY');
            });
        };

        return nextResult(criteriaOrder, params);
    }

}
