import MultiTask from "./MultiTask";
import AccManager from "../AccManager";
import ResumableTask from "./ResumableTask";
import { createTaskFromData } from "../helpers";

export default class MultiAccTask extends MultiTask {
    constructor(ownerId, taskName, params, subTaskClass) {
        super(ownerId, taskName, params, subTaskClass);
    }
    newTaskInit(params) {
        var manager = new AccManager(this.ownerId);
        //this.accs = _.map(params.accs, acc => manager.getAcc(acc));
        this.accs = manager.getAccs(params.accs || []);
        //if (_.has(this.accs, '[0]') && !(this.accs[0] instanceof AccInfo))
        //    this.accs = _.map(this.accs, accData => AccInfo.deserializeAcc(accData));
        var self = this; // bug??
        this.tasks = _.map(this.accs, acc => {
            var task = new this.subTaskClass(self, acc.login);
            task.acc = acc;
            acc.http.log = acc.log = task.log;
            return task;
        });

        super.newTaskInit(params);
        //this.state.params = params;//_.omit(params, 'accs');
    }
    updateParams(params) {
        var manager = new AccManager(this.ownerId);
        //this.accs = _.map(params.accs, acc => mngr.getAcc(acc));
        this.accs = manager.getAccs(params.accs || []);

        var self = this; // bug??
        this.tasks = _.map(this.accs, acc => {
            let task = _.find(this.tasks, ['acc.login', acc.login]);
            if (!task)
                task = new this.subTaskClass(self, acc.login);
            task.acc = acc;
            acc.log = task.log;
            return task;
        });

        this.state.params = params;//_.omit(params, 'accs');
    }

    //waitChildrenAndPrint() {
    //    let { OK, ...FAIL } = this.waitChildren();
    //    this.info('--------------------------');
    //    _.each(FAIL, (logins, exitState) => this.info('ОШИБКИ: {1}: {2}', exitState, logins.join(', ')));
    //    this.info('Все аккаунты обработаны. Успешно завершены {1}/{2}', OK.length, this.accs.length);
    //    this.finished();
    //}

    serialize() {
        var data = { state: this.state };
        //data.accs = _.map(this.accs, acc => acc.serialize());
        data.accs = _.map(this.accs, acc => acc._id);
        data.tasks = _.map(this.tasks, task => {
            var taskData = task.getMetaData();
            taskData.data = task.serialize();
            return taskData;
            //return { taskInfo: task.getMetaData(), taskData: task.serialize() } });
        });
        //data.tasksName = _.get(this.tasks, '[0].taskName');
        return data;
    }
    deserialize(data) {
        this.state = data.state;
        var mngr = new AccManager(this.ownerId);
        this.accs = mngr.getAccs(data.accs);
        //let self = this;
        //this.accs = _.map(data.accs, accData => mngr.getAcc(accData));
        //this.tasks = _.map(data.tasks, taskData => ResumableTask.createFromData(this.tasksName, taskData));
        this.tasks = _.map(data.tasks, taskData => {
            var task = createTaskFromData(taskData, this);
            task.acc = _.find(this.accs, ['login', task.title]);
            task.acc.http.log = task.acc.log = task.log;
            //task.parent = this;
            return task;
        });
        //_.each(this.tasks, task => task.parent = this);
    }
    saveState() {
        if (this.saveStateThrottleEnable)
            this.saveStateThrottle();
        else
            super.saveState();
    }
}

export class MultiAccSubTask extends ResumableTask {
    constructor(...args) {
        super(...args);
    }

    serialize() {
        return _.merge(super.serialize(), {
            acc: _.pick(this.acc.serialize(), ['login', 'pass', 'proxy', 'state', 'blockedReason'])
        });
    }
    //serialize() {
    //    return _.merge(super.serialize(), {
    //        acc: this.acc.serialize()
    //    });
    //}
    //deserialize(data) {
    //    let fields = ['acc'], ownData = _.pick(data, fields), parentData = _.omit(data, fields);
    //    super.deserialize(parentData);
    //    // acc deserial in the parent MultiTask
    //}
}
