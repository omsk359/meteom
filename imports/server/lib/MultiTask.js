import { delayByNum } from "../../common/helpers";
import { TaskRunStates } from "../../common/types";
import ScheduledTask from "./ScheduledTask";
import ResumableTask from "./ResumableTask";
import { createTaskFromData } from "../helpers";
import Future from 'fibers/future';
import moment from 'moment';

export default class MultiTask extends ScheduledTask {
    constructor(ownerId, taskName, params, subTaskClass) {
        super(ownerId, taskName, null, '', params);
        this.shiftDelay = [0, 0];
        this.tasks = [];
        this.subTaskClass = subTaskClass;

        this.saveStateThrottleEnable = false;
        this.saveStateThrottle = _.throttle(Meteor.bindEnvironment(() => super.saveState()), 2 * 1000);
    }
    futureShift(subTaskId, func, shift) {
        if (shift > 0) {
            this.debug('Пауза перед запуском: {1} сек', shift / 1000);
            this.nextRunTime[subTaskId] = +moment().add(shift, 'ms');
            this.saveState();
        }
        var self = this; // setTimeout loose lambda binding
        this.futureDelayIntId = this.futureDelayIntId || {};
        this.futureDelayIntId[subTaskId] = Meteor.setTimeout(() => {
            //this.debug('Запуск задачи #{1}', subTaskId);
            Future.task(() => {
                try {
                    func();
                } catch (e) {
                    self.error('futureDelayIntId: {1}', e.stack);
                    self.error('futureDelayIntId: {1}', e.message || e);
                }
            });
        }, shift);
    }
    newTaskInit(params) {
        for (let i = 0; i < params.poolSize; i++) {
            let task = new this.subTaskClass(this, '#' + i);
            this.tasks.push(task);
        }

        super.newTaskInit(params);
        //this.state.params = params;//_.omit(params, 'accs');
    }

    updateParams(params) {
    }
    getDelay(i) {
        return delayByNum(this.state.params.delays, i);
    }
    startNow() {
        if (_.has(this.state.params, 'delays[0][0]'))
            this.shiftDelay = this.state.params.delays[0];
        this.nextRunTime = {};
        _.each(this.tasks, (task, i) => {
            this.futureShift(i, () => {
                task.start();
            }, i * _.random(...this.shiftDelay) * 1000);
        });
        super.startNow();
        this.saveStateThrottleEnable = true;
    }
    restartTaskNow(i) {
        let task = this.tasks[i];
        this.futureShift(i, () => {
            task.start();
        }, 0);
    }
    waitChildren() {
        let future = new Future();
        let results = {}, self = this;
        let doneCnt = _.get(_.countBy(this.tasks, task => task.isStopped()), 'true') || 0;
        _.each(this.tasks, (task, i) => task.eventEmitter.once(TaskRunStates.FINISHED, result => {
            results[i] = result.replace(/\./g, '\uFF0E'); // MongoError: The dotted field
            if (++doneCnt >= self.tasks.length)
                future.return();
        }));
        if (doneCnt < this.tasks.length)
            future.wait();
        // group by results { OK -> ['task1', 'task2'], ... }
        //results = _.groupBy(this.tasks.map(t => t.title), (login, i) => results[i]);
        let titles = this.tasks.map(t => t.title);
        let grouped = _.transform(titles, (grouped, login, i) =>
            grouped[results[i]] = _.union(grouped[results[i]], [login])
        , {});
        grouped['OK'] = grouped['OK'] || [];
        this.setState({ results: grouped });
        return grouped;
    }
    waitChildrenAndPrint() {
        let { OK, ...FAIL } = this.waitChildren();
        this.info('--------------------------');
        _.each(FAIL, (logins, exitState) => this.info('ОШИБКИ: {1}: {2}', exitState, logins.join(', ')));
        this.info('Все задачи обработаны. Успешно завершены {1}/{2}', OK.length, this.tasks.length);
        this.finished();
    }
    pause() {
        _.each(this.futureDelayIntId, Meteor.clearInterval);
        //_.each(this.tasks, task => task.pause());
        _.each(this.tasks, task => {
            task.pause();
        });
        super.pause();
    }
    stop() {
        _.each(this.futureDelayIntId, Meteor.clearInterval);
        let cnt = _.get(_.countBy(this.tasks, task => task.isStarted()), 'false') || 0, self = this;
        _.each(this.tasks, task => {
            task.eventEmitter.once(TaskRunStates.STOPPED, () => {
                if (++cnt >= self.tasks.length)
                    super.stop();
            });
            task.stop();
        });
        if (cnt >= self.tasks.length)
            super.stop();
    }
    resumeNow() {
        if (!this.isPaused())
            return this.startNow();
        super.resumeNow();
        _.each(this.tasks, (task, i) => {
            this.futureShift(i, () => {
                if (task.isPaused())
                    task.resume();
                else
                    task.start();
            }, moment(_.get(this.nextRunTime, `[${i}]`)).diff(moment(), 'ms'));
        });
    }

    serialize() {
        var data = { state: this.state, nextRunTime: this.nextRunTime };
        data.tasks = _.map(this.tasks, task => {
            var taskData = task.getMetaData();
            taskData.data = task.serialize();
            return taskData;
        });
        return data;
    }
    deserialize(data) {
        this.state = data.state;
        this.nextRunTime = data.nextRunTime;
        this.tasks = _.map(data.tasks, taskData => {
            var task = createTaskFromData(taskData, this);
            task.parent = this;
            return task;
        });
    }
    saveState() {
        if (this.saveStateThrottleEnable)
            this.saveStateThrottle();
        else
            super.saveState();
    }
}

//MultiSubTask = class MultiSubTask extends ResumableTask {
//    constructor(...args) {
//        super(...args);
//    }
//    newTaskInit(params) {
//        if (params.poolSize)
//            this.tasks = _.range(params.poolSize, () => {});
//        super.newTaskInit(params);
//    }
//}
