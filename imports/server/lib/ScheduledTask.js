import ResumableTask from "./ResumableTask";
import * as schedule from 'node-schedule';

export default class ScheduledTask extends ResumableTask {
    constructor(ownerId, taskName, parent, title, params) {
        super(ownerId, taskName, parent, title, params);
        this.shiftDelay = [0, 0];
        this.intervalDelay = [0, 0];
        this.periodicTask = false;
    }
    getScheduleParams() {
        return _.get(this.initParams || this.state.params, 'taskScheduler') || {};
    }
    start() {
        let params = this.getScheduleParams();
        if (!params.enabled)
            return this.startNow();
        this.setWait();
        if (this.cronJob)
            this.cronJob.cancel();
        this.cronJob = schedule.scheduleJob(params.cron, Meteor.bindEnvironment(this.cronStart.bind(this)));
    }
    stop() {
        if (this.cronJob)
            this.cronJob.cancel();
        super.stop();
    }
    pause() {
        if (this.cronJob)
            this.cronJob.cancel();
        super.pause();
    }
    resume() {
        let params = this.getScheduleParams();
        if (!params.enabled)
            return super.resume();
        if (this.cronJob)
            this.cronJob.cancel();
        this.setWait();
        this.cronJob = schedule.scheduleJob(params.cron, Meteor.bindEnvironment(this.cronResume.bind(this)));
    }
    finished(exitState) {
        super.finished(exitState);
        let params = this.getScheduleParams();
        if (params.enabled)
            this.setWait();
    }

    cronStart() {
        if (!this.isStarted())
            this.startNow();
    }
    cronResume() {
        if (!this.isStarted())
            this.resumeNow();
    }
    // to override
    startNow() {
        super.start();
    }
    resumeNow() {
        super.resume();
    }
}
