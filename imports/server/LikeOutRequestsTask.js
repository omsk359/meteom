import ResumableTask from "./lib/ResumableTask";
import MultiAccTask from "./lib/MultiAcc";

export const LikeOutRequests_DB = new Mongo.Collection('LikeOutRequests');

export class LikeOutRequestsTaskSingle extends ResumableTask {
    constructor(parent, title) {
        super(parent.ownerId, 'LikeOutRequestsTaskSingle', parent, title);
        // acc serialize/deserialize in the MultiAccTask
    }

    start() {
        super.start();
        var params = this.parent.state.params;
        var likesCntMax = params.likesCntMax, likesCnt = 0;
        this.debug('start LikeOutRequestsTaskSingle: {1}', params);
        //return;

        var acc = this.acc;
        try {
            this.info('Получение исходящих заявок...');
            let friendsOutRequests = acc.friends().getRequests(true);
            this.info('Всего исходящих {1}/{2} заявок', friendsOutRequests.items.length, friendsOutRequests.count);
            let dbFiltered = LikeOutRequests_DB.find({ ownerId: this.ownerId, userIdAcc: acc.userId, userIdLike: { $in: friendsOutRequests.items }, photo: true }, { userIdLike: 1, _id: 0, userIdAcc: 0, ownerId: 0 });
            dbFiltered = dbFiltered.map(item => item.likedUserId);
            dbFiltered = _.difference(friendsOutRequests.items, dbFiltered);
            this.info('После фильтр. по базе: {1}/{2}', dbFiltered.length, friendsOutRequests.count);
            if (!dbFiltered.length) return;
            this.info('Получение диалогов...');
            var dialogs = acc.messages().getDialogs();
            this.info('Всего диалогов {1}/{2}', dialogs.items.length, dialogs.count);
            var ids2Send = [], ids2Check = [];
            _.each(dbFiltered, userId => {
                var userDialog = _.find(dialogs.items, item => item.message.user_id == userId);
                if (!userDialog)
                    return ids2Send.push(userId);
                if (userDialog.message.out)
                    ids2Check.push(userId);
            });
            this.info('Нет входящих сообщений от {1} чел., осталось проверить {2} заявок', ids2Send.length, ids2Check.length);
            this.idsQueue = new IdsQueueWithInfo(ids2Send, ids2Check, id => {
                this.info('Получаем историю сообщений с id{1}', id);
                var messages = acc.messages().getHistory(id, _.random(7, 15));
                return _.some(messages.items, item => item.out == 0);
            }, this.log, 'photo_id');
            let userId;
            let i = this.state.i = this.state.i || 0;
            while (likesCnt++ < likesCntMax && (userId = this.idsQueue.getNextId())) {
                let userInfo = this.idsQueue.getInfo(userId), userName = userInfo.last_name + ' ' + userInfo.first_name;
                this.info('Следующий id{1} ({2}) photo_id: {3}', userId, userName, _.get(userInfo, 'photo_id'));
                if (!_.has(userInfo, 'photo_id')) {
                    this.info('Нет фото!');
                    continue;
                }
                let [ owner_id, item_id ] = userInfo.photo_id.split('_');
                let result = acc.likes().add('photo', owner_id, item_id);
                if (_.has(result, 'likes')) {
                    this.info('Лайк поставлен. Всего {1} лайков на этом фото.', result.likes);
                    LikeOutRequests_DB.insert({ ownerId: this.ownerId, userIdAcc: acc.userId, userIdLike: userId, photo: true });
                } else
                    this.error('Не удалось поставить лайк! {1}', result);
                if (likesCnt < likesCntMax)
                    this.delay(this.parent.getDelay(++i));
            }
            if (likesCnt > likesCntMax)
                this.info('Разослали {1} - макс. ограничение', likesCntMax);
        } catch (e) {
            if (e.name == 'PROXY')
                return this.debug('Proxy {1} error!', acc.proxy.ip);
            else if (e.name == 'TASK TIMEOUT')
                return this.error('TASK TIMEOUT');
            else if (e.name == 'TASK STOP') {
                return this.debug('TASK STOP');
            } else if (e.name != 'AUTH') {
                this.error('e: {1}', _.get(e, 'stack'));
                this.error('e: {1}', e.message || e);
                throw e;
            }
            this.error('Ошибка авторизации: {1}', this.acc.getState());
        } finally {
            this.info('Закончили!');
            this.finished();
        }
    }

    pause() {
        super.pause();
        if (this.idsQueue)
            this.idsQueue.pause();
    }
    resume() {
        super.resume();
        if (this.idsQueue)
            this.idsQueue.resume();
    }
    stop() {
        super.stop();
        if (this.idsQueue)
            this.idsQueue.stop();
    }
}

export default class LikeOutRequestsTask extends MultiAccTask {
    constructor(ownerId, params) {
        super(ownerId, 'LikeOutRequestsTask', params, LikeOutRequestsTaskSingle);
        this.shiftDelay = 100;
    }

    start() {
        super.start();
        this.debug('LikeOutRequestsTask RUN: {1}', this.accs);
        this.waitChildren();
        this.info('Все аккаунты обработаны');
        this.finished();
    }
}