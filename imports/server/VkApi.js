import AccInfo from "../common/AccInfo";
import { AccTypes } from "../common/AccInfo";
import Logger from "../common/logger/Logger";
import { ERR_TYPE } from "../common/SomeError";
import { AccStateType } from "../common/AccInfo";
import HttpHelpers from "./lib/HttpHelpers";
import SomeError from "../common/SomeError";
import { toQueryString } from "../common/helpers";
import { omitUndef } from "../common/helpers";
import { _getStr } from "../common/helpers";

export const VK_ERR = {
    CAPTCHA: 'CAPTCHA'
};

const CAPTCHA_ERR_MAX_CNT = 3;

export default class VkAcc extends AccInfo {
    constructor(str) {
        super(str);
        this.log = new Logger();
        this.vkUnknownErrCnt = 0;
        this.vkUnknownErrCntMax = 4;
        this.type = AccTypes.VK;
        //if (str)
            this.http = new HttpHelpers(this.log, this.userAgent, this.cookieJar, this.proxy);
        this.unfreezeData = null;
    }

    // API ============================================

    allWithOffset(method, params = {}, maxCnt, startOffset = 0, noAuth = false) {
        //params = _.pickBy(params, a => a !== undefined); // remove keys with undefined values
        params = _.transform(params, (params, val, key) => val === undefined || (params[key] = val));
        var results = { items: [] }, offset = startOffset;
        do {
            var response = this.api(method, merge(params, { offset: offset, count: maxCnt }), noAuth);
            //this.debugApi(method, 'offset({1}): {2}', offset, response);
            results.count = response.count;
            results.items = results.items.concat(response.items);
            offset += response.items.length;
        } while (response.items.length >= maxCnt && results.items.length < results.count);
        return results;
    }

    static allWithOffsetStatic(method, params, maxCnt, startOffset = 0, http) {
        //params = _.pickBy(params, a => a !== undefined); // remove keys with undefined values
        params = _.transform(params, (params, val, key) => val === undefined || (params[key] = val));
        var results = { items: [] }, offset = startOffset;
        do {
            var response = VkAcc.apiNoAuth(method, merge(params, { offset: offset, count: maxCnt }), null, http);
            results.count = response.count;
            results.items = results.items.concat(response.items);
            offset += response.items.length;
        } while (response.items.length >= maxCnt && results.items.length < results.count);
        return results;
    }

    friends() {
        return {
            get: fields => {
                let params = { user_id: this.getUserId(), fields };
                //if (fields) _.assign(params, { fields });
                return this.api('friends.get', params, true);
                //return VkAcc.apiNoAuth('friends.get', params, acc.proxy, acc.http);
                //return this.allWithOffset('friends.get', params);
            },
            getRequests: (out = true, extended = false) => this.allWithOffset('friends.getRequests', { out: Number(!!out), extended: Number(!!extended) }, 1000),
            add: (userId, msg, follow) => {
                let params = { user_id: userId };
                if (typeof follow != 'undefined')
                    params.follow = !!follow;
                if (msg) params.text = msg;
                return this.api('friends.add', params);
            },
            delete: user_id => this.api('friends.delete', { user_id })
        }
    }

    likes() {
        return {
            add: (type, owner_id, item_id) => this.api('likes.add', { type, owner_id, item_id })
        }
    }

    users() {
        return {
            search: params => this.api('users.search', params)
        }
    }

    groups() {
        return {
            get: (params = {}) => {
                if (!params.user_id)
                    params.user_id = this.getUserId();
                return this.allWithOffset('groups.get', params, 1000);
            },
            leave: group_id => this.api('groups.leave', { group_id }),
            getInvites: extended => this.allWithOffset('groups.getInvites', { extended }, 1000)
        }
    }

    video() {
        return {
            get: params => this.allWithOffset('video.get', params, 200),
            delete: (video_id, owner_id, target_id) => this.api('video.delete', { video_id, owner_id, target_id })
        }
    }

    audio() {
        return {
            get: params => this.api('audio.get', _.defaults(params, { count: 6000 })),
            delete: (audio_id, owner_id) => this.api('audio.delete', { audio_id, owner_id })
        }
    }

    messages() {
        return {
            getDialogs: unreadOnly => {
                return this.allWithOffset('messages.getDialogs', { unread: unreadOnly ? 1 : 0 }, 200);
            },
            getHistory: (user_id, offset = 0, rev = 0, totalMaxCnt = 0, start_message_id = 0) => {
                if (start_message_id) {
                    var hist = this.api('messages.getHistory', { user_id, start_message_id, count: 200 });
                    //if (hist.items.length == 200)
                    return hist;
                }
                if (totalMaxCnt)
                    return this.api('messages.getHistory', { user_id: user_id, count: totalMaxCnt });
                return this.allWithOffset('messages.getHistory', { user_id: user_id, rev }, 200, offset);
            },
            send: (user_id, message) => this.api('messages.send', { user_id, message }),
            get: params => {
                let resultIn = this.allWithOffset('messages.get', params, 200);
                if (_.has(params, 'out'))
                    return resultIn;
                let resultOut = this.allWithOffset('messages.get', merge(params, { out: 1 }), 200);
                return {
                    count: resultIn.count + resultOut.count,
                    items: _.union(resultIn.items, resultOut.items)
                }
            },
            delete: ids => {
                if (!(ids instanceof Array))
                    ids = [ids];
                return this.api('messages.delete', { message_ids: ids.join(',') })
            }

        }
    }

    account() {
        return {
            setOnline: () => this.api('account.setOnline'),
            saveProfileInfo: params => this.api('account.saveProfileInfo', params)
        }
    }

    photos() {
        //let self = this;
        return {
            get: params => {
                if (!params.owner_id && this.userId)
                    params.owner_id = this.userId;
                return this.allWithOffset('photos.get', params, 1000, 0, !!params.owner_id);
            },
            delete: (photo_id, owner_id) => this.api('photos.delete', { photo_id, owner_id }),
            getAlbums: params => this.api('photos.getAlbums', params),
            deleteAlbum: (album_id, group_id) => this.api('photos.deleteAlbum', {album_id, group_id}),
            getAll: params => this.allWithOffset('photos.getAll', params, 200)
        }
    }

    wall() {
        return {
            get: (params = {}) => {
                if (!params.owner_id && this.userId)
                    params.owner_id = this.userId;
                return this.allWithOffset('wall.get', params, 100, 0, !!params.owner_id);
            },
            delete: (post_id, owner_id) => this.api('wall.delete', { post_id, owner_id: owner_id || this.getUserId() })
        }
    }

    // END API ============================================

    api(method, params = {}, noAuth = false) {
        //params = _.transform(params, (params, val, key) => val === undefined || (params[key] = val));
        //params = _.pickBy(params, a => a !== undefined); // remove keys with undefined values
        params = omitUndef(params);
        if (!noAuth) {
            if (!this.token)
                try {
                    this.updateToken();
                } catch (e) {
                    if (e.name != ERR_TYPE.UPDATE_TOKEN)
                        throw e;
                    this.updateToken();
                }
            params.access_token = this.token;
        }
        var json = VkAcc.apiNoAuth(method, params, this.proxy, this.http);

        if (_.has(json, 'error.error_msg')) {
            var vkErr = _getStr(json, 'error.error_msg');
            this.debug('vkErr: {1}', vkErr);
            if (_.some(['access_token has expired', 'no access_token passed', 'invalid access_token',
                        'access_token was given to another ip address', 'no access to call this method'], str => vkErr.contains(str))) {
                this.debug('Требуется авторизация!');
                try {
                    this.updateToken();
                } catch (e) {
                    if (e.name != ERR_TYPE.UPDATE_TOKEN)
                        throw e;
                    this.updateToken();
                }
                //this.debug('Token2: {1}', this.token);
                return this.api(method, params, noAuth);
            }
            if (vkErr == 'Captcha needed') {
                this.info('Капча!');
                if (!this.ruCaptchaKey)
                    throw new SomeError(ERR_TYPE.CAPTCHA, 'RuCaptcha key not found');
                try {
                    var text = this.captcha(json.error.captcha_img);
                    if (text) {
                        this.info('Капча получена');
                        this.captchaErrCnt = 0;
                    }
                    return this.api(method, _.assign(params, {
                        captcha_sid: json.error.captcha_sid,
                        captcha_key: text
                    }), noAuth);
                } catch (e) {
                    this.log.error('Captcha error: {1}', e.message);
                    if (++this.captchaErrCnt >= CAPTCHA_ERR_MAX_CNT)
                        throw new SomeError(ERR_TYPE.CAPTCHA, e.message);
                    return this.api(method, params, noAuth);
                }
            }
            if (vkErr == 'Unknown error occurred') {
                this.log.error('VK: Неизвестная ошибка');
                if (++this.vkUnknownErrCnt >= this.vkUnknownErrCntMax)
                    throw new SomeError(ERR_TYPE.VK_API, vkErr);
                return;
            }
            if (vkErr == 'Internal server error: could not get application') {
                const delay = 60;
                this.log.info('Пауза {1} сек', delay);
                Meteor._sleepForMs(delay * 1000);
                return this.api(method, params, noAuth);
            }
            throw new SomeError(ERR_TYPE.VK_API, json.error.error_msg);
        }
        this.vkUnknownErrCnt = 0;
        return json;
    }

    static apiNoAuth(method, params, proxy, http = new HttpHelpers()) {
        //http = http || new HttpHelpers();
        http.proxy = proxy;
        _.defaults(params, { v: '5.44' });
        var reqUrl = 'https://api.vk.com/method/' + method + '?' + toQueryString(params);
        var response = http.get(reqUrl);
        var content = JSON.parse(response.content);
        //var vkErr = _getStr(content, 'error.error_msg');
        //this.debug('vkErr: {1}', vkErr);
        if ('response' in content)
            content = content.response;
        return content;
    }

    getUserId(enableExceptions) {
        if (this.userId) return this.userId;
        try {
            try {
                this.updateToken();
            } catch (e) {
                if (e.name != ERR_TYPE.UPDATE_TOKEN)
                    throw e;
                this.updateToken();
            }
            return this.userId;
        } catch (e) {
            this.log.debug('getUserId: {1}', e.message);
            if (enableExceptions)
                throw e;
        }
    }


    auth() {
        var checkAuth = result => {
            if (result.content.contains('login?act=blocked_phone') || result.content.contains('blocked_resend') || result.content.contains('blocked_done')) {
                this.setStateFreeze();
                var e = new SomeError(ERR_TYPE.AUTH, AccStateType.FREEZE);
                match = result.content.match(/login_blocked_reason_text">([^<]*)/);
                if (match)
                    e.cause = match[1];
                throw e;
            }
            if (result.content.contains('login_blocked_panel')) {
                this.setStateBan();
                e = new SomeError(ERR_TYPE.AUTH, AccStateType.BAN);
                match = result.content.match(/login_blocked_reason_text">([^<]*)/);
                if (match)
                    e.cause = match[1];
                throw e;
            }
            match = result.content.match(/<a href="\/id(\d+)" data-name="([^"]*)" data-photo="([^"]*)">/);
            if (!match)
                return false;
            this.debug('id: {1}; name: {2}; photo: {3}', match[1], match[2], match[3]);
            this.userId = match[1];
            this.setStateOK();
            return true;
        };
        var result = this.http.get('https://m.vk.com/');
        if (checkAuth(result)) {
//            this.debug('auth result: {1}', result);
            return;
        }
        var match = result.content.match(/<form method="post" action="([^"]+)"/);
        if (!match)
            throw new SomeError(ERR_TYPE.AUTH, 'Auth form not found!') ;
        var auth_url = match[1];
        // this.debug('auth_url: {1}', auth_url);

        result = this.http.post(auth_url, { email: this.login, pass: this.pass });

        if (result.content.contains('field_prefix')) { // security check
            this.debug('Проверка безопасности');
            let re = /<span class="field_prefix">[^\d]*(\d+)[^\d]*<\/span>/g;
            let prefix = re.exec(result.content), postfix = re.exec(result.content);
            if (!prefix || !postfix)
                throw new SomeError(ERR_TYPE.AUTH, 'Не найдены цифры - "{1}", "{2}"'.format(prefix, postfix));
            //this.debug(`${prefix}(\\d+)${postfix}  <=  ${this.login}`);
            re = new RegExp(prefix[1] + '(\\d+)' + postfix[1]);
            match = this.login.match(re);
            if (!match)
                throw new SomeError(ERR_TYPE.AUTH, `Несоответствие: "${prefix}(\\d+)${postfix}"  !=  "${this.login}"`);
            let code = match[1];

            match = result.content.match(/<form.* action="([^"]+)"/);
            if (!match)
                throw new SomeError(ERR_TYPE.AUTH, 'Проверка безопасности: не найдена форма');
            let validate_url = 'https://m.vk.com' + match[1];
            result = this.http.post(validate_url, { code });
        }
        if (result.content.contains('captcha')) {
            this.debug('Капча при авторизации');
            match = result.content.match(/<img id="captcha".* src="([^"]+)"/);
            if (!match)
                throw new SomeError(ERR_TYPE.AUTH, 'Капча при авторизации: не найден img');
            let src = match[1], captcha_sid = src.match('sid=(\\d+)')[1];
            let captchaUrl = 'https://m.vk.com' + src;
            //let code = this.captcha(captchaUrl);


            throw new SomeError(ERR_TYPE.AUTH, `Капча при авторизации пока не обрабатывается! ОТПРАВЬТЕ ЭТОТ АККАУНТ ${this.toString()} РАЗРАБОТЧИКУ, ЧТОБЫ ОБРАБОТАТЬ ДАННУЮ СИТУАЦИЮ`);

        }

        if (!checkAuth(result)) {
            //this.debug('post result: {1}', result);
            this.setStateLoginFailed();
            throw new SomeError(ERR_TYPE.AUTH, AccStateType.LOGIN_FAILED);
        }
    }

    saveUnfreezeData(newLogin, newPass) {
        this.unfreezeData = { oldLogin: this.login, oldPass: this.pass, newLogin, newPass, date: +new Date() };
        this.pass = newPass;
        this.save();
    }

    unfreezeCheck() {
        if (!this.unfreezeData)
            return false;
        const unfreezeTimeout = 24; // hours
        let totalDiff = moment().diff(this.unfreezeData.date, 'hours');
        //let lastCheckDiff = moment().diff(this.unfreezeData.lastCheckDate, 'hours');
        this.info(`С момента разморозки прошло ${totalDiff} часов`);
        if (totalDiff > unfreezeTimeout) {
            //let { login, pass } = this;
            this.login = this.unfreezeData.newLogin;
            this.pass = this.unfreezeData.newPass;
            //this.unfreezeData = null;
            this.info(`Попытка входа: ${this.login}:${this.pass}`);
            try {
                this.authTry(2, true);
                this.error('Успешно сменили логин/пароль');

                this.unfreezeData = null;
                this.save();
                return true;
            } catch (e) {
                this.error('Не удалось войти');

                [ this.login, this.pass ] = [ this.unfreezeData.oldLogin, this.unfreezeData.oldPass ];
                this.save();
                return false;
            }
        }
        return false;
    }

    authTry(tryCnt = 3, noUnfreeze = false) {
        try {
            this.auth();
            this.save();
        } catch (e) {
            if (tryCnt <= 1 && e.message == AccStateType.LOGIN_FAILED)
                if (!noUnfreeze && !this.unfreezeCheck())
                    throw e;
            if (tryCnt <= 1 || _.includes([AccStateType.BAN, AccStateType.FREEZE], e.message)) {
                this.save();
                throw e;
            }
            this.info('Ошибка входа в аккаунт {1}:{2}  Повторная попытка', this.login, this.pass);
            this.authTry(tryCnt - 1);
        }
    }

    updateToken(opts) {
        this.state = AccStateType.UPDATING;
        this.authTry();
        this.info('Состояние аккаунта: {1}', this.getState());
        //if (this.getState() != 'OK') return;
        opts = opts || {};
        _.defaults(opts, {
            client_id: '3682744', // Ipad
            scope: 'friends,status,messages,wall,photos,groups,video,audio' // https://vk.com/dev/permissions
        });
        //this.debug('OPTS = {1}', opts);

        var result = this.http.get('https://oauth.vk.com/authorize?client_id={1}&scope={2}&display=mobile&redirect_uri=blank.html&response_type=token'.format(opts.client_id, opts.scope));
        if (result.content.contains('https://login.vk.com/?act=grant_access')) {
            let grandAccessUrl = result.content.match(/<form method="post" action="([^"]+)/)[1];
            result = this.http.post(grandAccessUrl);
        }
        if (!result.content.contains('OAuth Blank'))
            throw new SomeError(ERR_TYPE.UPDATE_TOKEN, 'OAuth Blank not found');
        //this.debug('LOC: ' + result.href);
        this.token = result.href.match(/access_token=([^&]+)/);
        if (!this.token)
            throw new SomeError(ERR_TYPE.UPDATE_TOKEN, 'Not found access token');
        this.token = this.token[1];
        this.userId = result.href.match(/user_id=([^&]+)/)[1];

        //this.debug('token: {1}; user_id: {2}', this.token, this.userId);
        this.save();
        //AccManager.saveAll();
    }


    formatMsg(params) {
        //var msg = '[{1}]'.format(this.login);
        //if ('method' in params)
        //    msg += '[{1}]'.format(params.method);
        //return msg + ' ' + params.msg;
        var msg = 'method' in params ? '[{1}] '.format(params.method) : '';
        return msg + params.msg;
    }
    info(msg, ...params) {
        this.log.info(this.formatMsg({msg: msg}), ...params);
    }
    debug(msg, ...params) {
        this.log.debug(this.formatMsg({msg: msg}), ...params);
    }
    debugApi(method, msg, ...params) {
        this.log.debug(this.formatMsg({msg: msg, method: method}), ...params);
    }
    error(msg, ...params) {
        this.log.error(this.formatMsg({msg: msg}), ...params);
    }

    serialize() {
        var data = super.serialize();
        _.assign(data, {token: this.token, userId: this.userId, ownerId: this.ownerId, unfreeze: this.unfreezeData});
        return data;
    }
    deserialize(data) {
        super.deserialize(data);
        this.http = new HttpHelpers(this.log, this.userAgent, this.cookieJar, this.proxy);
    }

/*    static fromObject(obj) {
        var inst = new VkAcc();
        Object.assign(inst, obj);
        inst.log = new Logger();
        inst.proxy = Proxy.fromObject(inst.proxy);
        inst.http = new HttpHelpers(inst.log, inst.userAgent, inst.cookieJar, inst.proxy);
        return inst;
    }*/
}
