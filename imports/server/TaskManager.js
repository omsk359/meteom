import { ERR_TYPE } from "../common/SomeError";
import SomeError from "../common/SomeError";
import { log } from "../common/logger/Logger";
import { createTaskFromData } from "./helpers";
import taskClassByName from "./taskClassByName";
import * as _ from 'lodash';
import TaskData from '../common/collections/TaskData';
import Future from 'fibers/future';

// TODO: 
// https://www.npmjs.com/package/meteor-job

export default class TaskManager {
    constructor(logger = log) {
        this.log = logger;
        this.log.debug('11TaskManager ');
        this.deserialize(TaskData.find({}).fetch());
        _.each(this.tasks, task => {
            if (task.isStarted()/* || task.isWait()*/)
                Future.task(() => task.start());
        });
        taskManagerInstance = this;
    }
    create(taskName, params, ownerId) {
        var TaskClass = taskClassByName(taskName);
        var task = new TaskClass(ownerId, params);
        if (!TaskClass) throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Create: Unknown task type: {1}'.format(taskName));
        //task.ownerId = ownerId;
        var id = task.initDB();
        this.tasks.push(task);
        return id;
    }
    checkParams(params) {
        if (!_.get(params, 'accs.length'))
            throw new SomeError(ERR_TYPE.TASK_WRONG_PARAM, 'Empty accounts list');
    }
    start(id) {
        var task = this.taskById(id);
        Future.task(() => task.start());
        this.log.debug('[TaskManager] Starting task #{1}', id);
    }
    stop(id, ownerId) {
        var task = this.taskById(id);
        //if (!task)                   throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Stop: Task {1} not found!'.format(id));
        if (!task) {
            this.remove(id, ownerId);
            //throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Stop: Task {1} not found!'.format(id));
        }
        if (task.ownerId != ownerId) throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Stop: User {1} try to stop task {2} for user {3}'.format(ownerId, id, task.ownerId));
        task.stop();
    }
    remove(id, ownerId) {
        var task = this.taskById(id);
        if (!task) {
            TaskData.remove({ _id: id, ownerId });
            throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Remove: Task {1} not found!'.format(id));
        }
        if (task.ownerId != ownerId) throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Remove: User {1} try to remove task {2} for user {3}'.format(ownerId, id, task.ownerId));
        _.pull(this.tasks, task);
        task.remove();
    }
    pause(id, ownerId) {
        var task = this.taskById(id);
        if (!task)                   throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Pause: Task {1} not found!'.format(id));
        if (task.ownerId != ownerId) throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Pause: User {1} try to pause task {2} for user {3}'.format(ownerId, id, task.ownerId));
        Future.task(() => task.pause());
        //task.pause();
    }
    resume(id, ownerId) {
        var task = this.taskById(id);
        if (!task)                   throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Resume: Task {1} not found!'.format(id));
        if (task.ownerId != ownerId) throw new SomeError(ERR_TYPE.TASK_MANAGER, 'Resume: User {1} try to pause task {2} for user {3}'.format(ownerId, id, task.ownerId));
        //Future.task(() => task.resume());
        task.resume();
    }
    taskById(id) {
        return _.find(this.tasks, { _id: id });
    }
    static instance() {
        return taskManagerInstance || new TaskManager();
    }
    tasksByUser(ownerId) {
        return _.filter(this.tasks, task => task.ownerId == ownerId);
    }

    serialize() {
        return _.map(this.tasks, task => (
            { taskName: task.tagName, data: task.serialize() }
        ));
    }
    deserialize(data) {
        this.tasks = _.map(data, item => {
            try {
                return createTaskFromData(item);
            } catch (e) {
                this.log.error('TaskManager.deserialize: {1}', e.stack);
            }
        });
        this.tasks = _.compact(this.tasks);
    }
}
var taskManagerInstance;
