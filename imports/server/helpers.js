import { log } from "../common/logger/Logger";
import UserData from '../common/collections/UserData';
import taskClassByName from "./taskClassByName";

// import VkAcc from "./VkApi";
// import AccInfo from "../common/AccInfo";
//
// export function accClassByType(type) {
//     if (type == 'VK')
//         return VkAcc;
//     return AccInfo;
// }

export function setUserData(path, value, userId) {
    var set_obj = {};
    set_obj['data.' + path] = value;
    log.debug('setUserData: [{1}] = {2}', ' data.' + path, value);
    UserData.update(UserData.findOne({userId: userId})._id, { $set: set_obj });
}

export function getUserData(path, userId) {
    if (!path)
        return UserData.findOne({userId: userId}).data;
    var data = _.get(UserData.findOne({userId: userId}), 'data.' + path);
    return data;
}

export function wrapMeteorError(f) {
    try {
        return f();
    } catch (e) {
        log.error('taskMethod: {1}', e.message);
        log.error('stack: {1}', e.stack);
        throw new Meteor.Error(500, e.message, e.stack);
    }
}

export function createTaskFromData(data, parent) {
    var TaskClass = taskClassByName(data.taskName);
    var task = new TaskClass(parent, data.title);
    task.restoreState(data);
    //task.log = task.parent ? new SubTaskLogger(task) : new TaskLogger(task);
    return task;
}
