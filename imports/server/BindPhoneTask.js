import { MultiAccSubTask } from "./lib/MultiAcc";
import { ERR_TYPE } from "../common/SomeError";
import SomeError from "../common/SomeError";
import MultiAccTask from "./lib/MultiAcc";
import SimSms from "./SmsProviders/SimSms";
import SmsVk from "./SmsProviders/SmsVk";
import SmsLike from "./SmsProviders/SmsLike";
import SmsArea from "./SmsProviders/SmsArea";

export class BindPhoneTaskSingle extends MultiAccSubTask {
    constructor(parent, title) {
        super(parent.ownerId, 'BindPhoneTaskSingle', parent, title);
    }


    bindPhone(smsProvider) {
        let { acc } = this, { http } = acc;
        this.info('Привязка аккаунта у номеру');

        let phone = smsProvider.getPhone();
        //smsProvider.numberId = '117055';
        //let phone = '79152518009';
        this.info('Получен номер: {1}', phone);

        let response = http.getMatch('http://m.vk.com/settings', '/activation?act=validate');
        let match = response.content.match(/activation\?act=validate&hash=([^"]*)/);
        let hash = match[1], url = `http://m.vk.com/activation?act=validate_phone&hash=${hash}`;

        response = http.post(url, { phone, _ref: 'activation', _tstat: 'activation,0,0,288,55', act: 'validate_phone', hash });

        let resendUrl = `http://m.vk.com/activation?act=validate_resend&hash=${hash}`;
        let resendSms = () => http.get(resendUrl);
        this.debug('Повтор отправки смс: {1}', resendUrl);
        // code
        let code = smsProvider.getSms(resendSms);
        this.debug('Получен код: {1}', code);
        //let code = '56703';


        url = `http://m.vk.com/activation?act=validate_code&hash=${hash}`;
        response = http.post(url, { code, _ref: 'activation', _tstat: 'activation,0,0,297,51', act: 'validate_code', hash });

        this.setState({ oldLogin: acc.login, newLogin: phone });
        acc.login = phone;
        acc.save();
    }

    chooseSmsProvider() {
        let providers = this.smsProviders, sorted = _.sortBy(providers, provider => provider.cost());
        return _.find(sorted, provider => {
            let balance = provider.getBalance();
            this.info('[{1}] Баланс: {2}', provider.getName(), balance);
            if (balance < provider.cost()) {
                this.info('[{1}] Мало денег - пропуск', provider.getName());
                return false;
            }
            let availableCnts = provider.getAvailableCnts();
            this.info('[{1}] Доступных активаций: {2}', provider.getName(), availableCnts);
            return availableCnts;
        });
    }

    bindPhoneTry() {
        do {
            var provider = this.chooseSmsProvider();
            if (!provider) {
                this.info('Нет доступных сервисов');
                this.delay(60);
            }
        } while (!provider);

        let onLowBalance = self => {
            var actualProvider = this.chooseSmsProvider();
            if (actualProvider && actualProvider.getName() != self.getName())
                throw new SomeError(ERR_TYPE.SMS, 'CHANGE_PROVIDER');
        };
        provider.setOnLowBalanceCb(onLowBalance);
        provider.setOnNoAvailableNumbersCb(onLowBalance);

        this.info('Используется сервис {1}', provider.getName());
        let clone = provider.clone();
        try {
            this.bindPhone(clone);
        } catch (e) {
            if (e.name == ERR_TYPE.SMS && e.message == 'CHANGE_PROVIDER') {
                this.info('СМС не пришло. Отклоняем номер, получаем новый.');
                clone.reject();
                return this.bindPhoneTry();
            }
            throw e;
        }
    }

    start() {
        super.start();
        this.wrapErrHandlers(() => {
            let { acc } = this;
            let { useSimSms, useSmsVk, useSmsLike, useSmsArea, simSmsKey, smsVkKey, smsLikeKey, smsAreaKey } = this.params;

            this.smsProviders = [];
            if (useSimSms)  this.smsProviders.push(new SimSms(simSmsKey, this.log));
            if (useSmsVk)   this.smsProviders.push(new SmsVk(smsVkKey, this.log));
            if (useSmsLike) this.smsProviders.push(new SmsLike(smsLikeKey, this.log));
            if (useSmsArea) this.smsProviders.push(new SmsArea(smsAreaKey, this.log));

            this.info('Проверка состояния аккаунта');
            acc.authTry();
            this.bindPhoneTry();
            this.info('Аккаунт привязан к номеру {1}. Тест.', acc.login);
            acc.authTry();
        });
    }
}


export default class BindPhoneTask extends MultiAccTask {
    constructor(ownerId, params) {
        super(ownerId, 'BindPhoneTask', params, BindPhoneTaskSingle);
        this.shiftDelay = [5, 15];
    }

    checkSmsProvidersInfo() {
        let { useSimSms, useSmsVk, useSmsLike, useSmsArea, simSmsKey, smsVkKey, smsLikeKey, smsAreaKey } = this.state.params, smsProviders = [];
        if (simSmsKey/* && useSimSms*/)
            smsProviders.push(new SimSms(simSmsKey, this.log));
        if (smsVkKey /*&& useSmsVk*/)
            smsProviders.push(new SmsVk(smsVkKey, this.log));
        if (smsLikeKey /*&& useSmsLike*/)
            smsProviders.push(new SmsLike(smsLikeKey, this.log));
        if (smsAreaKey /*&& useSmsArea*/)
            smsProviders.push(new SmsArea(smsAreaKey, this.log));
        _.each(smsProviders, smsProvider => this.setState({
            smsProviders: {
                [smsProvider.getName()]: {
                    balance: smsProvider.getBalance(),
                    availableCnts: smsProvider.getAvailableCnts()
                }
            }
        }));
    }
    startTimers() {
        this.checkSmsProvidersInfo();
        this.checkIntervalId = Meteor.setInterval(Meteor.bindEnvironment(this.checkSmsProvidersInfo.bind(this)), 20 * 1000);
    }
    stopTimers() {
        Meteor.clearInterval(this.checkIntervalId);
    }

    startNow() {
        try {
            this.tasks.forEach(task => task.params = this.state.params);
            super.startNow();

            this.waitChildrenAndPrint();
        } catch (e) {
            this.error('>>>>>>>>>>>>>>>> TASK EXCEPTION: {1}', e.stack);
            this.finished(e.message);
        }
    }
}
