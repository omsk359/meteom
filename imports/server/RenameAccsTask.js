import { MultiAccSubTask } from "./lib/MultiAcc";
import { ERR_TYPE } from "../common/SomeError";
import MultiAccTask from "./lib/MultiAcc";

export class RenameAccsTaskSingle extends MultiAccSubTask {
    constructor(parent, title) {
        super(parent.ownerId, 'RenameAccsTaskSingle', parent, title);
    }

    clearPersonalInfo(sleep) {
        let { acc } = this;
        this.info('Удаление контактов');
        try {
            acc.auth();
        } catch (e) {
            if (e.name == ERR_TYPE.AUTH)
                acc.auth();
        } finally {
            acc.save();
        }
        let response = acc.http.get('http://vk.com/edit?act=contacts');
        let match = response.content.match(/saveHash":"(.*?)"/);
        if (match) {
            acc.http.post('http://vk.com/al_profileEdit.php', {
                act: 'a_save_contacts', al: 1, hash: match[1],
                city: '', country: 0, email: '', home: '', mobile: '',
                privacy_email: 0, privacy_home: 0, privacy_mobile: 0, skype: '', website: ''
            });
            sleep();
        } else
            this.error('saveHash не найден!');

        this.info('Удаление интересов');
        response = acc.http.get('http://vk.com/edit?act=interests');
        match = response.content.match(/hash":"(.*?)"/);
        if (match) {
            acc.http.post('http://vk.com/al_profileEdit.php', {
                act: 'a_save_interests', al: 1, hash: match[1],
                about: '', activities: '', books: '', games: '', interests: '',
                movies: '', music: '', quotes: '', tv: ''
            });
            sleep();
        } else
            this.error('hash не найден!');

        this.info('Удаление среднего образования');
        response = acc.http.get('http://vk.com/edit?act=education');
        match = response.content.match(/id="hash" value="([^"]+)"/);
        if (match) {
            acc.http.post('http://vk.com/al_profileEdit.php', {
                act: 'a_save_education_school', al: 1, hash: match[1],
                school0id: 1, school0deleted: 1
            });
            sleep();
        } else
            this.error('hash не найден!');

        this.info('Удаление высшего образования');
        response = acc.http.get('http://vk.com/edit?act=education');
        match = response.content.match(/id="hash" value="([^"]+)"/);
        if (match) {
            acc.http.post('http://vk.com/al_profileEdit.php', {
                act: 'a_save_education_uni', al: 1, hash: match[1],
                primary_unideleted: 1
            });
            sleep();
        } else
            this.error('hash не найден!');

        this.info('Очистка карьеры');
        response = acc.http.get('http://vk.com/edit?act=career');
        match = response.content.match(/id="hash" value="([^"]+)"/);
        if (match) {
            acc.http.post('http://vk.com/al_profileEdit.php', {
                act: 'a_save_career', al: 1, hash: match[1],
                deleted0: 1, id0: 1
            });
            sleep();
        } else
            this.error('hash не найден!');

        this.info('Очистка военной службы');
        response = acc.http.get('http://vk.com/edit?act=military');
        match = response.content.match(/id="hash" value="([^"]+)"/);
        if (match) {
            let hash = match[1];
            match = response.content.match(/cur.militaries = ([^;]+)/);
            let militaries = JSON.parse(match[1]), milParams = { act: 'a_save_military', al: 1, hash };
            _.each(militaries, (mil, i) => _.assign(milParams, { [`deleted${i}`]: 1, [`id${i}`]: mil.id }));
            acc.http.post('http://vk.com/al_profileEdit.php', milParams);
            sleep();
        } else
            this.error('hash не найден!');

        this.info('Очистка жизненной позиции');
        response = acc.http.get('http://vk.com/edit?act=personal');
        match = response.content.match(/hash":"(.*?)"/);
        if (match) {
            acc.http.post('http://vk.com/al_profileEdit.php', {
                act: 'a_save_personal', al: 1, hash: match[1],
                alcohol: 0, smoking: 0, inspired_by: '', life_priority: 0,
                people_priority: 0, political: 0, religion: '', religion_custom: ''
            });
            sleep();
        } else
            this.error('hash не найден!');
    }

    clear(params, delay = [0, 0]) {
        let { acc } = this;
        let sleep = () => {
            let sec = _.random(...delay);
            if (sec) this.delay(sec);
        };
        if (params.avatars) {
            this.info('Очистка аваторок');
            let photos = acc.photos().get({ album_id: 'profile' }), cnt = photos.items.length, okCnt = 0;
            this.info('Получено {1}/{2} фото', cnt, photos.count);
            for (let i = 0; i < cnt; i++) {
                let id = photos.items[i].id, result = acc.photos().delete(id);
                this.info('[{3}/{4}] Удаление аватарки #{1} - {2}', id, result === 1 ? 'OK' : result, i + 1, cnt);
                okCnt += result === 1;
                sleep();
            }
            this.info('Успешно удалили {1}/{2} аватарок', okCnt, cnt);
        }
        if (params.wall) {
            this.info('Очистка стены');
            let posts = acc.wall().get(), cnt = posts.items.length, okCnt = 0;
            this.info('Получено {1}/{2} постов', cnt, posts.count);
            for (let i = 0; i < cnt; i++) {
                let id = posts.items[i].id, result = acc.wall().delete(id);
                this.info('[{3}/{4}] Удаление поста #{1} - {2}', id, result === 1 ? 'OK' : result, i + 1, cnt);
                okCnt += result === 1;
                sleep();
            }
            this.info('Успешно удалили {1}/{2} постов', okCnt, cnt);
        }
        if (params.messages) {
            const chunkSize = 500;
            this.info('Удаление сообщений по {1} шт.'.format(chunkSize));
            let messages = acc.messages().get(), cnt = messages.items.length, okCnt = 0;
            this.info('Получено {1}/{2} сообщений', cnt, messages.count);
            let ids = _.map(messages.items, 'id'), chunks = _.chunk(ids, chunkSize);
            for (let i = 0; i < chunks.length; i++) {
                let chunk = chunks[i], result = acc.messages().delete(chunk);
                this.info('[{3}-{4}/{5}] Удаление {1} сообщений -> {2} успешно', chunk.length, _.countBy(result)[1] || 0, i * chunkSize, i * chunkSize + chunk.length, cnt);
                okCnt += _.countBy(result)[1] || 0;
                sleep();
            }
            this.info('Успешно удалили {1}/{2} сообщений', okCnt, cnt);
        }
        if (params.info) {
            this.info('Удаление личной информации');
            this.clearPersonalInfo(sleep);
        }
        if (params.friends) {
            this.info('Удаляем друзей');
            let friends = acc.friends().get(), cnt = friends.items.length, okCnt = 0;
            this.info('Получено {1}/{2} друзей', cnt, friends.count);
            for (let i = 0; i < cnt; i++) {
                let id = friends.items[i], result = acc.friends().delete(id);
                let ok = !!result.success, status, resultMsg;
                if (ok)
                    resultMsg = 'Друг id{1} удален.'.format(id);
                else
                    resultMsg = 'ОШИБКА: {1}'.format(result);
                this.info('[{2}/{3}] {1}', resultMsg, i + 1, cnt);
                okCnt += ok;
                sleep();
            }
        }
        if (params.friendRequests) {
            this.info('Отменяем заявки в друзья');
            let requests = acc.friends().getRequests(), cnt = requests.items.length, okCnt = 0;
            this.info('Получено {1}/{2} заявок', cnt, requests.count);
            for (let i = 0; i < cnt; i++) {
                let id = requests.items[i], result = acc.friends().delete(id);
                let ok = !!result.success, status, resultMsg;
                if (ok)
                    resultMsg = 'Отменена исходящая заявка id{1}'.format(id);
                else
                    resultMsg = 'ОШИБКА: {1}'.format(result);
                this.info('[{2}/{3}] {1}', resultMsg, i + 1, cnt);
                okCnt += ok;
                sleep();
            }
            this.info('Успешно отменили {1}/{2} заявок', okCnt, cnt);
        }
        if (params.leaveGroups) {
            this.info('Выходим из групп');
            let requests = acc.groups().get(), cnt = requests.items.length, okCnt = 0;
            this.info('Получено {1}/{2} групп', cnt, requests.count);
            for (let i = 0; i < cnt; i++) {
                let id = requests.items[i], result = acc.groups().leave(id);
                this.info('[{3}/{4}] Выход из group{1} - {2}', id, result === 1 ? 'OK' : result, i + 1, cnt);
                okCnt += result === 1;
                sleep();
            }
            this.info('Успешно покинули {1}/{2} групп', okCnt, cnt);
        }
        if (params.video) {
            this.info('Удаляем видеозаписи');
            let requests = acc.video().get(), cnt = requests.items.length, okCnt = 0;
            this.info('Получено {1}/{2} видеозаписей', cnt, requests.count);
            for (let i = 0; i < cnt; i++) {
                let { id, owner_id } = requests.items[i], result = acc.video().delete(id, owner_id, acc.getUserId());
                this.info('[{3}/{4}] Удаление видео #{1} - {2}', id, result === 1 ? 'OK' : result, i + 1, cnt);
                okCnt += result === 1;
                sleep();
            }
            this.info('Успешно удалили {1}/{2} видео', okCnt, cnt);
        }
        if (params.photoAlbums) {
            this.info('Удаляем фотоальбомы');
            let requests = acc.photos().getAlbums(), cnt = requests.items.length, okCnt = 0;
            this.info('Получено {1}/{2} фотоальбомов', cnt, requests.count);
            for (let i = 0; i < cnt; i++) {
                let { id } = requests.items[i], result = acc.photos().deleteAlbum(id);
                this.info('[{3}/{4}] Удаление фотоальбома #{1} - {2}', id, result === 1 ? 'OK' : result, i + 1, cnt);
                okCnt += result === 1;
                sleep();
            }
            this.info('Успешно удалили {1}/{2} фотоальбомов', okCnt, cnt);
        }
        if (params.photo) {
            this.info('Удаляем фото');
            let requests = acc.photos().getAll(), cnt = requests.items.length, okCnt = 0;
            this.info('Получено {1}/{2} фото', cnt, requests.count);
            for (let i = 0; i < cnt; i++) {
                let { id } = requests.items[i], result = acc.photos().delete(id);
                this.info('[{3}/{4}] Удаление фото #{1} - {2}', id, result === 1 ? 'OK' : result, i + 1, cnt);
                okCnt += result === 1;
                sleep();
            }
            this.info('Успешно удалили {1}/{2} фото', okCnt, cnt);
        }
        if (params.music) {
            this.info('Удаляем аудиозаписи');
            let requests = acc.audio().get(), cnt = requests.items.length, okCnt = 0;
            this.info('Получено {1}/{2} аудиозаписей', cnt, requests.count);
            for (let i = 0; i < cnt; i++) {
                let { id, owner_id } = requests.items[i], result = acc.audio().delete(id, owner_id);
                this.info('[{3}/{4}] Удаление аудиозаписи #{1} - {2}', id, result === 1 ? 'OK' : result, i + 1, cnt);
                okCnt += result === 1;
                sleep();
            }
            this.info('Успешно удалили {1}/{2} аудиозаписей', okCnt, cnt);
        }
        if (params.groupInvites) {
            this.info('Отклоняем приглашения в группы');
            let requests = acc.groups().getInvites(), cnt = requests.items.length, okCnt = 0;
            this.info('Получено {1}/{2} приглашений', cnt, requests.count);
            for (let i = 0; i < cnt; i++) {
                let { id } = requests.items[i], result = acc.groups().leave(id);
                this.info('[{3}/{4}] Отмена приглашения #{1} - {2}', id, result === 1 ? 'OK' : result, i + 1, cnt);
                okCnt += result === 1;
                sleep();
            }
            this.info('Успешно отменили {1}/{2} приглашений в группы', okCnt, cnt);
        }
    }

    start() {
        super.start();
        this.wrapErrHandlers(() => {
            let { acc } = this;
            let p = this.params, task_i = this.parent.tasks.indexOf(this);
            let params = {};
            if (p.changeName) {
                params.first_name = p.firstNames[task_i % p.firstNames.length];
                params.last_name = p.shuffleNames ? _.sample(p.lastNames) : p.lastNames[task_i % p.lastNames.length];
                params.maiden_name = '';
            }
            if (p.changeAge) {
                let age = _.random(p.ageFrom, p.ageTo), year = new Date().getFullYear() - age;
                params.bdate = '{1}.{2}.{3}'.format(_.random(1, 28), _.random(1, 12), year);
            }
            if (p.changeBdateVisibility)    params.bdate_visibility = p.bdateVisibility;
            if (p.changeGender)             params.sex = p.gender;
            if (p.changeRelation)           params.relation = p.relation;
            if (p.changeHomeTown)           params.home_town = p.homeTown;
            if (p.changeStatus)             params.status = p.status;

            if (!_.isEmpty(params)) {
                let result = acc.account().saveProfileInfo(params);
                this.debug('result: {1}', result);
                if (_.has(result, 'name_request'))
                    this.info('Успешно изменили');
                else
                    this.info('Ошибка: {1}', result);
            }

            if (p.clear) {
                let { enable, range } = p.clearParams.delay;
                if (!enable)
                    range = undefined;
                this.clear(p.clearParams, range);
            }
        });
    }
}

export default class RenameAccsTask extends MultiAccTask {
    constructor(ownerId, params) {
        super(ownerId, 'RenameAccsTask', params, RenameAccsTaskSingle);
    }

    startNow() {
        try {
            this.tasks.forEach(task => task.params = this.state.params);
            super.startNow();

            this.waitChildrenAndPrint();
        } catch (e) {
            this.error('>>>>>>>>>>>>>>>> TASK EXCEPTION: {1}', e.stack);
        }
    }
}