import AccInfo from "../common/AccInfo";
import { AccOpTypes } from "../common/types";
import { getUserData } from "./helpers";
import UserAccs from '../common/collections/UserAccs';
import VkAcc from "./VkApi";

export default class AccManager {
    constructor(userId) {
        this.userId = userId || Meteor.userId();
    }
    getAccs(ids) {
        let accsData = UserAccs.find({ _id: {$in: ids}}).fetch();
        let ruCaptchaKey = this.getRuCaptchaKey();
        return _.map(accsData, accData => {
            let acc = deserializeAcc(accData);
            acc.ruCaptchaKey = ruCaptchaKey;
            return acc;
        });
    }
    getRuCaptchaKey() {
        return getUserData('userSettings.ruCaptchaKey', this.userId);
    }
    static saveAcc(acc) {
        UserAccs.update(acc._id, acc.serialize());
    }
    //addAccs(accsDataFromUser) {}
    //updateAccs(accsDataFromUser) {}
    //removeAccs(accsDataFromUser) {}
};

export function updateAccs(accsData, userId = Meteor.userId()) {
    let results = {};
    _.each(accsData, accData => {
        accData.ownerId = userId;
        let { numberAffected, insertedId } = UserAccs.upsert({ _id: accData._id, ownerId: userId }, accData);
        if (insertedId)
            results[accData.login] = { opType: AccOpTypes.ADD, state: 'OK', _id: insertedId };
        else
            results[accData.login] = { opType: AccOpTypes.UPDATE, state: 'OK', _id: accData._id };
    });
    return results;
}

export function deserializeAcc(data) {
    var accClass = accClassByType(data.type);
    var acc = new accClass();
    acc.deserialize(data);
    return acc;
}
export function accClassByType(type) {
    if (type == 'VK')
        return VkAcc;
    return AccInfo;
}
