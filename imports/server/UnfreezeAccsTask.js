import { MultiAccSubTask } from "./lib/MultiAcc";
import SomeError from "../common/SomeError";
import { ERR_TYPE } from "../common/SomeError";
import { AccStateType } from "../common/AccInfo";
import MultiAccTask from "./lib/MultiAcc";
import SmsArea from "./SmsProviders/SmsArea";
import SmsLike from "./SmsProviders/SmsLike";
import SmsVk from "./SmsProviders/SmsVk";
import SimSms from "./SmsProviders/SimSms";

export class UnfreezeAccsTaskSingle extends MultiAccSubTask {
    constructor(parent, title) {
        super(parent.ownerId, 'UnfreezeAccsTaskSingle', parent, title);
    }


    unfreeze(smsProvider) {
        let { acc } = this, { http } = acc;
        this.info('Разморозка аккаунта');

        let number = smsProvider.getPhone();
        this.info('Получен номер: {1}', number);
        //let number = '79774534970';

        let checkErrOnPage = response => {
            if (!response.content.contains('service_msg_warning'))
                return;
            let match = response.content.match(/service_msg_warning.*?>(.*?)<\/div>/);
            if (match) {
                if (match[1].contains('Этот номер недавно использовался'))
                    throw new SomeError(ERR_TYPE.SMS, 'CHANGE_PROVIDER');
                throw new SomeError(ERR_TYPE.TASK_LOGIC, match[1]);
            }
            throw new SomeError(ERR_TYPE.TASK_LOGIC, 'service_msg_warning??');
        };

        //let response = acc.http.get('http://m.vk.com/');
        let response = http.postMatch('http://m.vk.com/login?act=blocked&change_phone', 'blocked_phone', { _nlm: 1, _ref: 'login', act: 'blocked', change_phone: '' });
        checkErrOnPage(response);
        let match = response.content.match(/act=blocked_phone&hash=(.*?)"/);
        let hash = match[1], phone = /*'+7' + */number;
        response = http.post(`http://m.vk.com/login?act=blocked_phone&hash=${hash}`, {
            _nlm: 1, _ref: 'login', act: 'blocked_phone', hash, phone
        });
        checkErrOnPage(response);
        response = http.post(`http://m.vk.com/login?act=blocked_phone&phone=${phone}&sure=1&hash=${hash}`, {
            _nlm: 1, _ref: 'login', act: 'blocked_phone', hash, phone
        });
        checkErrOnPage(response);
        let resendUrl = `http://m.vk.com/login?act=blocked_resend&hash=${hash}`;
        let resendSms = () => http.get(resendUrl);
        this.debug('Повтор отправки смс: {1}', resendUrl);
        // code
        let code = smsProvider.getSms(resendSms);
        this.debug('Получен код: {1}', code);
        //let code = '60074';
        response = http.postMatch(`http://m.vk.com/login?act=blocked_check&hash=${hash}`, 'blocked_done', {code});
        checkErrOnPage(response);
        match = response.content.match(/act=blocked_done&hash=(.*?)"/);
        let pass = acc.pass.replace(/^(\d)0/, (sub, n) => `${+n + 1}0`);
        if (pass == acc.pass)
            pass = '10' + pass;
        response = http.post(`http://m.vk.com/login?act=blocked_done&hash=${match[1]}`, {email: phone, pass: pass});
        checkErrOnPage(response);

        acc.saveUnfreezeData(phone.replace('+', ''), pass);
        this.setState(acc.unfreezeData);
    }

    chooseSmsProvider() {
        let providers = this.smsProviders, sorted = _.sortBy(providers, provider => provider.cost());
        return _.find(sorted, provider => {
            let balance = provider.getBalance();
            this.info('[{1}] Баланс: {2}', provider.getName(), balance);
            if (balance < provider.cost()) {
                this.info('[{1}] Мало денег - пропуск', provider.getName());
                return false;
            }
            let availableCnts = provider.getAvailableCnts();
            this.info('[{1}] Доступных активаций: {2}', provider.getName(), availableCnts);
            return availableCnts;
        });
    }

    unfreezeTry() {
        do {
            var provider = this.chooseSmsProvider();
            if (!provider) {
                this.info('Нет доступных сервисов');
                this.delay(60);
            }
        } while (!provider);

        let onLowBalance = self => {
            var actualProvider = this.chooseSmsProvider();
            if (actualProvider && actualProvider.getName() != self.getName())
                throw new SomeError(ERR_TYPE.SMS, 'CHANGE_PROVIDER');
        };
        provider.setOnLowBalanceCb(onLowBalance);
        provider.setOnNoAvailableNumbersCb(onLowBalance);

        this.info('Используется сервис {1}', provider.getName());
        let clone = provider.clone();
        try {
            this.unfreeze(clone);
        } catch (e) {
            if (e.name == ERR_TYPE.SMS && e.message == 'CHANGE_PROVIDER') {
                this.info('СМС не пришло. Отклоняем номер, получаем новый.');
                clone.reject();
                return this.unfreezeTry();
            }
            throw e;
        }
    }

    start() {
        super.start();
        this.wrapErrHandlers(() => {
            let { acc } = this;
            let { useSimSms, useSmsVk, useSmsLike, useSmsArea, simSmsKey, smsVkKey, smsLikeKey, smsAreaKey } = this.params;

            this.smsProviders = [];
            if (useSimSms)  this.smsProviders.push(new SimSms(simSmsKey, this.log));
            if (useSmsVk)   this.smsProviders.push(new SmsVk(smsVkKey, this.log));
            if (useSmsLike) this.smsProviders.push(new SmsLike(smsLikeKey, this.log));
            if (useSmsArea) this.smsProviders.push(new SmsArea(smsAreaKey, this.log));

            this.info('Проверка состояния аккаунта');
            try {
                return acc.authTry();
            } catch (e) {
                if (e.name == ERR_TYPE.AUTH && e.message == AccStateType.FREEZE) {
                    this.unfreezeTry();
                    this.info('Аккаунт разморожен. Тест.');
                    return acc.authTry();
                }
                throw e;
            }
        });
    }
}





export default class UnfreezeAccsTask extends MultiAccTask {
    constructor(ownerId, params) {
        super(ownerId, 'UnfreezeAccsTask', params, UnfreezeAccsTaskSingle);
        this.shiftDelay = [5, 15];
    }

    checkSmsProvidersInfo() {
        let { useSimSms, useSmsVk, useSmsLike, useSmsArea, simSmsKey, smsVkKey, smsLikeKey, smsAreaKey } = this.state.params, smsProviders = [];
        if (simSmsKey/* && useSimSms*/)
            smsProviders.push(new SimSms(simSmsKey, this.log));
        if (smsVkKey /*&& useSmsVk*/)
            smsProviders.push(new SmsVk(smsVkKey, this.log));
        if (smsLikeKey /*&& useSmsLike*/)
            smsProviders.push(new SmsLike(smsLikeKey, this.log));
        if (smsAreaKey /*&& useSmsArea*/)
            smsProviders.push(new SmsArea(smsAreaKey, this.log));
        _.each(smsProviders, smsProvider => this.setState({
            smsProviders: {
                [smsProvider.getName()]: {
                    balance: smsProvider.getBalance(),
                    availableCnts: smsProvider.getAvailableCnts()
                }
            }
        }));
    }
    startTimers() {
        this.checkSmsProvidersInfo();
        this.checkIntervalId = Meteor.setInterval(Meteor.bindEnvironment(this.checkSmsProvidersInfo.bind(this)), 20 * 1000);
    }
    stopTimers() {
        Meteor.clearInterval(this.checkIntervalId);
    }

    startNow() {
        try {
            this.tasks.forEach(task => task.params = this.state.params);
            super.startNow();

            this.waitChildrenAndPrint();
        } catch (e) {
            this.error('>>>>>>>>>>>>>>>> TASK EXCEPTION: {1}', e.stack);
            this.finished(e.message);
        }
    }
}

//Meteor.startup(() => {
//    if (TaskData.find({taskName: 'UnfreezeAccsTask'}).count() === 5) {
//        //let accs = AccInfo.fromString(`79629602831:ec0e9e8a9a 107.179.66.227:60099|USA2:15122015`, '4o8cEwvB7XimbrEgS');
//        let accDB = UserAccs.findOne({login: '79509022233'/*, ownerId: 'uvBQZcGa2zX66aqXD'*//*'4o8cEwvB7XimbrEgS'*/});
//        var taskId = Meteor.call('createTask', 'UnfreezeAccsTask', { accs: [accDB._id], useSimsms: true, simsmsKey: 'PtjzIvVClSKRehsnDX9mbkRt6vhsld' });
//        //Meteor.call('startTask', taskId);
//    }
//    //let sms = new SmsVk('CdeA86d0B320A9485CC1AB925603d4AB');
//    //let balance = sms.getBalance();
//    //let cnts = sms.getAvailableCnts();
//    //log.info(`balance = ${balance}; cnts: ${cnts}`);
//    //let phone = sms.getPhone();
//    //;
//});
