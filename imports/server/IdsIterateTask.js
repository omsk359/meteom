import { MultiAccSubTask } from "./lib/MultiAcc";
import { IdActions } from "./lib/IdsIterateHelpers";
import { ERR_TYPE } from "../common/SomeError";
import MultiAccTask from "./lib/MultiAcc";
import { ApplyIdsQueue } from "./lib/IdsIterateHelpers";
export class IdsIterateTaskSingle extends MultiAccSubTask {
    constructor(parent, title) {
        super(parent.ownerId, 'IdsIterateTaskSingle', parent, title);
    }

    start() {
        super.start();
        this.wrapErrHandlers(() => {
            let { acc, idsQueue } = this;
            let { actions, onActions } = this.params;
            actions = _.transform(actions, (actions, aType) => actions[aType] = IdActions[aType], {});
            let userId;//, ___idsOkCnts___ = this.state.idsResult = _.transform(actions, (cnts, a, aType) => cnts[aType] = {ok: 0, fail: 0});

            this.setState({
                //idsOkCnts: _.mapValues(actions, () => ({ok: 0, fail: 0})),
                idsOkCnts: _.mapValues(actions, (a, aType) => _.defaults(_.get(this.state, `idsOkCnts[${aType}]`) || {}, {ok: 0, fail: 0})),
                idsOkCntsMax: _.mapValues(actions, (a, aType) => _.get(this.params[aType], 'idsOkCntMax'))
            });
            //this.setState({ idsOkCnts: _.mapValues(actions, () => ({ok: 0, fail: 0})) });
            //this.setState({ idsOkCntsMax: _.mapValues(actions, (a, aType) => _.get(this.params[aType], 'idsOkCntMax')) });
            let { idsOkCnts, idsOkCntsMax } = this.state;

            //let idsOkCntsMax = _.mapValues(actions, (a, aType) => _.get(this.params[aType], 'idsOkCntMax'));
            //let idsOkCntsMax = _.transform(actions, (maxCnts, a, aType) => maxCnts[aType] = _.get(this.params[aType], 'idsOkCntMax'));
            let defaultExitCheck = () => _.some(idsOkCnts, (cnt, aType) => idsOkCntsMax[aType] && (idsOkCnts[aType].ok >= idsOkCntsMax[aType]));
            let checkExit = this.params.checkExit || defaultExitCheck;

            let onAction = (results, userId) => {
                _.each(onActions, (actionsRule, aTypes) => {
                    aTypes = aTypes.split(',');
                    this.ok = _.every(aTypes, aType => results[aType]);
                    let fail = _.every(aTypes, aType => !results[aType]);
                    this.currActionTypes = aTypes;
                    if (this.ok)
                        _.each(actionsRule.onOK, actionName => IdActions[actionName](this, userId));
                    else if (fail)
                        _.each(actionsRule.onFail, actionName => IdActions[actionName](this, userId))
                });
            };

            while (!checkExit(idsOkCnts, idsOkCntsMax) && (userId = idsQueue.nextId(this))) {
                let results = {};
                _.each(actions, (action, aType) => {
                    try {
                        this.currActionType = aType;
                        let result = action(this, userId, idsOkCnts[aType]);
                        idsOkCnts[aType][result.ok ? 'ok' : 'fail']++;

                        this.info('[{2}/{3}][{4}] {1}', result.resultMsg || 'OK', idsOkCnts[aType].ok, idsOkCntsMax && idsOkCntsMax[aType] ? idsOkCntsMax[aType] : '??', aType);
                        results[aType] = result.ok;
                    } catch (e) {
                        if (e.name != ERR_TYPE.VK_API) throw e;
                        if (e.message == 'Permission to perform this action is denied')
                            this.info('id{1} -> Доступ запрещен', userId);
                        else
                            throw e;
                        idsOkCnts[aType]['fail']++;
                        results[aType] = false;
                    }
                });
                this.setState({ idsOkCnts });
                onAction(results, userId);
                //let ok = _.some(results), maxCnt = _.max(_.map(idsOkCnts, okCnt => okCnt.ok));
                //if (ok)
                //    this.delay(this.parent.getDelay(maxCnt));
            }
            if (checkExit(idsOkCnts, idsOkCntsMax))
                this.info('Достигнуто макс. ограничение - {1}', idsOkCntsMax);
        });
    }

    finished(exitState) {
        super.finished(exitState);
        this.idsQueue.getIdsProvider().reset();
    }
}


/*
     -- IdsIterateTask --

    actions = ['invite', 'like']

    params = {
        idsQueue: 'friends',
        ids = [id1, id2, ...],
        filter: ['online', 'inviterDB'],
        invite: {
            idsOkCntsMax: 100,
            msg: 'Hi!',
        },
        like: {
            idsOkCntsMax: 50,
        },
        delays: {},
        exitCheck: ({aType: cnt1, aType2: ...}) => false
        onActions: { 'invite' -> { onOk: ['addToInviteDB'], onFail: [] }, 'sendMsg,like' -> { onOk: [] } }
    }

 ->

 -- SubTask.invite --

 this.idsQueue
 this.params = this.parent.params;
 this.onAction = ({ invite: true, like: false }) => {}

 this.actions = {
    invite: IdActions['invite']
 }

 1. ids -> inviteDB, lastSeen
 invite fr

 2. friends -> online, notExistOutMsg
 send msg, like

 */

export default class IdsIterateTask extends MultiAccTask {
    constructor(ownerId, params) {
        super(ownerId, 'IdsIterateTask', params, IdsIterateTaskSingle);
    }

    startNow() {
        try {
            _.defaults(this.state, { okIds: [], failIds: [] });
            this.tasks.forEach(task => task.params = this.state.params);

            ApplyIdsQueue[this.state.params.idsQueue](this);

            super.startNow();
            this.waitChildrenAndPrint();
        } catch (e) {
            this.error('>>>>>>>>>>>>>>>> TASK EXCEPTION: {1}', e.stack);
            throw e;
        }
    }

    finished(exitState) {
        //_.each(this.state.idsResult, ({ok, fail = [11]}, aType) => {
        //    ids  = ids || [];
        //    this.info('[{3}] Обработано ID: {1}; Ошибок: {2}', ok.length, fail.length, aType);
        //    this.debug('[{3}] OK: {1}; Ошибки: {2}', ok.join(', '), fail.join(', '), aType);
        //});
        //this.info('Обработано ID: {1}; Ошибок: {2}', this.state.okIds.length, this.state.failIds.length);
        //this.debug('OK: {1}; Ошибки: {2}', this.state.okIds.join(', '), this.state.failIds.join(', '));
        super.finished(exitState);
    }
}

/*
Meteor.startup(() => {
    if (TaskData.find({taskName: 'IdsIterateTask'}).count() == 10) {
        let ids = getIds();
        let accs = [ new VkAcc('48782353294:O0vTuhJfYgnuVn7Q55mm  173.234.169.99:8080|petrov482@meta.ua:7EG5p4au2p') ];
        //let accs = AccManager.parseFromString(`48782353294:O0vTuhJfYgnuVn7Q55mm  173.234.169.99:8080|petrov482@meta.ua:7EG5p4au2p`, '4o8cEwvB7XimbrEgS' || '6hzcZQQk2PbvdDPpp');
        accs = accs.map(acc => acc.serialize());
        var taskId = Meteor.call('createTask', 'IdsIterateTask', {
            accs: accs,
            idsQueue: 'ids',
            ids: ids,
            delays: {0: [10, 10], 1: [10, 10]},
            actions: ['nope'],
            //invite: {
            //    idsOkCntMax: 4
            //},
            nope: {
                idsOkCntMax: 2
            },
            filters: ['notInInviteDB'],
            //filters: ['online', 'notInInviteDB'],
            onActions: {
                invite: { onOk: ['addToInviteDB'] },
                nope: { onOk: ['addToInviteDB'] }
            }
        });
        Meteor.call('startTask', taskId);
    } else {
        //let taskId = TaskData.findOne({taskName: 'IdsIterateTask'})._id;
        //Meteor.call('startTask', taskId);
    }
    let acc = new VkAcc('48782353294:O0vTuhJfYgnuVn7Q55mm  173.234.169.99:8080|petrov482@meta.ua:7EG5p4au2p');
    acc.save();
    log.debug('acc: {1}', UserAccs.find({}).count());
    //AccInfo.captcha('https://confluence.atlassian.com/doc/files/216957808/captcha.png');
});


function getIds() {
    var idsStr =
        `806425
8786525
8898055
10961355
12172760
13186019
`;
    var ids = _.compact(idsStr.split('\n').map(_.trim).map(_.ary(parseInt, 1)));
    return ids;
}
    */