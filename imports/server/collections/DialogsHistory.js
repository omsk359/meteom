import DialogsHistory_DB from '../../common/collections/DialogsHistory_DB';

Meteor.publish('Dialogs', function() {
    return DialogsHistory_DB.find({ ownerId: this.userId });
});
