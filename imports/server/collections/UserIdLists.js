import UserIdLists_DB from '../../common/collections/UserIdLists_DB';
import { wrapMeteorError } from "../helpers";

Meteor.publish('UserIdLists', function() {
    return UserIdLists_DB.find({ ownerId: this.userId });
});

//Meteor.publish('UserIdListsNames', function() {
//    return UserIdLists_DB.find({}, { fields: {ids: 0, listName: 1} });
//});
//Meteor.publish('UserIdLists', function(listName) {
//    return UserIdLists_DB.find({ ownerId: this.userId, listName });
//});

Meteor.methods({
    updateUserIdList: function(listName, ids) {
        return wrapMeteorError(() => {
            return UserIdLists_DB.update({ ownerId: this.userId, listName }, { $set: {ids} }, { upsert: true });
        });
    },
    removeUserIdLists: function(listNames) {
        return wrapMeteorError(() => {
            return UserIdLists_DB.remove({ ownerId: this.userId, listName: { $in: listNames } });
        });
    }
});