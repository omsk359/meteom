import TaskData from '../../common/collections/TaskData';

Meteor.publish('tasks', function() {
    //return TaskData.find({});
    return TaskData.find({ ownerId: this.userId });
});
