import UserData from '../../common/collections/UserData';
import { Accounts } from "meteor/accounts-base"

UserData.allow({
    update: function(userId, userData, fields, modifier) {
        return _.isEqual(fields, ['data']);
    }
});

Meteor.publish('userData', function() {
    return UserData.find({ userId: this.userId }, { fields: { data: 1 }, limit: 1 });
});

Accounts.onCreateUser(function(options, user) {
    UserData.insert({ userId: user._id, data: {} });
    return user;
});
