import UserAccs from '../../common/collections/UserAccs';
import { updateAccs } from "../AccManager";
import * as _ from 'lodash';

Meteor.publish('Accs', function() {
    return UserAccs.find({ ownerId: this.userId });
});

UserAccs.allow({
    insert: function(userId, acc) {
        return (userId && acc.ownerId === userId);
    },
    update: function(userId, doc, fields, modifier) {
        return doc.ownerId === userId;
    },
    remove: function(userId, doc) {
        return doc.ownerId === userId;
    }
});

UserAccs.deny({
    update: function (userId, doc, fields, modifier) {
        // can't change owners
        return _.includes(fields, 'ownerId');
    },
    fetch: ['ownerId'] // no need to fetch 'owner'
});

Meteor.methods({
    updateAccs: _.ary(updateAccs, 1),
    removeAccs: function(ids) {
        //ids.splice(2);
        return UserAccs.remove({ _id: {$in: ids}, ownerId: Meteor.userId() });
    }
});