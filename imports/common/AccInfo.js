import { _getStr } from "./helpers";
import SomeError from "./SomeError";
import { ERR_TYPE } from "./SomeError";
import { log } from './logger/Logger';
import UserAccs from './collections/UserAccs';
import { HTTP } from 'meteor/dandv:http-more';
// !!!!!!!!!!
// import VkAcc from "../server/VkApi";

if (Meteor.isServer) {
//    import * as request from 'request';
    import request from 'request';
}

export const AccStateType = {
    OK: 'OK',
    BAN: 'BAN',
    FREEZE: 'FREEZE',
    LOGIN_FAILED: 'LOGIN FAILED',
    UPDATING: 'UPDATING'
};

export const AccTypes = {
    VK: 'VK'
};

// clients interface
export default class AccInfo {
    constructor(str, type) {
        if (typeof str == 'string')
            this.fromString(str);
        this.type = type || '';
        this.userAgent = AccInfo.randomUA();
        if (Meteor.isServer)
            this.cookieJar = request.jar();
    }
    getType()               { return this.type; }
    setType(t)              { this.type = t; }
    getState()              { return this.state; }
    isOK()                  { return this.state == AccStateType.OK }
    setStateOK()            { this.state = AccStateType.OK }
    setStateBan(reason)     { this.state = AccStateType.BAN; this.blockedReason = reason; }
    setStateFreeze(reason)  { this.state = AccStateType.FREEZE; this.blockedReason = reason; }
    setStateLoginFailed()   { this.state = AccStateType.LOGIN_FAILED; }

    save() {
        UserAccs.update(this._id, this.serialize());
        //AccManager.instance(this.ownerId).saveAcc(this);
    }

    captcha(url, checkDelay = 2000, maxCheckCnt = 20) {
        //let ruCaptchaKey = AccManager.instance(this.ownerId).ruCaptchaKey;
        if (!this.ruCaptchaKey)
            //return '';
            throw new SomeError(ERR_TYPE.CAPTCHA, 'ruCaptchaKey не задан');
        let captchaId = this.http.ruCaptchaUploadTry(url, this.ruCaptchaKey, 2);
        return this.http.ruCaptchaWait(captchaId, this.ruCaptchaKey, checkDelay, maxCheckCnt);
    }

    fromString(str) {
        var fields = str.split(/[\s:|]+/);
        if (fields.length < 2)
            throw new SomeError(ERR_TYPE.UNKNOWN, log.error('Acc parse err: ' + str));
        this.login = fields[0];
        if (/^8\d{10}$/.test(this.login))
            this.login = '7' + this.login.slice(1);
        this.pass = fields[1];

        this.proxy = new Proxy();
        if (fields.length >= 4)
            this.proxy.fromString(fields.slice(2, 10).join(' '));
    }
    toString() {
        if (_.has(this.proxy, 'ip'))
            return '{1}:{2} {3}'.format(this.login, this.pass, this.proxy.toString());
        return '{1}:{2}'.format(this.login, this.pass);
    }

    serialize() {
        var data = _.pick(this, ['login', 'pass', 'type', 'userAgent', 'state', 'userId', '_id', 'blockedReason']);
        data.proxy = this.proxy.serialize();
        return data;
    }
    deserialize(data, noOverride = false) {
        if (noOverride)
            return _.defaults(this, data);
        _.assign(this, data);
        var proxy = new Proxy();
        proxy.deserialize(this.proxy);
        this.proxy = proxy;
        return this;
    }
    // static deserializeAcc(data) {
    //     var accClass = AccInfo.accClassByType(data.type);
    //     var acc = new accClass();
    //     acc.deserialize(data);
    //     return acc;
    // }
    // static accClassByType(type) {
    //     if (type == 'VK')
    //         return VkAcc;
    //     return AccInfo;
    // }

    static randomUA() {
        var userAgents = [
            ['Mozilla/5.0 (Windows NT 6.1) AppleWebKit/{1}.{2} (KHTML, like Gecko) Chrome/{3}.{4}.{5}.0 Safari/{1}.{2}', [450, 550], [20, 40], [40, 41], [0, 2], [1600, 2228]],
            ['Mozilla/5.0 (Macintosh; Intel Mac OS X 10_{1}_{2}) AppleWebKit/{3}.{4} (KHTML, like Gecko) Chrome/{5}.0.{6}.1 Safari/{3}.{4}', [9, 10], [0, 2], [450, 550], [20, 40], [40, 41], [1600, 2228]]
        ];
        var uaItem = _.sample(userAgents);
        var args = _.map(_.tail(uaItem), arg => _.random.apply(_, arg));
        return uaItem[0].format(...args);
    }

    setRandomUA() { this.userAgent = AccInfo.randomUA(); }
    getUA() { return this.userAgent }
}

export class Proxy {
    constructor(str) {
        if (typeof str == 'string')
            this.fromString(str);
    }
    fromString(str) {
        if (!str) return;
        var fields = str.split(/[\s:|]+/);
        if (fields.length < 2)
            throw new Error(log.error('Proxy parse err: ' + str));
        this.ip = fields[0];
        this.port = fields[1];
        if (fields.length < 4)
            return;
        this.login = fields[2];
        this.pass = fields[3];
    }
    isEmpty() { return !this.ip; }
    toString() {
        if (!this.ip)
            return '';
        if (this.login !== undefined)
            return '{1}:{2}|{3}:{4}'.format(this.ip, this.port, this.login, this.pass);
        return '{1}:{2}'.format(this.ip, this.port);
    }
    toHttpString() {
        if (!this.ip)
            return '';
        if (this.login)
            return 'http://{3}:{4}@{1}:{2}'.format(this.ip, this.port, this.login, this.pass);
        return 'http://{1}:{2}'.format(this.ip, this.port);
    }
    check() {
        try {
            var result = HTTP.get('http://vk.com', { proxy: this.toHttpString() });
            this.debug('proxy check result: {1}', result);
            this.state = 200;
        } catch (e) {
            if (_getStr(e, 'message').contains('tunneling socket could not be established'))
                this.state = 404;
        }
        return this.state;
    }

    serialize() {
        return this;
    }
    deserialize(data) {
        _.assign(this, data);
        return this;
    }
}
