import * as _ from 'lodash';
import URL from 'url';
import moment from "moment";

//if (Meteor.isServer)
//    stringify = Meteor.npmRequire('json-stringify-safe');

//String.prototype.format = function() {
//    var formatted = this;
//    for (var i = 0; i < arguments.length; i++) {
//        var arg = arguments[i];
//        if (typeof arg != 'string' && !(arg instanceof String) && arg instanceof Object)
//            if (Meteor.isClient)
//                try {
//                    arg = JSON.stringify(arg);
//                } catch (e) {
//                    arg = '[CIRCULAR JSON]';
//                }
//            else
//                arg = stringify(arg);
//        var regexp = new RegExp('\\{' + (i + 1) + '\\}', 'gi');
//        formatted = formatted.replace(regexp, arg);
//    }
//    return formatted;
//};
String.prototype.format = function (...args) {
    var formatted = this;
    for (var i = 0; i < args.length; i++) {
        var arg = args[i];
        if (!_.isString(arg) && arg instanceof Object) {
            if (_.isFunction(arg))
                arg = arg.toString();
            else
                try {
                    arg = JSON.stringify(arg);
                } catch (e) {
                    arg = '[CIRCULAR JSON]';
                }
        }
        var regexp = new RegExp('\\{' + (i + 1) + '\\}', 'g');
        formatted = formatted.replace(regexp, arg);
    }
    return formatted;
};
String.prototype.contains = function(it) { return this.indexOf(it) != -1; };

export function numFromRange(range) {
    if (typeof range == 'undefined')
        return 0;
    if (range instanceof Array)
        return _.random(+range[0], +range[1]);
    return range;
}
export function delayByNum(delays, id_i) {
    var maxKey = -1;
    _.each(delays, function(val, key) {
        if (id_i % key == 0 && (key > maxKey || maxKey < 0) && key > 0)
            maxKey = key;
    });
    if (maxKey < 0) return 0;
    var maxVal = delays[maxKey];
    return numFromRange(maxVal);
}

export function getQueryParams(url) {
    var queryData = URL.parse(url, true).query;
    return queryData;
    //var match,
    //    pl     = /\+/g,  // Regex for replacing addition symbol with a space
    //    search = /([^&=]+)=?([^&]*)/g,
    //    decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
    //    query  = new URL(url).search.substring(1);
    //
    //var urlParams = {};
    //while (match = search.exec(query))
    //    urlParams[decode(match[1])] = decode(match[2]);
    //return urlParams;
}

export function toQueryString(obj) {
    var parts = [];
    for (var i in obj) {
        if (obj.hasOwnProperty(i))
            parts.push(encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]));
    }
    return parts.join('&');
}

export function timeDiffFormat(timeStart, timeEnd) {
    timeEnd = timeEnd || moment();
    return moment.utc(timeEnd.diff(timeStart)).format('HH:mm:ss.SSS');
}

export function timeFormat(date) {
    //date = date || new Date();
    //return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, '$1');
    return moment(date).utcOffset('+03:00').format('HH:mm:ss');
}
export function seconds2time(seconds) {
    var hours   = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    seconds = Math.round(seconds - (hours * 3600) - (minutes * 60));
    var time = '';

    if (hours != 0)
        time = hours + ':';

    if (minutes != 0 || time !== '') {
        minutes = (minutes < 10 && time !== '') ? '0' + minutes : String(minutes);
        time += minutes + ':';
    }
    if (time === '')
        time = seconds + 's';
    else
        time += (seconds < 10) ? '0' + seconds : String(seconds);
    return time;
}

export function parseDelays(delaysStr) {
    var delaysRe = /(\d+)\s*:\s*(\d+)\s*-\s*(\d+)/g;
    var match = delaysRe.exec(delaysStr);
    var delays = {};
    while (match != null) {
        delays[match[1]] = [+match[2], +match[3]];
        match = delaysRe.exec(delaysStr);
    }
    return delays;
}
export function delaysToStr(delays = {}) {
    return _.reduce(delays, (s, delay, i) => s += `${i}: ${delay[0]}-${delay[1]} `, '');
}

export function _getStr(obj, path) {
    return _.get(obj, path) || '';
}

export function merge(oldState, newState) {
    return _.extend({}, oldState, newState);
}

export function omitUndef(obj) {
    //return _.transform(obj, (newObj, val, key) => val === undefined || (newObj[key] = val));
    return _.omit(obj, _.isUndefined);
}

export function listNameDateFormat(s) {
    return s.replace(/(!.*!)/g, match => moment().format(match.replace(/!/g, '')));
}

export function listNamesDateFormat(strs) {
    let gens = _.transform(strs, (gens, s) => {
        var gen = listNameDateFormat(s);
        gen != s && gens.push(gen)
    });
    return [...strs, ...gens];
}


export function splitStr(s, splitter = '\n') {
    return _.compact(_.map(s.split(splitter), _.trim));
}

export function variateText(text) { // {aaa|ббб|ccc|ddd}[2,3,', ',0]
    return text.replace(/\{(.*?)\}(\[.*?\])?/g, function(match, vars, nums) {
        vars = vars.split('|');
        if (!nums)
            return _.sample(vars);
        var nums_parts = nums.match(/(\d+).*?(\d+)(?:.*['"](.*?)['"])?(?:[^\d]*([01]+))?/);
        if (!nums_parts)
            return _.sample(vars);
        if (typeof nums_parts[3] == 'undefined')
            nums_parts[3] = ', ';
        if (typeof nums_parts[4] == 'undefined')
            nums_parts[4] = '1';
        var randSize = _.random(+nums_parts[1], +nums_parts[2]);
        if (+nums_parts[4]) // 1 - shuffle (default)
            return _.take(_.shuffle(vars), randSize).join(nums_parts[3]);
        return randSequence(vars, randSize).join(nums_parts[3]);
    });
}
export function randSequence(arr, size) {
    var indexes = [];
    while (indexes.length < size) {
        var i = _.random(arr.length - 1);
        if (!_.includes(indexes, i))
            indexes.push(i);
    }
    return indexes.sort().map(i => arr[i]);
}

export function splitMsgsDalay(text) {
    var re = /([\s\S]*?)\$\[(\d+)(?:-(\d+))?\]/g;
    var match = re.exec(text);
    var results = [{ delay: [0, 0] }];
    while (match != null) {
        _.last(results).text = _.trim(match[1]);
        results.push({delay: [+match[2], +match[3] || +match[2]]});
        var lastIndex = re.lastIndex;
        match = re.exec(text);
    }
    _.last(results).text = text.substr(lastIndex);
    return results;
}



export function getSynonyName(name) {
    var synonyms = `
Александр Александр Саша Саш
Алексей Алексей Лёша Лёш
Альберт Альберт Алик
Анатолий Анатолий Толя Толь
Борис Борис Боря Борь
Вадим Вадим Вадик
Валентин Валентин Валя
Валерий Валерий Валер Валера
Василий Василий Вася Вась
Виталий Виталий Виталик Виталь
Владимир Владимир Володя Вова
Владислав Владислав Слава Слав
Вячеслав Вячеслав Слава Слав
Геннадий Геннадий Гена Ген
Георгий Георгий Гера Гер
Григорий Григорий Гриша Гриш
Даниил Даниил Данил Даня
Дмитрий Дмитрий Дима Дим
Евгений Евгений Женя Жень
Ефим Ефим Фима
Иван Иван Ваня Вань
Константин Константин Костя Кость
Леонид Леонид Лёня Лёнь
Максим Максим Макс
Михаил Михаил Миша Миш
Никита Никита Никит
Николай Николай Коля Коль
Павел Павел Паша Паш
Петр Петр Петя Петь
Роман Роман Рома Ром
Святослав Святослав Слава Слав
Сергей Сергей Серёжа Серёж
Станислав Станислав Слава
Степан Степан Стёпа Стёп
Федор Федор Федя Федь
Эдвард Эдвард
Юрий Юрий Юра Юр
Яков Яков Яша Яш
Ярослав Ярослав Слава Слав
Александра Александра Саша Саш
Алена Алена Алён
Алеся Алеся Алесь
Алина Алина Алин
Алиса Алиса Алис
Алла Алла Алл
Альбина Альбина Альбин
Анастасия Анастасия Настя Насть
Ангелина Ангелина Ангелин
Анжела Анжела Анжел
Анна Анна Аня Ань
Антонина Антонина Тоня Тонь
Ариана Ариана Ариан
Арина Арина Арин
Ася Ася Ась
Валентина Валентина Валя Валь
Валерия Валерия Лера Лер
Варвара Варвара Варя Варь
Василиса Василиса Василис
Вера Вера Вер
Вероника Вероника Вероник
Виктория Виктория Вика Вик
Галина Галина Галя Галь
Дарья Дарья Даша Даш
Диана Диана Диан
Дина Дина Дин
Евгения Евгения Женя Жень
Евдокия Евдокия Дуня
Екатерина Екатерина Катерина Катя
Елена Елена Лена Лен
Елизавета Елизавета Лиза Лиз
Жанна Жанна Жанн
Зоя Зоя Зой
Инна Инна Инн
Ирина Ирина Ира Ир
Карина Карина Варин
Кира Кира Кир
Клавдия Клавдия Кава Клав
Кристина Кристина Кристин
Ксения Ксения Ксюша Ксюш
Лариса Лариса Ларис
Лидия Лидия Лида Лид
Лилия Лилия Лиля Лиль
Любовь Любовь Люба Люб
Людмила Людмила Мила Люда
Майя Майя Май
Маргарита Маргарита Рита Рит
Марианна Марианна Мариан
Марина Марина Марин
Мария Мария Маша Маш
Марта Марта Март
Мила Мила Мил
Надежда Надежда Надя Надь
Наталья Наталья Наташа Наташ
Нина Нина Нин
Оксана Оксана Оксан
Олеся Олеся Олесь
Ольга Ольга Оля Оль
Полина Полина Полин
Светлана Светлана Света Свет
Софья Софья София Соня
Таисия Таисия Тая Тай
Тамара Тамара Тамар
Татьяна Татьяна Таня Тань
Ульяна Ульяна Ульян
Юлия Юлия Юля Юль
Яна Яна Ян
`;
    var arr = _.compact(_.map(synonyms.split(/[\r\n]/), _.trim));
    var map = {};
    _.each(arr, function(s) {
        var parts = s.split(' ');
        map[parts[0].toLowerCase()] = _.tail(parts);
    });
    var synNames = map[name.toLowerCase()];
    if (!synNames)
        return name;
    return _.sample(synNames);
}

export function prepareDescText(s) {
    return s.split('\n').map(s => _.trim(s)).join('<br />');
    //s = _.trim(s);
    //s.replace('\n', '<br />');
    //return s;
}