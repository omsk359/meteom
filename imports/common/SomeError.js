class ExtendableError extends Error {
    constructor(message, e) {
        super();
        this.message = message;
        this.stack = _.get(e, 'stack');// || (new Error()).stack;
        this.name = this.constructor.name;
    }
}

export const ERR_TYPE = {
    AUTH: 'AUTH',
    UPDATE_TOKEN: 'UPDATE_TOKEN',
    PROXY: 'PROXY',
    VK_API: 'VK_API',
    TASK_MANAGER: 'TASK_MANAGER',
    TASK_STOP: 'TASK_STOP',
    NO_PROXY: 'NO_PROXY',
    CAPTCHA: 'CAPTCHA',
    SMS: 'SMS',
    HTTP: 'HTTP',
    TASK_LOGIC: 'TASK_LOGIC',
    UNKNOWN: 'UNKNOWN',

    // TODO
    TASK_TIMEOUT: 'TASK_TIMEOUT',

    TASK_STOP_EVENT: 'TASK_STOP_EVENT', // signal
    TASK_WRONG_PARAM: 'TASK_WRONG_PARAM'
};

export default class SomeError extends ExtendableError {
    constructor(type, msg, e) {
        super(msg, e || new Error());
        this.name = type;
    }
    //errMsg() {
    //    if (this.name == ERR_TYPE.VK_API)
    //        return _.get(result, 'error.error_msg') || _.get(result, 'error') || result;
    //}
    //type() { return this.name; }
}
