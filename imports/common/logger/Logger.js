import { LEVELS } from './levels'
import { timeFormat } from "../helpers";

export class LogFormatters {
    static withTime(msg, ...params) {
        msg = msg.format(...params).replace(/<.*?>/g, '');
        return '[{1}] {2}'.format(timeFormat(), msg);
    }
    static subTaskPrefix(name) {
        return '[{1}] '.format(name);
    }
    static subTask(name) {
        var prefix = LogFormatters.subTaskPrefix(name);
        return function(msg, ...params) {
            return LogFormatters.withTime(prefix + msg, ...params);
            //return '[{1}] [{2}] {3}'.format(name, timeFormat(), msg.format(...params));
        }
    }
}

export class LogHandler {
    constructor() {}
    write(text) {}
    clear() {}
}

export  class ConsoleLogger extends LogHandler {
    write(text) {
        console.log('> ' + text);
    }
}

export default class Logger {
    constructor(parent) {
        this.parent = parent;
        this.lvl = LEVELS.ALL;
        this.handlers = [];
        this.addHandler(new ConsoleLogger());
        this.formatter = LogFormatters.withTime;
    }
    addHandler(handler) {
        this.handlers.push(handler);
    }
    setLevel(val) {
        this.lvl = LEVELS.getLevel(val);
    }
    logAtLevel(lvl, ...params) {
        if (this.parent)
            this.parent.logAtLevel(lvl, ...params);
        if (lvl < this.lvl)
            return;
        var text = this.formatter(...params);
        text = text.substr(0, 500);
        this.handlers.forEach(handler => handler.write(text));
    }
    info(...params) {
        return this.logAtLevel(LEVELS['INFO'], ...params);
    }
    debug(...params) {
        return this.logAtLevel(LEVELS['DEBUG'], ...params);
    }
    error(...params) {
        return this.logAtLevel(LEVELS['ERROR'], ...params);
    }
    clear() {
        _.each(this.handlers, h => h.clear());
    }
}

export var log = new Logger(); // default logger


//Logger = class Logger {
//    info(msg, ...params) {
//        if (params.length)
//            msg = msg.format(...params);
//        console.log(msg);
//        return msg;
//    }
//
//    debug(msg, ...params) {
//        if (params.length)
//            msg = msg.format(...params);
//        console.log(msg);
//        return msg;
//    }
//
//    error(msg, ...params) {
//        if (params.length)
//            msg = msg.format(...params);
//        var s = 'ERROR: ' + msg;
//        console.log(s);
//        return s;
//    }
//}
