import fs from 'fs';
import readline from 'readline';
import path from 'path';
import { log } from '../Logger';
import { LogHandler } from "../Logger";
import Logger from "../Logger";
import { LogFormatters } from "../Logger";
import rimraf from "rimraf";

//__ROOT_APP_PATH__ = fs.realpathSync('.');
// export const __LOG_DIR__ = process.platform == 'win32' ?
//                             'C:\\meteor-logs' :
//                             Meteor.settings.logDir;
// export const Meteor.settings.tasksLogDir = path.join(Meteor.settings.logDir, 'tasks');

class FileLogger extends LogHandler {
    constructor(fileName) {
        super();
        this.fileName = fileName;
    }
    write(text) {
        fs.appendFile(this.fileName, text + '\n', err => {
            if (err)
                console.error(err);
        });
    }
    read(onRead, onFinished) {
        let input = fs.createReadStream(this.fileName);
        let lineReader = readline.createInterface({
            input,
            terminal: false
        });
        lineReader.on('line', onRead);
        input.on('end', onFinished);
        input.on('error', err => {
            log.error('FileLogger.read.createReadStream: {1}', err.message);
            log.error('FileLogger.read.createReadStream: {1}', err.stack);
        });
    }
    clear() {
    }
}

class StreamTaskLogger extends LogHandler {
    constructor(userId, eventName, fileLogger) {
        super();
        this.userId = userId;
        this.eventName = eventName;
        //let self = this;
        Streamy.on(this.eventName + '_loadAll', (data, from) => {
            fileLogger.read(
                //line => Streamy.emit(this.eventName, { data: line }),
                line => Streamy.emit(this.eventName, { data: line }, from),
                () => Streamy.emit(this.eventName + '_loadAllFinished', { data: {} })
            );
        });
    }
    write(text) {
        //console.log('write to stream[{1}]: {2}'.format(this.eventName, text));
        try {
            Streamy.sessionsForUsers(this.userId).emit(this.eventName, { data: text });
        } catch (e) {
            log.error('StreamTaskLogger write: {1}', e);
            log.error('stask: {1}', e.stack);
        }
    }
    clear() {
        let handlers = Streamy.handlers();
        delete handlers[this.eventName + '_loadAll'];
        log.debug('clear StreamTaskLogger: {1}', this.eventName + '_loadAll');
    }
}

export default class TaskLogger extends Logger {
    constructor(task) {
        super();
        this.task = task;
        this.formatter = LogFormatters.withTime;
        log.debug('TaskLogger task._id: {1}', task._id);
        if (task._id) {
            this.dirName = path.join(Meteor.settings.tasksLogDir, '{1}_{2}'.format(task.ownerId, task._id));
            log.debug('TaskLogger dirName: {1}', path.resolve(this.dirName));
            if (!fs.existsSync(this.dirName))
                fs.mkdirSync(this.dirName);
            this.fileName = path.join(this.dirName, 'ALL.log');
        }
        let fileLogger = new FileLogger(this.fileName);
        log.debug('TaskLogger fileName: {1}', this.fileName);
        this.addHandler(fileLogger);
        this.addHandler(new StreamTaskLogger(task.ownerId, task._id, fileLogger));
    }
    //static pauseTaskHook(task, logger) {
    //    if (task.isPaused()) {
    //        logger.handlers.forEach(handler => handler.write('Пауза'));
    //        task.pauseHook();
    //    }
    //    if (task.isStopped()) {
    //        logger.handlers.forEach(handler => handler.write('Стоп'));
    //        throw new SomeError(ERR_TYPE.TASK_STOP_EVENT);
    //    }
    //}
    logAtLevel(lvl, ...params) {
        //TaskLogger.pauseTaskHook(this.task, this);
        this.task.pauseHook();
        super.logAtLevel(lvl, ...params);
    }
    clear() {
        rimraf(this.dirName, err => {
            if (err)
                log.debug('DIR CLEAR ERR: {1}', err);
        });
        super.clear();
    }
}

export class SubTaskLogger extends Logger {
    constructor(subTask) {
        super(subTask.parent.log);
        this.subTask = subTask;
        //this.formatter = LogFormatters.subTask(subTask._id.replace(/.*#/, ''));
        this.formatter = LogFormatters.subTask(subTask.title);
        this.dirName = subTask.parent.log.dirName;
        this.fileName = path.join(this.dirName, '{1}.log'.format(subTask.title));
        this.handlers = []; // parent Console only
        let fileLogger = new FileLogger(this.fileName);
        this.addHandler(fileLogger);
        this.addHandler(new StreamTaskLogger(subTask.ownerId, subTask._id, fileLogger));
    }
    logAtLevel(lvl, ...params) {
        //TaskLogger.pauseTaskHook(this.subTask, this);
        this.subTask.pauseHook();
        var text = this.formatter(...params);
        text = _.truncate(text, { length: 2000 });
        if (this.parent && params.length) {
            //this.parent.logAtLevel(lvl, ...params);
            params[0] = LogFormatters.subTaskPrefix(this.subTask.title) + params[0];
            this.parent.logAtLevel(lvl, ...params);
        }
        if (lvl < this.lvl)
            return;
        //var text = this.formatter(...params);
        this.handlers.forEach(handler => handler.write(text));
    }
}
