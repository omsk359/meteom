export const TaskActionTypes = {
    INVITE: 'ACTION_INVITE',
    SEND_MSG: 'ACTION_SEND_MSG',
    DELETE_FRIEND: 'ACTION_DELETE_FRIEND',
    //ADD_TO_INVITE_DB: 'ACTION_ADD_TO_INVITE_DB',
    ADD_TO_OLD_INVITE_DB: 'ACTION_ADD_TO_OLD_INVITE_DB',
    NOPE: 'ACTION_NOPE',
    DELAY: 'ACTION_DELAY',
    ADD_TO_USER_ID_LISTS: 'ACTION_ADD_TO_USER_ID_LISTS',
    REMOVE_FROM_USER_ID_LISTS: 'REMOVE_FROM_USER_ID_LISTS'
};

export const TaskFilterTypes = {
    ONLINE: 'FILTER_ONLINE',
    LAST_SEEN: 'FILTER_LAST_SEEN',
    NOT_EXIST_OUT_MSG: 'FILTER_NOT_EXIST_OUT_MSG',
    //NOT_IN_INVITE_DB: 'FILTER_NOT_IN_INVITE_DB',
    NOT_IN_OLD_INVITE_DB: 'FILTER_NOT_IN_OLD_INVITE_DB',
    FRIENDS: 'FILTER_FRIENDS',
    USER_ID_LISTS:  'FILTER_USER_ID_LISTS'
};

export const IdsSourceTypes = {
    IDS_COMMON_LIST: 'IDs_SOURCE_COMMON_LIST',
    IDS_COPY_LIST: 'IDs_SOURCE_COPY_LIST',
    SEARCH_URLS: 'IDs_SOURCE_SEARCH_URLS',
    FRIENDS: 'IDs_SOURCE_FRIENDS',
    FRIEND_REQUESTS_IN: 'FRIEND_REQUESTS_IN',
    FRIEND_REQUESTS_OUT: 'FRIEND_REQUESTS_OUT'
};



export const TaskRunStates = {
    STARTED: 'STARTED',
    STOPPED: 'STOPPED',
    PAUSED: 'PAUSED',
    FINISHED: 'FINISHED',
    WAIT: 'WAIT',
    RESUME: 'RESUME',
    INIT: 'INIT'
};

export const StdUserIdLists = {
    INVITE_LIST: 'INVITE',
    FIRST_MSG_LIST: '1st MSG'
};

export const AccOpTypes = {
    ADD: 'ADD',
    UPDATE: 'UPDATE',
    REMOVE: 'REMOVE'
};
