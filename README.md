## Automation of actions in social networks (VKontakte).

A task management system has been developed (design, suspension, deletion).
One task can manage multiple accounts. You can run multiple tasks in parallel.

Some features:
- Account registration
- Clearing accounts, renaming, changing some settings
- Collection of user IDs on VK search links
- Filtering users by criteria (time of the last visit / check whether it is a friend / whether it is on the list / etc)
- Sending messages / invitations to friends / etc
- Scheduling task launch time, periodic launch
- Integration with captcha recognition service

**Stack: JavaScript (ES6), Meteor (Node.js), React, Redux, MongoDB**
